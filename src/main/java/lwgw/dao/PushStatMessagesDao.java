package lwgw.dao;

import java.sql.Statement;

import lwgw.settings.LoraWanServer;

/**
 * DAO class for json schemas that are used to make gateway statistic messages.
 * If no record is present, then default json encapsulation expected in Semtech 
 * document : ANNWS.01.2.1.W.SYS "Gateway to server interface" is used.
 * Each time a change of json schema is done for a server, a line in the table
 * is added with the modification date. The latest record is the current in use.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class PushStatMessagesDao extends PushMessagesSettingsDao {
	private static PushStatMessagesDao psmDao = null;
	private static String sqlCreate = null;
	private static String tableName = null;
	
	private PushStatMessagesDao(Statement s) {
		super(s);
	}

	public static PushStatMessagesDao getInstance(Statement s) {
		if (psmDao == null) {
			psmDao = new PushStatMessagesDao(s);
		}
		return psmDao;
	}

	/** Insertion method.
	 * @param s The server with its new Statistic Json encapsulation.
	 * @param modificationDate The modification date.
	 */
	//Common code put in mother class
	public void insert(LoraWanServer s, String modificationDate) {
		super.insert(s, modificationDate, s.getStatJsonSchema());
	}

	//Common code put in mother class
	@Override
	public String read(LoraWanServer s) {
		String result = super.read(s);
		return result;
	}
	
	//Method used by the mother class to retrieve the field
	//since the field is declared in the daughter class
	@Override
	protected void setTableNameAndSqlCreate(String tableName) {
		PushStatMessagesDao.tableName = tableName;
		sqlCreate = sqlCreatePart1 + tableName + sqlCreatePart2;
	}
	
	//Method used by the mother class to retrieve the field
	//since the field is declared in the daughter class
	@Override
	protected String getTableName(){
		return PushStatMessagesDao.tableName;
	}
	
	@Override
	protected String getSqlCreate(){
		return PushStatMessagesDao.sqlCreate;
	}
}
