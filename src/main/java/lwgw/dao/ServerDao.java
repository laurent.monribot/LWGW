package lwgw.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import lwgw.settings.LoraWanServer;


/**
 * This object represents the LoraWanServers table in database.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class ServerDao {
	private final static Logger logger = Logger.getLogger(ServerDao.class);
	private static final String sqlCreate = "CREATE TABLE IF NOT EXISTS LoraWanServers "
			+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT, " + " address        CHAR(15)  NOT NULL, "
			+ " port           INTEGER  NOT NULL, "
			+ " nbMesSent      INTEGER  NOT NULL, "
			+ " nbMesAcked     INTEGER  NOT NULL, "
			+ " dateBegin      CHAR(24) NOT NULL, " // '2017-05-27T22:23:20.535Z'
			+ " dateEnd        CHAR(24) ); ";

	private static ServerDao sDao = null;
	private Statement statement = null;
	private PushRadioMessagesDao prmDao = null;
	private PushStatMessagesDao psmDao = null;
	private ServerProtocolVersionDao spvDao = null;
		
	private ServerDao(Statement s, PushRadioMessagesDao prmDao, PushStatMessagesDao psmDao, ServerProtocolVersionDao spvDao) {
		// dm.getStatement() can't be null
		// when Dao is instantiated by dm.openDatabase(dbFileName)
		statement = s;
		this.prmDao = prmDao;
		this.psmDao = psmDao;
		this.spvDao = spvDao;

		try {
			statement.executeUpdate(sqlCreate);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public static ServerDao getInstance(Statement s,
			PushRadioMessagesDao prmDao,
			PushStatMessagesDao psmDao,
			ServerProtocolVersionDao spvDao
			) {
		if (sDao == null)
			sDao = new ServerDao(s, prmDao, psmDao, spvDao);
		return sDao;
	}

	public void insert(LoraWanServer s) {
		StringBuilder str = new StringBuilder();
		str.append("INSERT INTO LoraWanServers( ");
		str.append("address, port, nbMesSent, nbMesAcked, dateBegin ) ");
		str.append("VALUES ( '");
		str.append(s.getAddress());
		str.append("', ");
		str.append(s.getPort());
		str.append(", ");
		str.append(s.getNbMesSent());
		str.append(", ");
		str.append(s.getNbMesAcknowledged());
		str.append(", '");
		str.append(s.getCreationDate());
		str.append("' ); ");
		try {
			if(statement.executeUpdate(str.toString()) == 1) {
				ResultSet generatedKeys = statement.executeQuery("SELECT last_insert_rowid()");
				if (generatedKeys.next()) {
					s.setId(generatedKeys.getInt(1));
				}
				spvDao.insert(s, s.getCreationDate());
			}
			logger.debug("LoraWan Server inserted in database of id = " + s.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}
	
	public void updateForTermination(LoraWanServer s, String time) {
		int id = s.getId();
		if(id != 0) {			
			
			StringBuilder str = new StringBuilder();
			str.append("UPDATE LoraWanServers SET ");
			str.append(" dateEnd = '");
			str.append(time);
			str.append("' WHERE ID = ");
			str.append(id);
			str.append(" ; ");
			try {
				if(statement.executeUpdate(str.toString()) == 1) {
					logger.debug("LoraWan Server of id = " + s.getId() + " updated for termination in database" );
				} else {
					logger.error("LoraWan Server of id = " + s.getId() + " NOT updated for termination in database" );				
				}
			} catch (SQLException e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	public void updateForStatistics(LoraWanServer s) {
		int id = s.getId();
		if(id != 0) {			
			StringBuilder str = new StringBuilder();
			str.append("UPDATE LoraWanServers SET ");
			str.append(" nbMesSent  = ");
			str.append(s.getNbMesSent());
			str.append(", nbMesAcked = ");
			str.append(s.getNbMesAcknowledged());
			str.append(" WHERE ID = ");
			str.append(id);
			str.append(" ; ");
			try {
				if(statement.executeUpdate(str.toString()) == 1) {
					logger.debug("LoraWan Server of id = " + s.getId() + " updated for statistics in database" );
				} else {
					logger.error("LoraWan Server of id = " + s.getId() + " NOT updated for statistics in database" );				
				}
			} catch (SQLException e) {
				logger.error(e.getMessage());
			}
		}
	}

	
	public ArrayList<LoraWanServer> readActivesServers() {
		ArrayList<LoraWanServer> sList = new ArrayList<LoraWanServer>();
		try {
			String str = new String();
			str = "SELECT * FROM LoraWanServers WHERE dateEnd IS NULL ;";
			
			ResultSet rs = statement.executeQuery(str);
			LoraWanServer lws = null;
			while (rs.next()) {
				//logger.debug("SERVER FOUND IN DB " + rs.getString("address") + ":" + rs.getInt("port"));
				lws = new LoraWanServer(rs.getString("address"), rs.getInt("port"));
				//logger.debug("NB MES SENT " + rs.getString("nbMesSent"));
				lws.setNbMesSent(rs.getInt("nbMesSent"));
				//logger.debug("NB MES ACKNOWLEDGED " + rs.getString("nbMesAcked"));
				lws.setNbMesAcknowledged(rs.getInt("nbMesAcked"));
				//logger.debug("CREATION DATE " + rs.getString("dateBegin"));
				lws.setCreationDate(rs.getString("dateBegin"));
				//logger.debug("DATABASE ID " + rs.getInt(1));
				lws.setId(rs.getInt(1));
				sList.add(lws);
			}
			// This here after separate for is needed,
			// because you cannot make nested queries when treating a resultSet
			// if not, the resultSet iterator is impacted
			for(LoraWanServer s : sList) {
				//logger.debug("RADIO JSON SCHEMA " + prmDao.read(s));
				s.setRadioJsonSchema(prmDao.read(s));
				//logger.debug("STAT JSON SCHEMA " + psmDao.read(s));
				s.setStatJsonSchema(psmDao.read(s));
				//logger.debug("PROTOCOL " + spvDao.read(s));
				s.setProtocolVersion((byte) spvDao.read(s));
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return sList;
	}

	public LoraWanServer read(String adr, int p) {
		LoraWanServer lws = null;		
		try {
			StringBuilder str = new StringBuilder();
			str.append("SELECT * FROM ( ");
			str.append("SELECT * FROM LoraWanServers WHERE address = '");
			str.append(adr);
			str.append("' and  port = ");
			str.append(p);
			str.append(" and dateEnd IS NULL ");
			str.append(" ) tmp ORDER BY dateBegin DESC LIMIT 1 ;");
			
			ResultSet rs = statement.executeQuery(str.toString());
			while (rs.next()) {
				lws = new LoraWanServer(rs.getString("address"), rs.getInt("port"));
				lws.setNbMesSent(rs.getInt("nbMesSent"));
				lws.setNbMesAcknowledged(rs.getInt("nbMesAcked"));
				lws.setCreationDate(rs.getString("dateBegin"));
				lws.setId(rs.getInt(1));
				lws.setRadioJsonSchema(prmDao.read(lws));
				lws.setStatJsonSchema(psmDao.read(lws));
				lws.setProtocolVersion((byte) spvDao.read(lws));
				logger.debug("CREATION DATE : " + rs.getString("dateBegin"));
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return lws;
	}

	public void display() {
		try {
			ResultSet rs = statement.executeQuery("select * from LoraWanServers;");
			while (rs.next()) {
				// Read the result set
				logger.info("address = " + rs.getString("address")
				+ "port = " + rs.getInt("port")
				+ "nbMesSent = " + rs.getInt("nbMesSent")
				+ "nbMesAcked = " + rs.getInt("nbMesAcked"));
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}
}
