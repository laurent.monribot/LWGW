package lwgw.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import lwgw.settings.LoraRadioSettings;
import lwgw.upstreamdata.GatewayRecord;

/**
 * Database cache for Lora radio settings that have to be read frequently
 * to know if it is needed to update these settings.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class LoraSettingsInCache {
	private final static Logger logger = Logger.getLogger(LoraSettingsInCache.class);
	public static final int LORA_MESSAGE_SETTINGS = 1;
	public static final int LORA_SETTINGS_CHANGE = 2;
	private int id;
	private double freq;
	private int chan;
	private int rfch;
	private int stat;
	private String codr;
	private String modu;
	private String datr;
	private int codeRedundancy;
	private int bandw;
	private int sf;
	private int power;

	public LoraSettingsInCache() {
		id = 0;
		freq = 0.0;
		chan = 0;
		rfch = 0;
		stat = 0;
		codr = "";
		modu = "";
		datr = "";
		codeRedundancy = 0;
		bandw = 0;
		sf = 0;
		power = 0;
	}
	
	public LoraSettingsInCache(ResultSet rs, int type ) {
		int resultSetId =0;
		if(type == LORA_MESSAGE_SETTINGS) {
			codeRedundancy = 0;
			bandw = 0;
			sf = 0;
			power = 0;
			try {
				// Read the result set we are interested in :
				resultSetId = rs.getInt("ID");
				// Read other values to keep them as a cache :
				freq = rs.getDouble("freq");
				chan = rs.getInt("chan");
				rfch = rs.getInt("rfch");
				stat = rs.getInt("stat");
				codr = rs.getString("codr");
				modu = rs.getString("modu");
				datr = rs.getString("datr");
			} catch (SQLException e) {
				logger.error(e.getMessage());
			}
		} else if(type == LORA_SETTINGS_CHANGE) {
			codr = "";
			datr = "";
			try {
				// Read the result set we are interested in :
				resultSetId = rs.getInt("ID");
				// Read other values to keep them as a cache :
				freq = rs.getDouble("freq");
				chan = rs.getInt("chan");
				rfch = rs.getInt("rfch");
				stat = rs.getInt("stat");
				codeRedundancy = Integer.valueOf(rs.getString("codr"));
				modu = rs.getString("modu");
				bandw = rs.getInt("bandw");
				sf = rs.getInt("sf");
				power = rs.getInt("power");

			} catch (SQLException e) {
				logger.error(e.getMessage());
			}
		}
		id = resultSetId;
	}

	public LoraSettingsInCache(LoraRadioSettings lrs, int id) {
		this.id = id;
		freq = 0.0;
		codr = "";
		datr = "";
		codeRedundancy = 0;
		bandw = 0;
		sf = 0;
		power = 0;
		
		chan = 0;
		rfch = 0;
		stat = 1;
		modu = "LORA";
		
		if(lrs!= null) {
				freq = lrs.getFrequency();
				chan = 0;
				rfch = 0;
				stat = 1;
				codeRedundancy = lrs.getCodeRedundancy();
				modu = "LORA";
				bandw = lrs.getBandwidth();
				sf = lrs.getSpreadingFactor();
				power = lrs.getPower();
			}
	}
	
	boolean equalsTo(GatewayRecord gr) {
		if(gr!= null
			&& gr.getFreq() + 0.05> freq
			&& gr.getFreq() - 0.05 < freq
			&& gr.getChan() == chan
			&& gr.getRfch() == rfch
			&& gr.getStat() == stat
			&& gr.getCodr().compareTo(codr)==0
			&& gr.getModu().compareTo(modu)==0
			&& gr.getDatr().compareTo(datr)==0) {
				return true;
			} else {
				return false;
			}
	}
	
	boolean equalsTo(LoraRadioSettings lrs) {
		if(lrs!= null
			&& lrs.getFrequency() + 0.05> freq
			&& lrs.getFrequency() - 0.05 < freq
			&& 0 == chan
			&& 0 == rfch
			&& 1 == stat
			&& lrs.getCodeRedundancy() == codeRedundancy
			&& "LORA".compareTo(modu)== 0
			&& lrs.getBandwidth() == bandw
			&& lrs.getSpreadingFactor() == sf
			&& lrs.getPower() == power) {
				return true;
			} else {
				return false;
			}
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getFreq() {
		return freq;
	}

	public void setFreq(double freq) {
		this.freq = freq;
	}

	public int getChan() {
		return chan;
	}

	public void setChan(int chan) {
		this.chan = chan;
	}

	public int getRfch() {
		return rfch;
	}

	public void setRfch(int rfch) {
		this.rfch = rfch;
	}

	public int getStat() {
		return stat;
	}

	public void setStat(int stat) {
		this.stat = stat;
	}

	public String getCodr() {
		return codr;
	}

	public void setCodr(String codr) {
		this.codr = codr;
	}

	public String getModu() {
		return modu;
	}

	public void setModu(String modu) {
		this.modu = modu;
	}

	public String getDatr() {
		return datr;
	}

	public void setDatr(String datr) {
		this.datr = datr;
	}

	public int getCodeRedundancy() {
		return codeRedundancy;
	}

	public void setCodeRedundancy(int codeRedundancy) {
		this.codeRedundancy = codeRedundancy;
	}

	public int getBandw() {
		return bandw;
	}

	public void setBandw(int bandw) {
		this.bandw = bandw;
	}

	public int getSf() {
		return sf;
	}

	public void setSf(int sf) {
		this.sf = sf;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}
	
}
