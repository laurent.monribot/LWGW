package lwgw.dao;

import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import lwgw.settings.LoraWanServer;

/**
 * This object represents the ServersActivations table in database.
 * Each line of the table represents an activation or deactivation.
 * This allow to keep a server in list without using it.
 * Each change of activation done for a server, a line in the table is added
 * with the modification date. The latest record is the current in use.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class ServerActivationDao {
	private final static Logger logger = Logger.getLogger(ServerActivationDao.class);
	private static final String sqlCreate = "CREATE TABLE IF NOT EXISTS ServersActivations "
			+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ " idServer       INTEGER  NOT NULL, "
			+ " activation CHAR(1)  NOT NULL, "
			+ " date           CHAR(24) NOT NULL "
			+ " ); ";

	private static ServerActivationDao saDao = null;
	private Statement statement = null;

	private ServerActivationDao(Statement s) {
		// dm.getStatement() can't be null
		// when Dao is instantiated by dm.openDatabase(dbFileName)
		statement = s;

		try {
			statement.executeUpdate(sqlCreate);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public static ServerActivationDao getInstance(Statement s) {
		if (saDao == null)
			saDao = new ServerActivationDao(s);
		return saDao;
	}
	
	/** Insertion method.
	 * @param s The server with its new activation status.
	 * @param time The modification date.
	 */
	public void insert(LoraWanServer s, String time) {
		int id = s.getId();
		if(id != 0) {
			StringBuilder str = new StringBuilder();
			String strActivated = s.isActivated() ? "Y" : "N";
			str.append("INSERT INTO ServersActivations( ");
			str.append(" idServer, activation, date ) VALUES ( ");
			str.append(id);
			str.append(", '");
			str.append(strActivated);
			str.append("', '");
			str.append(time);
			str.append("' ); ");
			try {
				if(statement.executeUpdate(str.toString()) != 1) {
					logger.error("LoraWan Server Activation not inserted in database");
				} else {
					logger.info("LoraWan Server Activation inserted in database for server id = " + id);
				}
			} catch (SQLException e) {
				logger.error(e.getMessage());
			}
		} else {
			logger.error("LoraWan Server Activation insertion cannot be done in database because LoraWanServer has not an id");
		}
	}
}
