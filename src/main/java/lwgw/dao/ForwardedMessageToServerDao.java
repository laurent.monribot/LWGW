package lwgw.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Logger;

import lwgw.databaseoperations.ForwardedMessageToServer;

/**
 * This is the DAO of the object that represents the relation between the
 * GatewayRecord table and the LoraWanServers table in database.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class ForwardedMessageToServerDao {
	private final static Logger logger = Logger.getLogger(ForwardedMessageToServerDao.class);
	private static final String sqlCreate = "CREATE TABLE IF NOT EXISTS ForwardedMessages "
			+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT, " + " idRecord      INTEGER  NOT NULL, "
			+ " idServer      INTEGER      NOT NULL, "
			+ " date           CHAR(24) NOT NULL ); ";

	private static ForwardedMessageToServerDao fmDao = null;
	private Statement statement = null;

	private ForwardedMessageToServerDao(Statement s) {
		// dm.getStatement() can't be null
		// when Dao is instantiated by dm.openDatabase(dbFileName)
		statement = s;

		try {
			statement.executeUpdate(sqlCreate);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public static ForwardedMessageToServerDao getInstance(Statement s) {
		if(fmDao == null)
			fmDao = new ForwardedMessageToServerDao(s);
		return fmDao;
	}

	public void insert(ForwardedMessageToServer fm) {
		StringBuilder str = new StringBuilder();
		str.append("insert into ForwardedMessages( idRecord, idServer, date ) values ( ");
		str.append(fm.getIdRecord());
		str.append(", ");
		str.append(fm.getIdServer());
		str.append(", '");
		str.append(fm.getCreationDate());
		str.append("' ); ");
		try {
			statement.executeUpdate(str.toString());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public void display() {
		try {
			ResultSet rs = statement.executeQuery("select * from ForwardedMessages;");
			while (rs.next()) {
				// read the result set
				logger.info("idRecord = " + rs.getInt("idRecord")
						+ "idServer = " + rs.getInt("idServer")
						+ " date = " + rs.getString("date"));
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}
}
