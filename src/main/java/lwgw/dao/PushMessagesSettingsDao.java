package lwgw.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import lwgw.settings.LoraWanServer;

/**
 * Abstract DAO class for whatever json schemas tables
 * @author Laurent Monribot
 * @version 1.0.0
 */
public abstract class PushMessagesSettingsDao {
	private final static Logger logger = Logger.getLogger(PushMessagesSettingsDao.class);
	protected static final String sqlCreatePart1 = "CREATE TABLE IF NOT EXISTS ";
	protected static final String sqlCreatePart2 = " (ID INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ " idServer       INTEGER  NOT NULL, "
			+ " dateBegin      CHAR(24) NOT NULL, "
			+ " jsonSchema     TEXT ); ";

	private Statement statement = null;

	protected PushMessagesSettingsDao(Statement s) {
		// dm.getStatement() can't be null
		// when Dao is instantiated by dm.openDatabase(dbFileName)
		statement = s;

		init();
		try {
			statement.executeUpdate(getSqlCreate());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	protected final void insert(LoraWanServer s, String modificationDate, String schema) {
		if(schema == null) schema = "";
		StringBuilder str = new StringBuilder();
		int id = 0;
		str.append("INSERT INTO ");
		str.append(getTableName());
		str.append(" ( idServer, dateBegin, jsonSchema ) VALUES( ");
		str.append(s.getId()); str.append(", '");
		str.append(modificationDate); str.append("', '");
		str.append(schema); str.append("' ); ");
		try {
			if (statement.executeUpdate(str.toString()) == 1) {
				ResultSet generatedKeys = statement.executeQuery("SELECT last_insert_rowid()");
				if (generatedKeys.next()) {
					id = generatedKeys.getInt(1);
				}
			}
			logger.debug("JsonShema inserted in database of id = " + id);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	protected String read(LoraWanServer s) {
		String result = null;
		try {
			StringBuilder str = new StringBuilder();
			str.append("SELECT * FROM ");
			str.append(getTableName());
			str.append(" WHERE idServer = ");
			str.append(s.getId());
			str.append(" ORDER BY dateBegin DESC LIMIT 1 ;");

			ResultSet rs = statement.executeQuery(str.toString());
			while (rs.next()) {
				result = rs.getString("jsonSchema");
				logger.debug("Json last modification date : " + rs.getString("dateBegin"));
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return result;
	}
	
	protected abstract void setTableNameAndSqlCreate(String tableName);
	protected abstract String getTableName();
	protected abstract String getSqlCreate();
	
	/**
	 * Function used to set the tableName member.
	 * Makes the query for table creation also.
	 */
	protected final void init() {
		StringBuilder strBTableName = new StringBuilder();
		strBTableName.append(this.getClass().getName());
		int index = strBTableName.lastIndexOf(".");
		index = (index > 0) ? index+1 : 0;
		String strTableName = strBTableName.substring(index, strBTableName.length()-3);
		setTableNameAndSqlCreate(strTableName);
	}

}
