package lwgw.dao;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

/**
 * This is the database manager of the gateway.
 * It initializes all DAOs at opening and provide them to other objects of the application.
 * It works with a SQLITE 3 database.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class DatabaseManager {
	public final static String DEFAULT_DATE_STRING_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	private final static Logger logger = Logger.getLogger(DatabaseManager.class);
	private static DatabaseManager dm = null;

	private Connection connection;
	private Statement statement;
	private GatewayRecordDao grDao;
	private ServerDao sDao;
	private LoraMessageSettingsDao lsDao;
	private ForwardedMessageToServerDao fmtsDao;
	private ServerActivationDao saDao;
	private PushRadioMessagesDao prmDao;
	private PushStatMessagesDao psmDao;
	private ServerProtocolVersionDao spvDao;
	private ServerCommandDao scDao;
	private LoraSettingsChangeDao lscDao;

	private DatabaseManager() {
		connection = null;
		statement = null;
		grDao = null;
		fmtsDao = null;
		saDao = null;
		prmDao = null;
		psmDao = null;
		spvDao = null;
		scDao = null;
	}

	private void databaseConnection(String fileName) {
		String url = "jdbc:sqlite:" + fileName;

		if (connection != null)
			closeDatabase();
		try {
//			SQLiteConfig sqLiteConfig = new SQLiteConfig();
//			Properties properties = sqLiteConfig.toProperties();
//			properties.getProperty(Pragma.DATE_STRING_FORMAT.pragmaName, DEFAULT_DATE_STRING_FORMAT);
//			connection = DriverManager.getConnection(url, properties);
			
			connection = DriverManager.getConnection(url);
			
			if (connection != null) {
				DatabaseMetaData meta = connection.getMetaData();				
				logger.info("Database to open is " + fileName);
				logger.info("The driver name is " + meta.getDriverName());
				logger.info("The database version is " + meta.getDatabaseProductVersion());
			}
		} catch (SQLException e) {
			// If the error message is "out of memory",
			// it probably means no database file is found
			logger.error(e.getMessage());
			try {
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException e2) {
				// Connection close failed.
				logger.error(e2.getMessage());
			}
		}
	}

	public boolean openDatabase(String fileName) {
		boolean returnValue = true;
		try {
			// load the sqlite-JDBC driver using the current class loader
			Class.forName("org.sqlite.JDBC");

			databaseConnection(fileName);
			if (connection != null) {
				try {
					statement = connection.createStatement();
					statement.setQueryTimeout(3); // set timeout to 3 sec.

					grDao = GatewayRecordDao.getInstance(statement);
					prmDao = PushRadioMessagesDao.getInstance(statement);
					psmDao = PushStatMessagesDao.getInstance(statement);
					spvDao = ServerProtocolVersionDao.getInstance(statement);
					sDao = ServerDao.getInstance(statement, prmDao, psmDao, spvDao);
					lsDao = LoraMessageSettingsDao.getInstance(statement);
					fmtsDao = ForwardedMessageToServerDao.getInstance(statement);
					saDao = ServerActivationDao.getInstance(statement);
					scDao = ServerCommandDao.getInstance(statement);
					lscDao = LoraSettingsChangeDao.getInstance(statement);
					
					if(grDao==null || sDao==null || lsDao==null
							|| fmtsDao==null || saDao==null
							|| prmDao==null || psmDao==null
							|| spvDao==null || scDao==null
							|| lscDao==null) {
						logger.error("DAO not found");
					}
				} catch (SQLException e) {
					logger.error(e.getMessage());
					returnValue = false;
				}
			}
		} catch (ClassNotFoundException notFoundException) {
			logger.error("Sqlite not installed : launch \"apt-get install sqlite\"");
			returnValue = false;
		}
		return returnValue;
	}

	public void closeDatabase() {
		try {
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		statement = null;
		connection = null;
	}

	public static DatabaseManager getInstance() {
		if (dm == null)
			dm = new DatabaseManager();
		return dm;
	}

	public GatewayRecordDao getGatewayRecordDao() {
		return grDao;
	}
	
	public ServerDao getServerDao() {
		return sDao;
	}
	
	public LoraMessageSettingsDao getLoraMessageSettingsDao() {
		return lsDao;
	}
	
	public ForwardedMessageToServerDao getForwardedMessageToServerDao() {
		return fmtsDao;
	}

	public ServerActivationDao getServerActivationDao() {
		return saDao;
	}

	public PushRadioMessagesDao getPushRadioMessagesDao() {
		return prmDao;
	}

	public PushStatMessagesDao getPushStatMessagesDao() {
		return psmDao;
	}

	public ServerProtocolVersionDao getServerProtocolVersionDao() {
		return spvDao;
	}
	public ServerCommandDao getServerCommandDao() {
		return scDao;
	}

	public LoraSettingsChangeDao getLoraSettingsChangeDao() {
		return lscDao;
	}
	
}