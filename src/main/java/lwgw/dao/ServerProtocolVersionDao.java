package lwgw.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import lwgw.settings.LoraWanServer;

/**
 * This object represents the ServersProtocols table in database.
 * Each line of the table represents a LoraWan protocol version in use by a server.
 * For now, only protocols versions 1 and 2 are recognized as it is described
 * in the Semtech documentation : ANNWS.01.2.1.W.SYS "Gateway to server interface".
 * Each change of protocol version done for a server, a line in the table is added
 * with the modification date. The latest record is the current in use.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class ServerProtocolVersionDao {
	private final static Logger logger = Logger.getLogger(ServerProtocolVersionDao.class);
	private static final String sqlCreate = "CREATE TABLE IF NOT EXISTS ServersProtocols "
			+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ " idServer       INTEGER  NOT NULL, "
			+ " versProtocol   INTEGER  NOT NULL, "
			+ " date           CHAR(24) NOT NULL "
			+ " ); ";

	private static ServerProtocolVersionDao spvDao = null;
	private Statement statement = null;

	private ServerProtocolVersionDao(Statement s) {
		// dm.getStatement() can't be null
		// when Dao is instantiated by dm.openDatabase(dbFileName)
		statement = s;

		try {
			statement.executeUpdate(sqlCreate);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public static ServerProtocolVersionDao getInstance(Statement s) {
		if (spvDao == null)
			spvDao = new ServerProtocolVersionDao(s);
		return spvDao;
	}
	
	/** Insertion method.
	 * @param s The server with its new protocol version.
	 * @param time The modification date.
	 */
	public void insert(LoraWanServer s, String time) {
		int id = s.getId();
		if(id != 0) {
			StringBuilder str = new StringBuilder();
			str.append("INSERT INTO ServersProtocols ( ");
			str.append(" idServer, versProtocol, date ) VALUES ( ");
			str.append(id);
			str.append(", ");
			str.append(s.getProtocolVersion());
			str.append(", '");
			str.append(time);
			str.append("' ); ");
			try {
				if(statement.executeUpdate(str.toString()) != 1) {
					logger.error("LoraWan Server Protocol Version not inserted in database");
				} else {
					logger.info("LoraWan Server Protocol Version inserted in database for server id = " + id);
				}
			} catch (SQLException e) {
				logger.error(e.getMessage());
			}
		} else {
			logger.error("LoraWan Server Protocol Version insertion cannot be done in database because LoraWanServer has not an id");
		}
	}
	
	protected int read(LoraWanServer s) {
		int result = 0;
		try {
			StringBuilder str = new StringBuilder();
			str.append("SELECT * FROM ServersProtocols WHERE idServer = ");
			str.append(s.getId());
			str.append(" ORDER BY date DESC LIMIT 1 ;");

			ResultSet rs = statement.executeQuery(str.toString());
			while (rs.next()) {
				result = (rs.getInt("versProtocol"));
				logger.debug("Protocol last modification date : " + rs.getString("date"));
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return result;
	}
}
