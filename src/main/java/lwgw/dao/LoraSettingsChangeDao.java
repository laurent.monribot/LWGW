package lwgw.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Logger;

import lwgw.settings.LoraRadioSettings;
import lwgw.settings.LoraWanServer;
import lwgw.upstreamdata.GatewayRecord;

/**
 * This object represents the LoraSettingsChanges table in database.
 * This table keeps each modification of LoraSettings with the date and with the server.
 * Each time a change of lora radio setting is done, a line in the table
 * is added with the modification date. The latest record is the current
 * in use.

 * @author Laurent Monribot
 * @version 1.0.0
 */
public class LoraSettingsChangeDao {
	private final static Logger logger = Logger.getLogger(LoraSettingsChangeDao.class);
	private static final String sqlCreate =
			"CREATE TABLE IF NOT EXISTS LoraSettingsChanges " + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ " freq           REAL     NOT NULL, " + " chan           INTEGER  NOT NULL, "
			+ " rfch           INTEGER  NOT NULL, " + " stat           INTEGER  NOT NULL, "
			+ " codr           CHAR(3)  NOT NULL, " + " modu           CHAR(4)  NOT NULL, "
			+ " bandw          INTEGER  NOT NULL, " + " sf             INTEGER  NOT NULL, "
			+ " power          INTEGER  NOT NULL, " + " date           CHAR(24) NOT NULL, "
			+ " idServer       INTEGER  NOT NULL  " + " ); ";

	private static LoraSettingsChangeDao lscDao = null;
	private Statement statement = null;

	private LoraSettingsInCache loraSettingsInCache = null;

	private LoraSettingsChangeDao(Statement s) {
		// dm.getStatement() can't be null
		// when Dao is instantiated by dm.openDatabase(dbFileName)
		statement = s;

		try {
			statement.executeUpdate(sqlCreate);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public static LoraSettingsChangeDao getInstance(Statement s) {
		if (lscDao == null)
			lscDao = new LoraSettingsChangeDao(s);
		return lscDao;
	}

	public int insert(LoraRadioSettings lrs, String time, LoraWanServer s) {
		int id = 0;
		
		id = readCurrentSettings(lrs);
		if(id != 0 && loraSettingsInCache.equalsTo(lrs)) {
			return id;
		}
		
		StringBuilder str = new StringBuilder();
		str.append("insert into LoraSettingsChanges(");
		str.append("freq, chan, rfch, stat, ");
		str.append("codr, modu, bandw, sf, power, date, idServer ");
		str.append(") values(");
		str.append(lrs.getFrequency());
		str.append(", 0, 0, 1, '");
		str.append(lrs.getCodeRedundancy());
		str.append("', 'LORA', ");
		str.append(lrs.getBandwidth());
		str.append(", ");
		str.append(lrs.getSpreadingFactor());
		str.append(", ");
		str.append(lrs.getPower());
		str.append(", '");
		str.append(time);
		str.append("', ");
		if(s!=null) str.append(s.getId()); else str.append("0");
		str.append(" ); ");
		
		try {
			if(statement.executeUpdate(str.toString()) == 1) {
				ResultSet generatedKeys = statement.executeQuery("SELECT last_insert_rowid()");
				if (generatedKeys.next()) {
					id = generatedKeys.getInt(1);
				}
			}
			loraSettingsInCache = new LoraSettingsInCache(lrs, id);
			logger.debug("Change of Lora Radio Listening Settings inserted in database of id = " + id);

		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return id;
	}

	private int readCurrentSettings(LoraRadioSettings lrs) {
		
		// Use the loraSettingsInCache first if it is existing :
		if(loraSettingsInCache != null
			&& loraSettingsInCache.equalsTo(lrs)) {
			return loraSettingsInCache.getId();
		}
		
		int id = 0;
		try {
			StringBuilder str = new StringBuilder();
			str.append("SELECT * FROM LoraSettingsChanges ");
			str.append(" ORDER BY date DESC LIMIT 1 ; ");
			
			ResultSet rs = statement.executeQuery(str.toString());
			while (rs.next()) {
				// Read all values to keep them as a cache :
				loraSettingsInCache = new LoraSettingsInCache(rs, LoraSettingsInCache.LORA_SETTINGS_CHANGE);
				// Read the result set we are interested in :
				id = loraSettingsInCache.getId();
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return id;
	}
	
	
	
}
