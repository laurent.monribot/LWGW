package lwgw.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import lwgw.databaseoperations.ServerCommand;

/**
* Dao for commands received from a server.
* @author Laurent Monribot
* @version 1.0.0
*/
public class ServerCommandDao {
	private final static Logger logger = Logger.getLogger(ServerCommandDao.class);
	private static final String sqlCreate = "CREATE TABLE IF NOT EXISTS ServersCommands "
			+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ " idServer       INTEGER   NOT NULL, "
			+ " performed      CHAR(1)   NOT NULL, "
			+ " errExplanation CHAR(16)  , "
			+ " date           CHAR(24)  NOT NULL, "
			+ " contents       CHAR(200) NOT NULL "
			+ " ); ";
	
	
	private static ServerCommandDao scDao = null;
	private Statement statement = null;

	private ServerCommandDao(Statement s) {
		// dm.getStatement() can't be null
		// when Dao is instanciated by dm.openDatabase(dbFileName)
		statement = s;

		try {
			statement.executeUpdate(sqlCreate);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public static ServerCommandDao getInstance(Statement s) {
		if (scDao == null)
			scDao = new ServerCommandDao(s);
		return scDao;
	}
	
	public void insert(ServerCommand c) {
		String strPerformed = c.isPerformed() ? "Y" : "N";
		String content = c.getContents().replace("\"", "").replace(",", " ").replace("{","").replace("}", "");
		StringBuilder str = new StringBuilder();
		str.append("INSERT INTO ServersCommands ( ");
		str.append("idServer, performed, errExplanation, date, contents ) VALUES ( ");
		str.append(c.getServer().getId());
		str.append(", '");
		str.append(strPerformed);
		str.append("', '");
		str.append(c.getErrorExplanation());
		str.append("', '");
		str.append(c.getDate());
		str.append("', '");
		str.append(content);
		str.append("' ); ");
		

		try {
			if(statement.executeUpdate(str.toString()) == 1) {
				ResultSet generatedKeys = statement.executeQuery("SELECT last_insert_rowid()");
				if (generatedKeys.next()) {
					c.setId(generatedKeys.getInt(1));
				}
			}
			logger.debug("ServerCommand inserted in database of id = " + c.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}
}
