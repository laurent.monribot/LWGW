package lwgw.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Logger;
import lwgw.upstreamdata.GatewayRecord;

/**
 * This object represents the LoraMessagesSettings table in database.
 * This table is referenced by each Lora radio message received to know how it was sent.
 * Since our gateway is single channeled, only one line is referenced by messages
 * all along the time Lora radio listening settings are remaining unchanged.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class LoraMessageSettingsDao {
	private final static Logger logger = Logger.getLogger(LoraMessageSettingsDao.class);
	private static final String sqlCreate =
			"CREATE TABLE IF NOT EXISTS LoraMessagesSettings " + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ " freq           REAL     NOT NULL, " + " chan           INTEGER  NOT NULL, "
			+ " rfch           INTEGER  NOT NULL, " + " stat           INTEGER  NOT NULL, "
			+ " codr           CHAR(3)  NOT NULL, " + " modu           CHAR(4)  NOT NULL, "
			+ " datr           CHAR(10) NOT NULL); ";

	private static LoraMessageSettingsDao lsDao = null;
	private Statement statement = null;
	
	private LoraSettingsInCache loraSettingsInCache = null;
	
	
	private LoraMessageSettingsDao(Statement s) {
		// dm.getStatement() can't be null
		// when Dao is instantiated by dm.openDatabase(dbFileName)
		statement = s;

		try {
			statement.executeUpdate(sqlCreate);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public static LoraMessageSettingsDao getInstance(Statement s) {
		if (lsDao == null)
			lsDao = new LoraMessageSettingsDao(s);
		return lsDao;
	}

	public int insert(GatewayRecord gr) {
		int id = 0;
		StringBuilder str = new StringBuilder();
		str.append("insert into LoraMessagesSettings ( ");
		str.append("freq, chan, rfch, stat, ");
		str.append("codr, modu, datr) values( ");
		str.append(gr.getFreq());
		str.append(", ");
		str.append(gr.getChan());
		str.append(", ");
		str.append(gr.getRfch());
		str.append(", ");
		str.append(gr.getStat());
		str.append(", '");
		str.append(gr.getCodr());
		str.append("', '");
		str.append(gr.getModu());
		str.append("', '");
		str.append(gr.getDatr());
		str.append("' ); ");

		try {
			if(statement.executeUpdate(str.toString()) == 1) {
				ResultSet generatedKeys = statement.executeQuery("SELECT last_insert_rowid()");
				if (generatedKeys.next()) {
					id = generatedKeys.getInt(1);
				}
			}
			logger.debug("Lora Radio Listening Settings inserted in database of id = " + id);

		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return id;
	}

	public int read(GatewayRecord gr) {
		
		// Use the loraSettingsInCache first if it is existing :
		if(loraSettingsInCache != null
			&& loraSettingsInCache.equalsTo(gr)) {
			return loraSettingsInCache.getId();
		}
		
		int id = 0;
		try {
			StringBuilder str = new StringBuilder();
			str.append("select * from LoraMessagesSettings ");
			str.append(" where freq = ");
			str.append(gr.getFreq());
			str.append(" and chan = ");
			str.append(gr.getChan());
			str.append(" and rfch = ");
			str.append(gr.getRfch());
			str.append(" and stat = ");
			str.append(gr.getStat());
			str.append(" and codr = '");
			str.append(gr.getCodr());
			str.append("' and modu = '");
			str.append(gr.getModu());
			str.append("' and datr = '");
			str.append(gr.getDatr());
			str.append("' ; ");
			
			ResultSet rs = statement.executeQuery(str.toString());
			while (rs.next()) {
				// Read all values to keep them as a cache :
				loraSettingsInCache = new LoraSettingsInCache(rs, LoraSettingsInCache.LORA_MESSAGE_SETTINGS);
				// Read the result set we are interested in :
				id = loraSettingsInCache.getId();
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return id;
	}
	
	
	
}
