package lwgw.dao;

import java.sql.Statement;

import lwgw.settings.LoraWanServer;

/**
 * DAO class for json schemas that are used to encapsulate radio messages.
 * If no record is present, then default json encapsulation expected in Semtech 
 * document : ANNWS.01.2.1.W.SYS "Gateway to server interface" is used.
 * Each time a change of json schema is done for a server, a line in the table
 * is added with the modification date. The latest record is the current in use.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class PushRadioMessagesDao extends PushMessagesSettingsDao {
	private static PushRadioMessagesDao prmDao = null;
	private static String sqlCreate = null;
	private static String tableName = null;

	private PushRadioMessagesDao(Statement s) {
		super(s);
	}

	public static PushRadioMessagesDao getInstance(Statement s) {
		if (prmDao == null) {
			prmDao = new PushRadioMessagesDao(s);
		}
		return prmDao;

	}

	/** Insertion method.
	 * @param s The server with its new Lora radio message encapsulation.
	 * @param modificationDate The modification date.
	 */
	//Common code put in mother class
	public void insert(LoraWanServer s, String modificationDate) {
		super.insert(s, modificationDate, s.getRadioJsonSchema());
	}

	//Common code put in mother class
	@Override
	public String read(LoraWanServer s) {
		String result = super.read(s);
		return result;
	}
	
	//Method used by the mother class to set some fields
	//since these fields are declared in the daughter class
	@Override
	protected void setTableNameAndSqlCreate(String tableName) {
		PushRadioMessagesDao.tableName = tableName;
		sqlCreate = sqlCreatePart1 + tableName + sqlCreatePart2;
	}
	
	//Method used by the mother class to retrieve the field
	//since the field is declared in the daughter class
	@Override
	protected String getTableName(){
		return PushRadioMessagesDao.tableName;
	}
	
	//Method used by the mother class to retrieve the field
	//since the field is declared in the daughter class
	@Override
	protected String getSqlCreate(){
		return PushRadioMessagesDao.sqlCreate;
	}
}
