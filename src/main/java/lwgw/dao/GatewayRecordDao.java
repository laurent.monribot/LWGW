package lwgw.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import lwgw.upstreamdata.GatewayRecord;

/**
 * This object represents the the GatewayRecord table in database.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class GatewayRecordDao {
	private final static Logger logger = Logger.getLogger(GatewayRecordDao.class);
	private static final String sqlCreate =
			"CREATE TABLE IF NOT EXISTS GatewayRecords " + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ " number         INTEGER  NOT NULL, " + " token          INTEGER      NOT NULL, "
			+ " rssi           INTEGER  NOT NULL, " + " snr            INTEGER      NOT NULL, "
			+ " loraSettings   INTEGER  NOT NULL, "
			+ " date           CHAR(24) NOT NULL, "
			+ " radioMessage   TEXT     NOT NULL ); ";

	private static GatewayRecordDao grDao = null;
	private Statement statement = null;

	private GatewayRecordDao(Statement s) {
		// dm.getStatement() can't be null
		// when Dao is instanciated by dm.openDatabase(dbFileName)
		statement = s;

		try {
			statement.executeUpdate(sqlCreate);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public static GatewayRecordDao getInstance(Statement s) {
		if (grDao == null)
			grDao = new GatewayRecordDao(s);
		return grDao;
	}

	public void insert(GatewayRecord gr) {				
		StringBuilder str = new StringBuilder();
		str.append("INSERT INTO GatewayRecords ( ");
		str.append("number, token, rssi, snr, loraSettings, ");
		str.append("date, radioMessage ) VALUES ( ");
		str.append(gr.getNumber());
		str.append(", ");
		str.append(gr.getToken());
		str.append(", ");
		str.append(gr.getRssi());
		str.append(", ");
		str.append(gr.getLsnr());
		str.append(", ");
		str.append(gr.getLoraMessageSettingsId()); //lora settings, index in another table
		str.append(", '");
		str.append(gr.getCreationDate());
		str.append("', '");
		str.append(gr.toStringHexDumpRadioMsgShort());
		str.append("' ); ");
		try {
			if(statement.executeUpdate(str.toString()) == 1) {
				ResultSet generatedKeys = statement.executeQuery("SELECT last_insert_rowid()");
				if (generatedKeys.next()) {
					gr.setId(generatedKeys.getInt(1));
				}
			}
			logger.debug("GatewayRecord inserted in database of id = " + gr.getId());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public void display() {
		try {
			ResultSet rs = statement.executeQuery("select * from GatewayRecords;");
			while (rs.next()) {
				// Read the result set
				logger.info("number = " + rs.getInt("number") +
						" token = " + rs.getInt("token") +
						" RSSI = " + rs.getInt("rssi") +
						" SNR = " + rs.getInt("snr") +
						" id loraSettings = " + rs.getString("loraSettings") +
						" date = " + rs.getString("date") +
						" radioMessage = " + rs.getString("radioMessage")
						);
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}
}
