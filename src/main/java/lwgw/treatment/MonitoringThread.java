package lwgw.treatment;


import java.io.IOException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lwgw.customizedformat.FieldsValuesToSet;
import lwgw.customizedformat.GeneratedClass;
import lwgw.customizedformat.ValuesIntoJsonConverter;
import lwgw.jmx.JmxManager;
import lwgw.lorawan.ComManager;
import lwgw.settings.LoraWanServer;
import lwgw.settings.LwgwSettings;
import lwgw.treatment.context.GatewayExecutionContext;
import lwgw.upstreamdata.LoraWanMessage;
import lwgw.upstreamdata.defaultmessagesformats.JsonStatContainer;

/**
 * This object is the monitoring thread of the application.
 * It is woken up each second for checking some values and decides
 * if a treatment is required, like for example, sending statistics
 * messages to servers. It uses a {@link ComManager} object.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class MonitoringThread implements Runnable {
	private static final int MAX_PERCENTAGE = 100;
	private static final int SLEEPING_MONITOR_DURATION = 1000;
	private ThreadPoolExecutor executor;
	private LwgwSettings lwgwSettings;
	private ArrayList<LoraWanServer> serversList;
	private ArrayList<LoraWanServer> serversListTmp;
	private final static Logger logger = Logger.getLogger(MonitoringThread.class);
	private boolean refreshServersList;
	private GatewayExecutionContext context;
	private byte[] macAddress;

	public MonitoringThread(ThreadPoolExecutor executor, LwgwSettings lwgwSettings, DatabaseThread g, GatewayExecutionContext context, byte[] macAddress) {
		this.executor = executor;
		this.lwgwSettings = lwgwSettings;

		serversList = null;
		serversListTmp = null;
		refreshServersList = false;
		this.context = context;
		this.macAddress = macAddress;
	}
	@Override
	public void run() {
		int monitoringCounter = 0;
		int statisticsCounter = 0;
		
		while (lwgwSettings.getState() != JmxManager.LWGW_STATE_STOP) {
			try {
				Thread.sleep(SLEEPING_MONITOR_DURATION);
			} catch (InterruptedException e) {
				logger.error(e.getMessage());
			}
			if (monitoringCounter >= lwgwSettings.getMonitoringDelayInterval()) {
				logger.info(String.format(
						"[monitor] [%d/%d] Active: %d, Completed: %d, Task: %d, isShutdown: %s, isTerminated: %s",
						this.executor.getPoolSize(), this.executor.getCorePoolSize(), this.executor.getActiveCount(),
						this.executor.getCompletedTaskCount(), this.executor.getTaskCount(), this.executor.isShutdown(),
						this.executor.isTerminated()));
				monitoringCounter = 0;
			}

			if (statisticsCounter >= lwgwSettings.getStatMessageDelayInterval()) {
				logger.debug("statMessageRequested");

				if (serversList != null) {
					for (LoraWanServer s : serversList) {
						if (s.isActivated()) {
							boolean serverResponse = false;
							ComManager cm = new ComManager(lwgwSettings, context);
							LoraWanMessage m = new LoraWanMessage(makeStatServerMsg(s), s);
							serverResponse = cm.sendWithRetry(m.getServerMsg(), s, null);
							logger.debug("PUSH STAT : " + m.toString() + " Ack : " + serverResponse);
						}
					}
				}			
				statisticsCounter = 0;
			}
			
			if (refreshServersList) {
				refreshServersList = false;
				serversList = serversListTmp;
				serversListTmp = null;
				logger.debug("Servers List refreshed");
			}
			
			monitoringCounter++;
			statisticsCounter++;
		}
		logger.info("LWGW STATE : " + lwgwSettings.getState());
	}
	
	private byte[] makeStatServerMsgHeader(LoraWanServer server) {
		byte[] header = new byte[12];

		Random rand = new Random();
		int nRandom = rand.nextInt(256);
		short token = (short) ((0xFF << 8) | nRandom);

		header[0] = server.getProtocolVersion();

		header[1] = (byte) (token & 0xFF);
		header[2] = (byte) (token >> 8);

		header[3] = lwgw.lorawan.Constants.MES_ID_PUSH_DATA;

		header[4] = macAddress[0];
		header[5] = macAddress[1];
		header[6] = macAddress[2];
		header[7] = (byte) 0xFF;
		header[8] = (byte) 0xFF;
		header[9] = macAddress[3];
		header[10] = macAddress[4];
		header[11] = macAddress[5];

		return header;
	}

	private byte[] makeStatServerMsg(LoraWanServer s) {
		String jsonInString;
		JsonStatContainer stat;
		byte[] header, content, destination = null;
		int reliability = MAX_PERCENTAGE;
		List<GeneratedClass> generatedClasses = null;
		
		try {
			if (context.getNumberRadioMessagesForwarded() != 0) {
				reliability = (int) (MAX_PERCENTAGE * context.getNumberRadioMessagesAcknowledged()
						/ context.getNumberRadioMessagesForwarded());
			}
			if((generatedClasses = s.getStatGeneratedClasses()) == null) {
				ObjectMapper mapper = new ObjectMapper();
				stat = new JsonStatContainer(context.getNumberRadioMessagesReceived(),
						// with correct CRC (but we do not receive incorrect messages)
						// so it is the same value passed as the previous one...
						context.getNumberRadioMessagesReceived(),
						context.getNumberRadioMessagesForwarded(),
						reliability,
						context.getNumberRadioMessagesGotFromServer(),
						context.getNumberRadioMessagesEmitted());
				// Default Semtech format :
				jsonInString = mapper.writeValueAsString(stat);
			} else {
				// Personalized format :
				FieldsValuesToSet fvts = new FieldsValuesToSetForStat(context, reliability, lwgwSettings);
				logger.debug("FieldsValuesToSet : " + fvts);
				
				jsonInString = ValuesIntoJsonConverter.makeJson(generatedClasses, fvts);
				logger.debug("Json made with a generated class : " + jsonInString);
				
			}
			header = makeStatServerMsgHeader(s);
			content = jsonInString.getBytes();

			// create a destination array that is the size of the two arrays
			destination = new byte[header.length + content.length];
			System.arraycopy(header, 0, destination, 0, header.length);
			System.arraycopy(content, 0, destination, header.length, content.length);
			
		} catch (JsonGenerationException e) {
			logger.error("JsonGenerationException : " + e.getMessage());
		} catch (JsonMappingException e) {
			logger.error("JsonMappingException : " + e.getMessage());
		} catch (IOException e) {
			logger.error("IOException : " + e.getMessage());
		}
		return destination;
	}
	
	private static class FieldsValuesToSetForStat extends FieldsValuesToSet {
		private TreeMap<String,Object[]> map;

	    private Object paramsObjSetTime[];
	    private Object paramsObjSetLati[];
	    private Object paramsObjSetLong[];
	    private Object paramsObjSetAlti[];
	    private Object paramsObjSetRxnb[];
	    private Object paramsObjSetRxok[];
	    private Object paramsObjSetRwfw[];
	    private Object paramsObjSetAckr[];
	    private Object paramsObjSetDwnb[];
	    private Object paramsObjSetTxnb[];
	    
	    FieldsValuesToSetForStat(GatewayExecutionContext context, int reliability, LwgwSettings lwgwSettings) {
		    paramsObjSetTime = new Object[1];
		    paramsObjSetLati = new Object[1];
		    paramsObjSetLong = new Object[1];
		    paramsObjSetAlti = new Object[1];
		    paramsObjSetRxnb = new Object[1];
		    paramsObjSetRxok = new Object[1];
		    paramsObjSetRwfw = new Object[1];
		    paramsObjSetAckr = new Object[1];
		    paramsObjSetDwnb = new Object[1];
		    paramsObjSetTxnb = new Object[1];

			// never forget GMT at the end in order to be acknowledged by TTN
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss' GMT'");
			TimeZone tz = TimeZone.getTimeZone("UTC");
			df.setTimeZone(tz);
			String time = df.format(new Date());
			
			// Coordinates with 5 decimals
			double lati = new Double(lwgwSettings.getLatitude());
			double lgtd = new Double(lwgwSettings.getLongitude());
			double alti = new Double(lwgwSettings.getAltitude());

			paramsObjSetTime[0] = time;
			map.put("Time", paramsObjSetTime);
		    paramsObjSetLati[0] = lati;
			map.put("Lati", paramsObjSetLati);
		    paramsObjSetLong[0] = lgtd;
			map.put("Long", paramsObjSetLong);
		    paramsObjSetAlti[0] = alti;
			map.put("Alti", paramsObjSetAlti);
		    paramsObjSetRxnb[0] = context.getNumberRadioMessagesReceived();
			map.put("Rxnb", paramsObjSetRxnb);
		    paramsObjSetRxok[0] = context.getNumberRadioMessagesReceived();
			map.put("Rxok", paramsObjSetRxok);
		    paramsObjSetRwfw[0] = context.getNumberRadioMessagesForwarded();
			map.put("Rwfw", paramsObjSetRwfw);
		    paramsObjSetAckr[0] = reliability;
			map.put("Ackr", paramsObjSetAckr);
		    paramsObjSetTxnb[0] = context.getNumberRadioMessagesGotFromServer();
			map.put("Txnb", paramsObjSetTxnb);
		    paramsObjSetDwnb[0] = context.getNumberRadioMessagesEmitted();
			map.put("Dwnb", paramsObjSetDwnb);
	    }

		@Override
		public String toString() {
			return "FieldsValuesToSet [paramsObjSetTime=" + Arrays.toString(paramsObjSetTime) + ", paramsObjSetLati="
					+ Arrays.toString(paramsObjSetLati) + ", paramsObjSetLong=" + Arrays.toString(paramsObjSetLong)
					+ ", paramsObjSetAlti=" + Arrays.toString(paramsObjSetAlti) + ", paramsObjSetRxnb="
					+ Arrays.toString(paramsObjSetRxnb) + ", paramsObjSetRxok=" + Arrays.toString(paramsObjSetRxok)
					+ ", paramsObjSetRwfw=" + Arrays.toString(paramsObjSetRwfw) + ", paramsObjSetAckr="
					+ Arrays.toString(paramsObjSetAckr) + ", paramsObjSetDwnb=" + Arrays.toString(paramsObjSetDwnb)
					+ ", paramsObjSetTxnb=" + Arrays.toString(paramsObjSetTxnb) + "]";
		}   
	}
	
	/** Dedicated to making specific messages according each server
	 * @param l The list of servers.
	 */
	public void setServersList(ArrayList<LoraWanServer> l) {
		this.serversListTmp = l;
		refreshServersList = true;		
	}
}
