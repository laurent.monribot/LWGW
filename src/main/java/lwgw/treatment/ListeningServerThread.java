package lwgw.treatment;

import java.util.Queue;
import java.util.Random;

import org.apache.log4j.Logger;

import lwgw.databaseoperations.DatabaseOperation;
import lwgw.lorawan.ComManager;
import lwgw.settings.LoraWanServer;
import lwgw.settings.LwgwSettings;
import lwgw.treatment.context.GatewayExecutionContext;
import lwgw.upstreamdata.LoraWanMessage;
/** This thread is used in a thread pool executor.
 * It is used to deal with only one server.
 * It pulls and listens for a command to treat.
 * It uses a {@link ComManager} object.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class ListeningServerThread implements Runnable {
	private final static Logger logger = Logger.getLogger(ListeningServerThread.class);
	private LwgwSettings lwgwSettings;
	private GatewayExecutionContext context;
	private LoraWanServer server;
	private byte[] macAddress;
	private Queue<DatabaseOperation> dbFifo;
	private Queue<byte[]> radioFifo;
	
	public ListeningServerThread(LoraWanServer s, LwgwSettings lwgwSettings, GatewayExecutionContext context,
			byte[] macAdr, Queue<DatabaseOperation> q, Queue<byte[]> radioQ) {
		this.lwgwSettings = lwgwSettings;
		this.context = context;
		server = s;
		macAddress = macAdr;
		dbFifo = q;
		radioFifo = radioQ;
	}
	
	@Override
	public void run() {	
	boolean serverResponse = false;
	ComManager cm = new ComManager(lwgwSettings, context, dbFifo, radioFifo);
	LoraWanMessage m = new LoraWanMessage(makePullServerMsg(server), server);
	serverResponse = cm.sendWithRetry(m.getServerMsg(), server, null);
	logger.debug("PULL : " + m.toString() + " Ack : " + serverResponse);
	}
	
	private byte[] makePullServerMsg(LoraWanServer server) {
		byte[] header = new byte[12];

		Random rand = new Random();
		int nRandom = rand.nextInt(256);
		short token = (short) ((0xFF << 8) | nRandom);

		header[0] = server.getProtocolVersion();

		header[1] = (byte) (token & 0xFF);
		header[2] = (byte) (token >> 8);

		header[3] = lwgw.lorawan.Constants.MES_ID_PULL_DATA;

		header[4] = macAddress[0];
		header[5] = macAddress[1];
		header[6] = macAddress[2];
		header[7] = (byte) 0xFF;
		header[8] = (byte) 0xFF;
		header[9] = macAddress[3];
		header[10] = macAddress[4];
		header[11] = macAddress[5];

		return header;
	}
}
