package lwgw.treatment.context;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import lwgw.downstreamdata.JsonServerCommand;
import lwgw.treatment.ServerCommandManagerThread;
import lwgw.treatment.TreatingMessageThread;
import lwgw.treatment.DatabaseThread;
import lwgw.treatment.MonitoringThread;

/**
* This object gathers all values that are ​​relative to the current execution.
* This object us used for making Json "stat" messages.
* Described in the <b>Semtech</b> documentation : <b>ANNWS.01.2.1.W.SYS</b>
* "Gateway to server interface". Might be enriched with new values in order
* to comply with further others specifications.
* This single object is referenced by {@link DatabaseThread}, {@link MonitoringThread},
* {@link ServerCommandManagerThread}, and {@link TreatingMessageThread} objects.
* Members are read by the {@link MonitoringThread} object for sending statistics.
* to servers.
* @author Laurent Monribot
* @version 1.0.0
*/
public class GatewayExecutionContext {
	private int numberRadioMessagesReceived;
	private int numberRadioMessagesForwarded;
	private int numberRadioMessagesAcknowledged;
	private int numberRadioMessagesGotFromServer;
	private int numberRadioMessagesEmitted;
	// private int numberRadioMessagesReceivedWithCorrectCrc; should be added
	// but it seems that we get only radio messages already crc checked from hardware interface

	private int[] numberOfMessagesOfLenth;
	
	// All setters of this object are thread safe :
	final ReadWriteLock lock = new ReentrantReadWriteLock();
	
	public GatewayExecutionContext() {
		numberRadioMessagesReceived = 0;
		numberRadioMessagesForwarded = 0;
		numberRadioMessagesAcknowledged = 0;
		numberRadioMessagesGotFromServer = 0;
		numberRadioMessagesEmitted = 0;
		
		// Add over Semtech specification :
		numberOfMessagesOfLenth = new int[300];
		for(int i=0; i<300; i++) {
			numberOfMessagesOfLenth[i] = 0;
		}
	}

    /** This method is used by {@link MonitoringThread} 
	 * @return The number of radio messages received.
	 */
	public int getNumberRadioMessagesReceived() {
		return numberRadioMessagesReceived;
	}

    /** This method is used by {@link DatabaseThread}
     */
	public void incNumberRadioMessagesReceived() {
	    final Lock w = lock.writeLock();
	    w.lock();
	    this.numberRadioMessagesReceived++;
	    w.unlock();
	}

    /** This method is used by {@link MonitoringThread}
 	 * @return The number of radio messages forwarded.
     */
	public int getNumberRadioMessagesForwarded() {
		return numberRadioMessagesForwarded;
	}

    /** This method is used by {@link DatabaseThread}
     */
	public void incNumberRadioMessagesForwarded() {
	    final Lock w = lock.writeLock();
	    w.lock();
	    this.numberRadioMessagesForwarded++;
	    w.unlock();
	}

    /** This method is used by {@link MonitoringThread}
	 * @return The number of radio messages acknowledged.
     */
	public int getNumberRadioMessagesAcknowledged() {
		return numberRadioMessagesAcknowledged;
	}

    /** This method is used by {@link DatabaseThread}
     */
	public void incNumberRadioMessagesAcknowledged() {
	    final Lock w = lock.writeLock();
	    w.lock();
		this.numberRadioMessagesAcknowledged++;
		w.unlock();
	}

    /** This method is used by {@link MonitoringThread}
	 * @return The number of radio messages got from a server.
     */
	public int getNumberRadioMessagesGotFromServer() {
		return numberRadioMessagesGotFromServer;
	}

    /** This method is used by {@link JsonServerCommand}
     */
	public void incNumberRadioMessagesGotFromServer() {
	    final Lock w = lock.writeLock();
	    w.lock();
		this.numberRadioMessagesGotFromServer++;
		w.unlock();
	}

    /** This method is used by {@link MonitoringThread}
	 * @return The number of radio messages broadcasted.
     */
	public int getNumberRadioMessagesEmitted() {
		return numberRadioMessagesEmitted;
	}

    /** This method is used by {@link JsonServerCommand}
     */
	public void incNumberRadioMessagesEmitted() {
	    final Lock w = lock.writeLock();
	    w.lock();
	    this.numberRadioMessagesEmitted++;
	    w.unlock();
	}

}
