package lwgw.treatment.context;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import lwgw.settings.LoraWanServer;

/**
* This object gathers all statistics values that are relative to a {@link LoraWanServer}
* all along the life of the application. It belongs to a server and is shared by
* several references representing the same server, like the internal object
* application, its adapter in the JMX JConsole and other (old) references if the
* server just became updated. It is a thread safe object because of the number of
* simultaneous threads that are sending encapsulated Lora radio messages and are
* changing acknowledgments counters or errors counters.
* @author Laurent Monribot
* @version 1.0.0
*/
public class LoraWanServerStatistics {
	private int nbMesSent;
	private int nbMesAcknowledged;
	// this to know if it is really needed
	// to retry not acknowledged messages
	// in case the server seems to be out of service :
	private int nbLastConsecutiveMesNotAck;

	// All setters of this object are thread safe :
	final ReadWriteLock lock = new ReentrantReadWriteLock();
	
	public LoraWanServerStatistics () {
		resetAllSatistics();
	}

	public int getNbMesSent() {
		return nbMesSent;
	}

	public void setNbMesSent(int nbMesSent) {
	    final Lock w = lock.writeLock();
	    w.lock();
		this.nbMesSent = nbMesSent;
	    w.unlock();
	    }

	public int getNbMesAcknowledged() {
		return nbMesAcknowledged;
	}

	public void setNbMesAcknowledged(int nbMesAcknowledged) {
	    final Lock w = lock.writeLock();
	    w.lock();
		this.nbMesAcknowledged = nbMesAcknowledged;
	    w.unlock();
	}

	public int getNbLastConsecutiveMesNotAck() {
		return nbLastConsecutiveMesNotAck;
	}

	public void setNbLastConsecutiveMesNotAck(int nbLastConsecutiveMesNotAck) {
	    final Lock w = lock.writeLock();
	    w.lock();
		this.nbLastConsecutiveMesNotAck = nbLastConsecutiveMesNotAck;
	    w.unlock();
	}
	
	public void incNbMesSent() {
	    final Lock w = lock.writeLock();
	    w.lock();
		this.nbMesSent++;
		w.unlock();
	}

	public void incNbMesAcknowledged() {
	    final Lock w = lock.writeLock();
	    w.lock();
		this.nbMesAcknowledged++;
		this.nbLastConsecutiveMesNotAck = 0;
	    w.unlock();
	}
	
	public void incNbLastConsecutiveMesNotAck() {
	    final Lock w = lock.writeLock();
	    w.lock();
		this.nbLastConsecutiveMesNotAck++;
		w.unlock();
	}

	private void resetAllSatistics() {
	    final Lock w = lock.writeLock();
	    w.lock();
		this.nbMesSent = 0;
		this.nbMesAcknowledged = 0;
		this.nbLastConsecutiveMesNotAck = 0;
		w.unlock();
	}

	@Override
	public String toString() {
		return "LoraWanServerStatistics [nbMesSent=" + nbMesSent + ", nbMesAcknowledged=" + nbMesAcknowledged
				+ ", nbLastConsecutiveMesNotAck=" + nbLastConsecutiveMesNotAck + "]";
	}

}
