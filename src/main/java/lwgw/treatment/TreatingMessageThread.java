package lwgw.treatment;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import lwgw.upstreamdata.GatewayRecord;
import lwgw.upstreamdata.LoraWanMessage;
import lwgw.upstreamdata.defaultmessagesformats.JsonRxpkContainer;
import lwgw.customizedformat.FieldsValuesToSet;
import lwgw.customizedformat.GeneratedClass;
import lwgw.customizedformat.ValuesIntoJsonConverter;
import lwgw.databaseoperations.DatabaseOperation;
import lwgw.databaseoperations.ForwardedMessageToServer;
import lwgw.lorawan.ComManager;
import lwgw.settings.LoraWanServer;
import lwgw.settings.LwgwSettings;
import lwgw.treatment.context.GatewayExecutionContext;

/**
 * This thread is used in a thread pool executor. This way,
 * treatments of Lora radio messages are parallelized.
 * This thread takes the Lora radio message and makes as many
 * Json messages encapsulations it is needed. Then it sends
 * encapsulated messages and at the end, it adds the GatewayRecord
 * object in the dbFifo in order to write the result of all this
 * processing. The sending of encapsulated Lora radio messages to the
 * server is done by a {@link ComManager} object.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class TreatingMessageThread implements Runnable {
	private Queue<DatabaseOperation> dbFifo;
	private final static Logger logger = Logger.getLogger(TreatingMessageThread.class);
	private GatewayRecord record;
	private LwgwSettings lwgwSettings;
	private GatewayExecutionContext context;
	
	public TreatingMessageThread(GatewayRecord gr, Queue<DatabaseOperation> q, LwgwSettings lwgw, GatewayExecutionContext context) {
		record = gr;
		dbFifo = q;
		lwgwSettings = lwgw;
		this.context = context;
	}

	@Override
	public void run() {
		boolean serverResponse;

		// First step : make specifics servers messages
		
		Random rand = new Random();
		int nRandom = rand.nextInt(256);
		short token = (short) ((0xFF << 8) | nRandom);
		record.setToken(token);
		logger.debug("Generated token for message number " + record.getNumber() + ", High value : "
				+ (byte) (token & 0xFF) + ", Low value : " + (byte) (token >> 8));

		ArrayList<LoraWanServer> serversList = record.getServersList();
		ArrayList<LoraWanMessage> messagesList = new ArrayList<LoraWanMessage>();

		for (LoraWanServer s : serversList) {
			if (s.isActivated()) {
				byte[] datagram = makeRadioEncapsulatedServerMsg(s);
				LoraWanMessage m = new LoraWanMessage(datagram, s);
				messagesList.add(m);
			}
		}
		record.setMessagesList(messagesList);
		
		// Second step : send specifics servers messages
		
		ArrayList<LoraWanServer> unreachableServersList = new ArrayList<LoraWanServer>();
		ArrayList<ForwardedMessageToServer> forwardsList = new ArrayList<ForwardedMessageToServer>();
		ComManager cm = new ComManager(lwgwSettings, context);

		for (LoraWanMessage m : messagesList) {
			serverResponse = false;
			serverResponse = cm.sendWithRetry(m.getServerMsg(), m.getLoraWanServer(), record);
			record.setForwarded(true);
			LoraWanServer server = m.getLoraWanServer();
			if (serverResponse == true) {
				record.setAcknowledged(true);
				ForwardedMessageToServer fmts;
				fmts = new ForwardedMessageToServer(server);
				forwardsList.add(fmts);
			} else {
				unreachableServersList.add(server);
			}
		}
		record.setForwardsList(forwardsList);
		record.setUnreachableServersList(unreachableServersList);
		dbFifo.add(new DatabaseOperation(record));
	}

	private byte[] makeRadioEncapsulatedServerMsgHeader(LoraWanServer s) {
		byte[] header = new byte[12];
		byte[] macAddress = record.getMacAddress();
		short token = record.getToken();

		header[0] = s.getProtocolVersion();

		header[1] = (byte) (token & 0xFF);
		header[2] = (byte) (token >> 8);

		header[3] = lwgw.lorawan.Constants.MES_ID_PUSH_DATA;

		header[4] = macAddress[0];
		header[5] = macAddress[1];
		header[6] = macAddress[2];
		header[7] = (byte) 0xFF;
		header[8] = (byte) 0xFF;
		header[9] = macAddress[3];
		header[10] = macAddress[4];
		header[11] = macAddress[5];

		return header;
	}

	private byte[] makeRadioEncapsulatedServerMsg(LoraWanServer s) {
		String jsonInString;
		byte[] header, content, destination = null;
		List<GeneratedClass> generatedClasses = null;

		try {
			if((generatedClasses = s.getRadioGeneratedClasses()) == null) {
				ObjectMapper mapper = new ObjectMapper();
				JsonRxpkContainer rxpk = new JsonRxpkContainer(record);
				// Default Semtech format :
				jsonInString = mapper.writeValueAsString(rxpk);
			} else {
				// Personalized format :
				FieldsValuesToSet fvts = new FieldsValuesToSetForRadioMess(record);
				logger.debug("FieldsValuesToSet : " + fvts);

				jsonInString = ValuesIntoJsonConverter.makeJson(generatedClasses, fvts);
				logger.debug("Json made with a generated class : " + jsonInString);
				
			}
			header = makeRadioEncapsulatedServerMsgHeader(s);
			content = jsonInString.getBytes();

			// Create a destination array that is the size of the two arrays
			destination = new byte[header.length + content.length];
			System.arraycopy(header, 0, destination, 0, header.length);
			System.arraycopy(content, 0, destination, header.length, content.length);

		} catch (JsonGenerationException e) {
			logger.error("JsonGenerationException : " + e.getMessage());
		} catch (JsonMappingException e) {
			logger.error("JsonMappingException : " + e.getMessage());
		} catch (IOException e) {
			logger.error("IOException : " + e.getMessage());
		}
		return destination;
	}

	
	private static class FieldsValuesToSetForRadioMess extends FieldsValuesToSet {
	    private Object paramsObjSetTime[];
	    private Object paramsObjSetTmst[];
	    private Object paramsObjSetChan[];
	    private Object paramsObjSetRfch[];
	    private Object paramsObjSetFreq[];
	    private Object paramsObjSetStat[];
	    private Object paramsObjSetModu[];
	    private Object paramsObjSetDatr[];
	    private Object paramsObjSetCodr[];
	    private Object paramsObjSetLsnr[];
	    private Object paramsObjSetRssi[];
	    private Object paramsObjSetSize[];
	    private Object paramsObjSetData[];
	    
	    FieldsValuesToSetForRadioMess(GatewayRecord record) {
	    	map = new TreeMap<String,Object[]>();
	    	
		    paramsObjSetTime = new Object[1];
		    paramsObjSetTmst = new Object[1];
		    paramsObjSetChan = new Object[1];
		    paramsObjSetRfch = new Object[1];
		    paramsObjSetFreq = new Object[1];
		    paramsObjSetStat = new Object[1];
		    paramsObjSetModu = new Object[1];
		    paramsObjSetDatr = new Object[1];
		    paramsObjSetCodr = new Object[1];
		    paramsObjSetLsnr = new Object[1];
		    paramsObjSetRssi = new Object[1];
		    paramsObjSetSize = new Object[1];
		    paramsObjSetData = new Object[1];

//			Instant instant = Instant.now();
//			Integer tmst = (int) instant.getEpochSecond();
			Integer tmst = (int) System.nanoTime()/1000;
			
			paramsObjSetTime[0] = record.getCreationDate();
			map.put("Time", paramsObjSetTime);
			paramsObjSetTmst[0] = tmst;
			map.put("Tmst", paramsObjSetTmst);
		    paramsObjSetChan[0] = record.getChan();
			map.put("Chan", paramsObjSetChan);
		    paramsObjSetRfch[0] = record.getRfch();
			map.put("Rfch", paramsObjSetRfch);
		    paramsObjSetFreq[0] = record.getFreq();
			map.put("Freq", paramsObjSetFreq);
		    paramsObjSetStat[0] = record.getStat();
			map.put("Stat", paramsObjSetStat);
		    paramsObjSetModu[0] = record.getModu();
			map.put("Modu", paramsObjSetModu);
		    paramsObjSetDatr[0] = record.getDatr();
			map.put("Datr", paramsObjSetDatr);
		    paramsObjSetCodr[0] = record.getCodr();
			map.put("Codr", paramsObjSetCodr);
		    paramsObjSetLsnr[0] = record.getLsnr();
			map.put("Lsnr", paramsObjSetLsnr);
		    paramsObjSetRssi[0] = record.getRssi();
			map.put("Rssi", paramsObjSetRssi);
		    paramsObjSetSize[0] = record.getRadioMsg().length;
			map.put("Size", paramsObjSetSize);
		    paramsObjSetData[0] = new String(Base64.getEncoder().encode(record.getRadioMsg()));		    
			map.put("Data", paramsObjSetData);
	    }

		@Override
		public String toString() {
			return "FieldsValuesToSet [paramsObjSetTime=" + Arrays.toString(paramsObjSetTime)
					+ "paramsObjSetTmst=" + Arrays.toString(paramsObjSetTmst) + ", paramsObjSetChan="
					+ Arrays.toString(paramsObjSetChan) + ", paramsObjSetRfch=" + Arrays.toString(paramsObjSetRfch)
					+ ", paramsObjSetFreq=" + Arrays.toString(paramsObjSetFreq) + ", paramsObjSetStat="
					+ Arrays.toString(paramsObjSetStat) + ", paramsObjSetModu=" + Arrays.toString(paramsObjSetModu)
					+ ", paramsObjSetDatr=" + Arrays.toString(paramsObjSetDatr) + ", paramsObjSetCodr="
					+ Arrays.toString(paramsObjSetCodr) + ", paramsObjSetLsnr=" + Arrays.toString(paramsObjSetLsnr)
					+ ", paramsObjSetRssi=" + Arrays.toString(paramsObjSetRssi) + ", paramsObjSetSize="
					+ Arrays.toString(paramsObjSetSize) + ", paramsObjSetData=" + Arrays.toString(paramsObjSetData)
					+ "]";
		}	    
	}
}
