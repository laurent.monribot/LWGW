package lwgw.treatment;

import java.util.Queue;

import org.apache.log4j.Logger;

import lwgw.settings.LwgwSettings;
import lwgw.treatment.context.GatewayExecutionContext;
import lwgw.settings.LoraRadioSettings;
import lwgw.settings.LoraWanServer;
import lwgw.dao.DatabaseManager;
import lwgw.databaseoperations.DatabaseOperation;
import lwgw.databaseoperations.ForwardedMessageToServer;
import lwgw.databaseoperations.LoraSettingsChange;
import lwgw.databaseoperations.ServerCommand;
import lwgw.databaseoperations.ServerDatabaseOperation;
import lwgw.databaseoperations.ServersListUpdate;
import lwgw.upstreamdata.GatewayRecord;

import lwgw.jmx.JmxManager;

import java.util.ArrayList;



/**
 * This object is the thread managing <b>sequentially</b> all database write operations.
 * It reads the input data queue that contains DatabaseOperations objects.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class DatabaseThread implements Runnable {
	private final static Logger logger = Logger.getLogger(DatabaseThread.class);


	private Queue<DatabaseOperation> inputQueue;
	private LwgwSettings lwgwSettings;
	private DatabaseManager dbManager;
	ArrayList<LoraWanServer> serversList;
	ArrayList<LoraWanServer> serversListTmp;

	GatewayExecutionContext context;

	/**
	 * This method creates the object owning the thread of database treatments.
	 * @param q The input data queue.
	 * @param lwgw The object that contains all settings.
	 * @param db The database manager.
	 * @param context The object that contains all statistic values regarding the current execution.
	 */
	public DatabaseThread(Queue<DatabaseOperation> q, LwgwSettings lwgw, DatabaseManager db, GatewayExecutionContext context) {
		inputQueue = q;
		lwgwSettings = lwgw;
		dbManager = db;
		this.context = context;
	}

	/** This method is for finishing to empty the input data queue
	 * from an another thread, if the thread of this object was
	 * unfortunately stopped.
	 */
	public void flushTheQueue() {
		while (!inputQueue.isEmpty()) {
			loopBody();
		}
		ending();
	}
	
	@Override
	public void run() {
		while (lwgwSettings.getState() != JmxManager.LWGW_STATE_STOP
				|| !inputQueue.isEmpty()) {
			loopBody();
		}
		ending();
	}
	
	/** This is the treatment loop that reads the input data queue.
	 * It is normally called by the thread and if needed, when the
	 * flushTheQueue method is used.
	 */
	private void loopBody() {
		if (!inputQueue.isEmpty()) {
			DatabaseOperation databaseOperation = inputQueue.poll();
			ServerCommand serverCommand = databaseOperation.getServerCommand(); 
			if(serverCommand != null) {
				writeServerCommand(serverCommand);
			}
			LoraSettingsChange loraSettingsChange = databaseOperation.getLoraSettingsChange();
			if(loraSettingsChange != null) {
				// In case of an update of the servers list, the operation has a timestamp :
				String time = databaseOperation.getTime();
				writeLoraSettingsChange(loraSettingsChange, time);
			}
			GatewayRecord record = databaseOperation.getGatewayRecord();
			if(record != null) {
				writeGatewayRecord(record);
			}
			ServersListUpdate serversListUpdate = databaseOperation.getServersListUpdate();
			if(serversListUpdate != null) {
				// In case of an update of the servers list, the operation has a timestamp :
				String time = databaseOperation.getTime();
				writeServersListUpdate(serversListUpdate, time);
			}
		} else {
			try {
				Thread.sleep(41);
			} catch (InterruptedException e) {
				logger.error("Sleep error : " + e.getMessage());
			}
		}
	}
	
	/** This method saves all the settings before closing the database.
	 */
	private void ending() {
		lwgwSettings.saveAll();
		DatabaseManager.getInstance().closeDatabase();
		logger.info("Database closed");
		
		logger.info("LWGW STATE : " + lwgwSettings.getState());
	}
	
	private void writeGatewayRecord(GatewayRecord record) {
		logger.debug("An incoming Lora radio message was treated " + record);

		context.incNumberRadioMessagesReceived();

		int id = dbManager.getLoraMessageSettingsDao().read(record);
		if (id == 0) {
			logger.debug("No Lora settings in accordance were found in database for this message");
			id = dbManager.getLoraMessageSettingsDao().insert(record);
		}
		record.setLoraMessageSettingsId(id);

		for(LoraWanServer s : record.getServersList()) {
			LoraWanServer lws = null;
			if (s.getId() == 0 && s.isActivated()) {
				lws = dbManager.getServerDao().read(s.getAddress(), s.getPort());
				if (lws == null) {
					logger.debug("No LoraWanServer in accordance were found in database");
					dbManager.getServerDao().insert(s);
				} else {
						s.setId(lws.getId());
				}
			}
		}
		// Now all fields in the gateway record are set for the insertion :
		dbManager.getGatewayRecordDao().insert(record);

		if (record.isForwarded()) {
			context.incNumberRadioMessagesForwarded();					
		}
		if (record.isAcknowledged()) {
			for(ForwardedMessageToServer fmts : record.getForwardsList()) {
				// At this point, we are sure our record has an id :
				fmts.setIdRecord(record.getId());
				// This is not the case for our server (it can be a new one) :
				LoraWanServer receiver = fmts.getServer();
				int idServer = receiver.getId();
				if(idServer == 0) {
					serverDbIsertion(receiver);
				}
				// At this point, we are sure our server has an id :
				fmts.setIdServer(receiver.getId());
				
				dbManager.getForwardedMessageToServerDao().insert(fmts);
				context.incNumberRadioMessagesAcknowledged();
			}
		}
		
// This would write too much times on the same place on disk :		
//		for(LoraWanServer s : record.getServersList()) {
//			if(s.isActivated())
//			dbManager.getServerDao().updateForStatistics(s);
//		}
// Prefer to do it only when stopping the LWGW gateway or on a server inactivation
	}
	
	// Save the servers' list update in database
	// as an unique and uninterruptible operation
	private void writeServersListUpdate(ServersListUpdate serversListUpdate, String time){
		LoraWanServer s;
		for(ServerDatabaseOperation sda : serversListUpdate.getOperationsList()) {
			if((s = sda.getServer()) == null) {
				logger.error("Server operation in database asked with a null reference");
			} else {
				if(sda.isInsert()){
					logger.debug("New server to save in database " + s.getAddress() + ":" + s.getPort());
					serverDbIsertion(s);
				} else {
					// This is only for a new server that has been copied too quickly :
					// (occurs when updating a new server before the time needed to record it in database)
					serverDbChecking(s);
				}
				
				if (s.getId() == 0) {
					logger.error("Server operation in database asked with server "
							+ s.getAddress() + ":" + s.getPort() + "that is not present in database");
				}
				
				// Operations here after can be performed being sure the server has an id : 
				if(sda.isUpdateForStatistics()){
					logger.debug("Save statistics in database");
					dbManager.getServerDao().updateForStatistics(s);
				}
				if(sda.isInsertServerActivation()){
					dbManager.getServerActivationDao().insert(s, time);
					if(!s.isActivated()) {
						logger.debug("Save statistics in database");
						dbManager.getServerDao().updateForStatistics(s);
					}
				}
				if(sda.isUpdateForProtocolVersion()){
					dbManager.getServerProtocolVersionDao().insert(s, time);
				}
				if(sda.isUpdateForRadioJsonSchema()){
					dbManager.getPushRadioMessagesDao().insert(s, time);
				}
				if(sda.isUpdateForStatJsonSchema()){
					dbManager.getPushStatMessagesDao().insert(s, time);
				}
				// A check before listing this operation ensure us,
				// especially in this case, the given server has an id :
				if(sda.isUpdateForTermination()){
					dbManager.getServerDao().updateForTermination(s, time);
					// Insert a deactivation line in database with a copied object
					// (don't touch the original one, it can be still in use)
					LoraWanServer sCopy = new LoraWanServer(s);
					sCopy.setActivated(false);
					dbManager.getServerActivationDao().insert(s, time);
				}
			}
		}
	}
	
	private void writeServerCommand(ServerCommand serverCommand){
		LoraWanServer commander = serverCommand.getServer();
		if(commander.getId() == 0) {
			serverDbIsertion(commander);
			logger.warn("The server wasn't in database " + commander);
		}
		dbManager.getServerCommandDao().insert(serverCommand);
	}
	
	private void writeLoraSettingsChange(LoraSettingsChange loraSettingsChange, String time) {
		LoraWanServer server = loraSettingsChange.getServer();
		LoraRadioSettings loraRadioSettings = loraSettingsChange.getLoraRadioSettings();
		if(server != null) {
			if(server.getId() == 0) {
				serverDbIsertion(server);
				logger.warn("The server wasn't in database " + server);
			}
		}
		dbManager.getLoraSettingsChangeDao().insert(loraRadioSettings, time, server);
	}
	
	private void serverDbIsertion(LoraWanServer s) {
		if(serverDbChecking(s) ==0) {
			logger.debug("The server wasn't in database " + s);
			dbManager.getServerDao().insert(s);
		}

	}
	
	// This get the id of the server if you were working with a not updated copy
	private int serverDbChecking(LoraWanServer s) {
		int idInDatabase = 0;
		LoraWanServer sIndatabase = null;
		sIndatabase = dbManager.getServerDao().read(s.getAddress(), s.getPort()); 
		if(sIndatabase != null) {
			idInDatabase = sIndatabase.getId();
			if(s.getId() ==0) s.setId(idInDatabase);
		}
		return idInDatabase;
	}
}
