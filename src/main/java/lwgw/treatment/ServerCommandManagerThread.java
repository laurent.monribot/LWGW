package lwgw.treatment;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import lwgw.databaseoperations.DatabaseOperation;
import lwgw.jmx.JmxManager;

import lwgw.settings.LoraWanServer;
import lwgw.settings.LwgwSettings;
import lwgw.treatment.context.GatewayExecutionContext;

/** This object is the thread that pulls servers in order
 * to provide them a connection for making commands through udp.
 * It use a thread pool executor of {@link ListeningServerThread}
 * objects.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class ServerCommandManagerThread implements Runnable {
	private final static Logger logger = Logger.getLogger(ServerCommandManagerThread.class);
	private final int SLEEPING_COMMAND_MANAGER_DURATION = 1000;
	private LwgwSettings lwgwSettings;
	private byte[] macAddress;
	ArrayList<LoraWanServer> serversList;
	ArrayList<LoraWanServer> serversListTmp;
	private boolean refreshServersList;

	private GatewayExecutionContext context;

	private Queue<DatabaseOperation> dbFifo;

	// Thread pool management
	private ThreadPoolExecutor executor;
	
	private Queue<byte[]> radioFifo;
	
	private void initThreadPool() {
		// RejectedExecutionHandler implementation
		RejectedMessageHandler rejectionHandler = new RejectedMessageHandler(ListeningServerThread.class.getName());
		// Get the ThreadFactory implementation to use
		ThreadFactory threadFactory = Executors.defaultThreadFactory();
		// creating the ThreadPoolExecutor (10 threads, 5 only in work, max
		// length life : 1 sec )
		executor = new ThreadPoolExecutor(40, 40, 600, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(10),
				threadFactory, rejectionHandler);
	}
	
	private void quit() {
		executor.shutdown();
		while (!executor.isTerminated()) {
		}
		logger.info("Finished all threads listening for servers commands");
	}
	
	public ServerCommandManagerThread(LwgwSettings lwgwSettings, byte[] macAdr, GatewayExecutionContext context,
			ConcurrentLinkedQueue<DatabaseOperation> q,
			ConcurrentLinkedQueue<byte[]> radioQ) {
		this.lwgwSettings = lwgwSettings;
		macAddress = macAdr;
		serversList = null;
		serversListTmp = null;
		refreshServersList = false;
		this.context = context;
		dbFifo = q;
		radioFifo = radioQ;
	}
	@Override
	public void run() {
		int pullMessageCounter = 0;
		initThreadPool();
		
		while (lwgwSettings.getState() != JmxManager.LWGW_STATE_STOP) {
			try {
				Thread.sleep(SLEEPING_COMMAND_MANAGER_DURATION);
			} catch (InterruptedException e) {
				logger.error("Sleep error : " + e.getMessage());
			}
			
			if (pullMessageCounter >= lwgwSettings.getPullMessageDelayInterval()) {
				logger.debug("pullMessageRequested");
				if (serversList != null) {
					for (LoraWanServer s : serversList) {
						if (s.isActivated()) {
							// Submit the record to the thread pool - and writes into dbFifo :
							Runnable worker = new ListeningServerThread(s, lwgwSettings, context, macAddress, dbFifo, radioFifo);
							executor.execute(worker);
						}
					}
				}
				pullMessageCounter = 0;
			}
			if (refreshServersList) {
				refreshServersList = false;
				serversList = serversListTmp;
				serversListTmp = null;
				logger.debug("Servers List refreshed");
			}
			pullMessageCounter++;
		}
		quit();
		logger.info("LWGW STATE : " + lwgwSettings.getState());
	}
	
	// for making specific messages according each server
	public void setServersList(ArrayList<LoraWanServer> l) {
		this.serversListTmp = l;
		refreshServersList = true;
	}
}
