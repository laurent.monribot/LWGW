package lwgw.treatment;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import org.apache.log4j.Logger;

/** This object is used in thread pool executors of the application
 * for notifying errors. Thread pool executors are for {@link TreatingMessageThread}
 * and for {@link ServerCommandManagerThread}. Almost not used, except for producing a log message.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class RejectedMessageHandler implements RejectedExecutionHandler {
	private final static Logger logger = Logger.getLogger(RejectedMessageHandler.class);
	private String threadPoolExecutorName;

	
	public RejectedMessageHandler(String name) {
		threadPoolExecutorName = name;
	}
	
	@Override
	public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
		logger.error("Thread pool limit reached for " + threadPoolExecutorName);
	}
}
