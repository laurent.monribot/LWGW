package lwgw.databaseoperations;

import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;

import lwgw.databaseoperations.LoraSettingsChange;
import lwgw.databaseoperations.ServerCommand;
import lwgw.upstreamdata.GatewayRecord;

/**
* Container for several kinds of database operations.
* This object is used to sequentialize all write operations in database.
* It is the object type used in the ConcurrentLinkedQueue "dbFifo".
* It contains one real operation among theses : {@link GatewayRecord},
* {@link ServersListUpdate}, {@link ServerCommand} and {@link LoraSettingsChange}.
* The {@link ServersListUpdate} operation is a compound type is made of
* several {@link ServerDatabaseOperation} objects.
* @author Laurent Monribot
* @version 1.0.0
*/
public class DatabaseOperation {
	private GatewayRecord gatewayRecord;
	private ServersListUpdate serversListUpdate;
	private ServerCommand serverCommand;
	private String time;
	private LoraSettingsChange loraSettingsChange;
	
	public DatabaseOperation() {
		gatewayRecord = null;
		serversListUpdate = null;
		serverCommand = null;
		loraSettingsChange = null;
		time = null;
	}
	
	public DatabaseOperation(GatewayRecord gatewayRecord) {
		this.gatewayRecord = gatewayRecord;
		serversListUpdate = null;
		serverCommand = null;
		loraSettingsChange = null;
		time = null;
	}
	
	public DatabaseOperation(ServersListUpdate serversListUpdate) {
		setTime();
		gatewayRecord = null;
		this.serversListUpdate = serversListUpdate;
		serverCommand = null;
		loraSettingsChange = null;
	}
	
	public DatabaseOperation(ServerCommand serverCommand) {
		gatewayRecord = null;
		serversListUpdate = null;
		this.serverCommand = serverCommand;
		loraSettingsChange = null;
		time = null;
	}
	
	public DatabaseOperation(LoraSettingsChange loraSettingsChange) {
		setTime();
		gatewayRecord = null;
		serversListUpdate = null;
		this.serverCommand = null;
		this.loraSettingsChange = loraSettingsChange;
	}
	
	public GatewayRecord getGatewayRecord() {
		return gatewayRecord;
	}
	
	public void setGatewayRecord(GatewayRecord gatewayRecord) {
		this.gatewayRecord = gatewayRecord;
	}
	
	public ServersListUpdate getServersListUpdate() {
		return serversListUpdate;
	}
	
	public void setServersListUpdate(ServersListUpdate serversListUpdate) {
		this.serversListUpdate = serversListUpdate;
	}
	
	public ServerCommand getServerCommand() {
		return serverCommand;
	}
	
	public void setServerCommand(ServerCommand serverCommand) {
		this.serverCommand = serverCommand;
	}

	public LoraSettingsChange getLoraSettingsChange() {
		return loraSettingsChange;
	}

	public void setLoraSettingsChange(LoraSettingsChange loraSettingsChange) {
		this.loraSettingsChange = loraSettingsChange;
	}

	private void setTime() {
		ISOChronology chrono = ISOChronology.getInstance();
		DateTime dt = DateTime.now(chrono);
		time = dt.toString();
	}
	
	public String getTime() {
		return time;
	}

}
