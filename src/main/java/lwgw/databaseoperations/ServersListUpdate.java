package lwgw.databaseoperations;

import java.util.ArrayList;

/**
* List of operations about servers in database.
* The whole content of this object is treated as a single non-interruptible
* transaction regarding database. It is used for keeping a trace of the list
* of servers that was modified each time a JMX JConsole user acted on it.
* It is nested into the DatabaseOperation object.
* @author Laurent Monribot
* @version 1.0.0
*/
public class ServersListUpdate {
	private ArrayList<ServerDatabaseOperation> operationsList;

	public ServersListUpdate(){
		operationsList = null;
	}
	
	public ArrayList<ServerDatabaseOperation> getOperationsList() {
		return operationsList;
	}

	public void setOperationsList(ArrayList<ServerDatabaseOperation> operationsList) {
		this.operationsList = operationsList;
	}
}
