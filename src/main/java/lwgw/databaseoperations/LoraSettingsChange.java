package lwgw.databaseoperations;

import lwgw.settings.LoraRadioSettings;
import lwgw.settings.LoraWanServer;

/** Object used when a change of lora radio setting is done
 * from the JMX JConsole or a server. If it is done by a server,
 * the id of the server is recorded. It is nested into the DatabaseOperation
 * object.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class LoraSettingsChange {
	private LoraRadioSettings loraRadioSettings;
	private LoraWanServer server;
	
	public LoraRadioSettings getLoraRadioSettings() {
		return loraRadioSettings;
	}
	public void setLoraRadioSettings(LoraRadioSettings loraRadioSettings) {
		this.loraRadioSettings = loraRadioSettings;
	}
	public LoraWanServer getServer() {
		return server;
	}
	public void setServer(LoraWanServer server) {
		this.server = server;
	}
	
}
