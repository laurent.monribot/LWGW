package lwgw.databaseoperations;

/**
 * This object represents data specific to the gateway : versions, Mac address, GPS position, etc.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class GatewayData {
	private int id;
	private String MacAddress;
	private String version;
	
	//SETTINGS
	private int state;
	private boolean radioAck;
	private int monitoringDelayInterval;
	private int udpSocketTimeout;
	private int maxNumberOfRetry;
	private String startUpDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMacAddress() {
		return MacAddress;
	}
	public void setMacAddress(String macAddress) {
		MacAddress = macAddress;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public boolean isRadioAck() {
		return radioAck;
	}
	public void setRadioAck(boolean radioAck) {
		this.radioAck = radioAck;
	}
	public int getMonitoringDelayInterval() {
		return monitoringDelayInterval;
	}
	public void setMonitoringDelayInterval(int monitoringDelayInterval) {
		this.monitoringDelayInterval = monitoringDelayInterval;
	}
	public int getUdpSocketTimeout() {
		return udpSocketTimeout;
	}
	public void setUdpSocketTimeout(int udpSocketTimeout) {
		this.udpSocketTimeout = udpSocketTimeout;
	}
	public int getMaxNumberOfRetry() {
		return maxNumberOfRetry;
	}
	public void setMaxNumberOfRetry(int maxNumberOfRetry) {
		this.maxNumberOfRetry = maxNumberOfRetry;
	}
	public String getStartUpDate() {
		return startUpDate;
	}
	public void setStartUpDate(String startUpDate) {
		this.startUpDate = startUpDate;
	}
}
