package lwgw.databaseoperations;

import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;

import lwgw.settings.LoraWanServer;

/**
* This object represents the <b>down stream</b>
* going through the gateway (i.e: commands received from a server).
* Treatment of commands is implemented for modifying
* Lora radio parameters and for broadcasting Lora radio messages
* that are presents in commands.
* @author Laurent Monribot
* @version 0.0.0
*/
public class ServerCommand {
	private int id;
	private String contents;
	private String date;
	private boolean waitingState;
	private boolean performed;
	private String errorExplanation;
	// We want to read ourselves in the server its id :
	private LoraWanServer server;
	
	public ServerCommand() {
		this.id = 0;
		this.contents = null;
		this.date = null;
		this.waitingState = true;
		this.performed = false;
		this.errorExplanation = null;
		this.server = null;
	}
	
	public ServerCommand(LoraWanServer server, String contents) {
		ISOChronology chrono = ISOChronology.getInstance();
		DateTime dt = DateTime.now(chrono);
		this.id = 0;
		this.contents = contents;
		this.date = dt.toString();
		this.waitingState = true;
		this.performed = false;
		this.errorExplanation = null;
		this.server = server;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public boolean isWaitingState() {
		return waitingState;
	}

	public void setWaitingState(boolean waitingState) {
		this.waitingState = waitingState;
	}

	public boolean isPerformed() {
		return performed;
	}

	public void setPerformed(boolean performed) {
		this.performed = performed;
	}

	public String getErrorExplanation() {
		return errorExplanation;
	}

	public void setErrorExplanation(String errorExplanation) {
		this.errorExplanation = errorExplanation;
	}

	public LoraWanServer getServer() {
		return server;
	}

	public void setServer(LoraWanServer server) {
		this.server = server;
	}
}
