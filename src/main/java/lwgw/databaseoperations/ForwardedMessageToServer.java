package lwgw.databaseoperations;

import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;

import lwgw.settings.LoraWanServer;

/**
 * This object represents the relation between the GatewayRecords table and the LoraWanServers table in database.
 * A line is added in the ForwardedMessages table for each message received by a server.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class ForwardedMessageToServer {
	private LoraWanServer server;
	private int idRecord;
	private int idServer;
	private String creationDate;
	
	public ForwardedMessageToServer(LoraWanServer server) {
		setCreationDate();
		this.idRecord = 0;
		this.idServer = 0;
		this.server = server;
	}

	public int getIdRecord() {
		return idRecord;
	}
	
	public void setIdRecord(int idRecord) {
		this.idRecord = idRecord;
	}
	
	public int getIdServer() {
		return idServer;
	}
	
	public void setIdServer(int idServer) {
		this.idServer = idServer;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public LoraWanServer getServer() {
		return server;
	}

	public void setServer(LoraWanServer server) {
		this.server = server;
	}

	private void setCreationDate() {
		ISOChronology chrono = ISOChronology.getInstance();
		DateTime dt = DateTime.now(chrono);
		creationDate = dt.toString();
	}
}
