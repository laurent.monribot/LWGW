package lwgw.databaseoperations;

import org.apache.log4j.Logger;

import lwgw.settings.LoraWanServer;
import lwgw.settings.LwgwSettings;

/**
* Container for operations about servers in database.
* This object is used in a list to sequentialize servers write
* operations in database. Especially used to update an entire list
* of servers in a single non interruptible operation regarding database.
* It is nested into the ServersListUpdate object. This object can contain
* several update operations of the same LoraWanServer object.
* @author Laurent Monribot
* @version 1.0.0
*/
public class ServerDatabaseOperation {
	private final static Logger logger = Logger.getLogger(ServerDatabaseOperation.class);
	// server to modify or update:
	LoraWanServer server;
	// Types of operations on servers in database :
	boolean updateForStatistics;
	boolean updateForTermination;
	boolean insert;
	boolean insertServerActivation;
	boolean updateForRadioJsonSchema;
	boolean updateForStatJsonSchema;
	boolean updateForProtocolVersion;

	
	public ServerDatabaseOperation() {
		reset();
	}

	public ServerDatabaseOperation(LoraWanServer server) {
		reset();
		this.server = server;
	}
	
	public LoraWanServer getServer() {
		return server;
	}

	public void setServer(LoraWanServer server) {
		this.server = server;
	}

	public boolean isUpdateForStatistics() {
		return updateForStatistics;
	}

	public void setUpdateForStatistics(boolean updateForStatistics) {
		logger.debug("Save statistics in database");
		this.updateForStatistics = updateForStatistics;
	}

	public boolean isUpdateForTermination() {
		return updateForTermination;
	}

	public void setUpdateForTermination(boolean updateForTermination) {
		this.updateForTermination = updateForTermination;
	}

	public boolean isInsert() {
		return insert;
	}

	public void setInsert(boolean insert) {
		this.insert = insert;
	}

	public boolean isInsertServerActivation() {
		return insertServerActivation;
	}

	public void setInsertServerActivation(boolean insertServerActivation) {
		this.insertServerActivation = insertServerActivation;
	}

	public boolean isUpdateForRadioJsonSchema() {
		return updateForRadioJsonSchema;
	}

	public void setUpdateForRadioJsonSchema(boolean updateForRadioJsonSchema) {
		this.updateForRadioJsonSchema = updateForRadioJsonSchema;
	}

	public boolean isUpdateForStatJsonSchema() {
		return updateForStatJsonSchema;
	}

	public void setUpdateForStatJsonSchema(boolean updateForStatJsonSchema) {
		this.updateForStatJsonSchema = updateForStatJsonSchema;
	}

	public boolean isUpdateForProtocolVersion() {
		return updateForProtocolVersion;
	}

	public void setUpdateForProtocolVersion(boolean updateForProtocolVersion) {
		this.updateForProtocolVersion = updateForProtocolVersion;
	}
	
	public void reset() {
		server = null;
		updateForStatistics = false;
		updateForTermination = false;
		insert = false;
		insertServerActivation = false;
		updateForRadioJsonSchema = false;
		updateForStatJsonSchema = false;
		updateForProtocolVersion = false;
	}	
}
