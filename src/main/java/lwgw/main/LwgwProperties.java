package lwgw.main;

import java.io.*;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


/**
 * This object represents saved properties of LWGW application on the file system.
 * It allow the user to specify some initialization values when starting without
 * a database.
 * This is the minimum pool settings at installation.
 * <br/><br/>Example of file :<br/>
 * &lt;?xml version="1.0" encoding="UTF-8" standalone="no"?&gt;&lt;properties version="1.0"&gt;<br/>
 * &lt;comment&gt;LWGW property file, last modified on 2017-07-17T08:58:46.685Z&lt;/comment&gt;<br/>
 * &lt;app.databaseFileName&gt;lwgw&lt;/app.databaseFileName&gt;<br/>
 * &lt;app.ipNetworkInterface&gt;eth0&lt;/app.ipNetworkInterface&gt;<br/>
 * &lt;app.defaultIpServerAddress&gt;40.114.249.243&lt;/app.defaultIpServerAddress&gt;<br/>
 * &lt;app.defaultIpServerPort&gt;1700&lt;/app.defaultIpServerPort&gt;<br/>
 * &lt;app.monitoringDelay&gt;60&lt;/app.monitoringDelay&gt;<br/>
 * &lt;app.pullMessageDelay&gt;30&lt;/app.pullMessageDelay&gt;<br/>
 * &lt;app.statisticsDelay&gt;120&lt;/app.statisticsDelay&gt;<br/>
 * &lt;app.udpSocketTimeOut&gt;100&lt;/app.udpSocketTimeOut&gt;<br/>
 * &lt;app.maxRetryNumber&gt;4&lt;/app.maxRetryNumber&gt;<br/>
 * &lt;app.loraRadio.Frequency&gt;868.0&lt;/app.loraRadio.Frequency&gt;<br/>
 * &lt;app.loraRadio.Power&gt;12&lt;/app.loraRadio.Power&gt;<br/>
 * &lt;app.loraRadio.CodeRedundancy&gt;45&lt;/app.loraRadio.CodeRedundancy&gt;<br/>
 * &lt;app.loraRadio.SpreadingFactor&gt;7&lt;/app.loraRadio.SpreadingFactor&gt;<br/>
 * &lt;app.loraRadio.Bandwidth&gt;125&lt;/app.loraRadio.Bandwidth&gt;<br/>
 * &lt;app.radioAcknowledgment&gt;true&lt;/app.radioAcknowledgment&gt;<br/>
 * &lt;app.radioMessageCounter&gt;1875&lt;/app.radioMessageCounter&gt;<br/>
 * &lt;app.latitude&gt;48.78846&lt;/app.latitude&gt;<br/>
 * &lt;app.longitude&gt;2.44282&lt;/app.longitude&gt;<br/>
 * &lt;app.altitude&gt;50&lt;/app.altitude&gt;<br/>
 * &lt;/properties&gt;<br/>
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class LwgwProperties {
	public final static String CONFIG_FILE_NAME = "lwgw.xml";
	
	public final static String DATABASE_FILE_NAME= "lwgw";
	public final static String DATABASE_FILE_EXT= ".db";

	public final static String IP_NETWORK_INTERFACE = "eth0";
	
	public final static String DEFAULT_IP_SERVER_ADDRESS = "40.114.249.243";
	public static final int DEFAULT_IP_SERVER_PORT = 1700;
	
	public static final int DEFAULT_MONITORING_DELAY = 60;
	public static final int DEFAULT_PULL_MESSAGE_DELAY = 300;
	public static final int DEFAULT_STATISTICS_DELAY = 120;
	public static final int DEFAULT_UDP_SOCKET_TIMEOUT = 100;
	public static final int DEFAULT_MAX_RETRY_NUMBER = 4;
	
	public static final double DEFAULT_LORA_RADIO_FREQUENCY = 868.0;
	public static final int DEFAULT_LORA_RADIO_POWER = 5;
	public static final int DEFAULT_LORA_RADIO_CODE_REDUNDANCY = 45;
	public static final int DEFAULT_LORA_RADIO_SPREADING_FACTOR = 7;
	public static final int DEFAULT_LORA_RADIO_BANDWIDTH = 125;
	
	public static final boolean DEFAULT_RADIO_ACKNOWLEDGMENT = true;

	public static final String DEFAULT_LATITUDE = "48.78846";// 48.788463;
	public static final String DEFAULT_LONGITUDE = "2.44282";// 2.442822;
	public static final String DEFAULT_ALTITUDE = "50";
	
	private static final String ROOT_NODE = "properties";

	private static final String APP_DATABASEFILENAME            = "app.databaseFileName";
	private static final String APP_IPNETWORKINTERFACE          = "app.ipNetworkInterface";
	private static final String APP_DEFAULTIPSERVERADDRESS      = "app.defaultIpServerAddress";
	private static final String APP_DEFAULTIPSERVERPORT         = "app.defaultIpServerPort";
	private static final String APP_MONITORINGDELAY             = "app.monitoringDelay";
	private static final String APP_PULLMESSAGEDELAY            = "app.pullMessageDelay";
	private static final String APP_STATISTICSDELAY             = "app.statisticsDelay";
	private static final String APP_UDPSOCKETTIMEOUT            = "app.udpSocketTimeOut";
	private static final String APP_MAXRETRYNUMBER              = "app.maxRetryNumber";
	private static final String APP_LORARADIO_FREQUENCY         = "app.loraRadio.Frequency";
	private static final String APP_LORARADIO_POWER             = "app.loraRadio.Power";
	private static final String APP_LORARADIO_CODEREDUNDANCY    = "app.loraRadio.CodeRedundancy";
	private static final String APP_LORARADIO_SPREADINGFACTOR   = "app.loraRadio.SpreadingFactor";
	private static final String APP_LORARADIO_BANDWIDTH         = "app.loraRadio.Bandwidth";
	private static final String APP_RADIOACKNOWLEDGMENT         =  "app.radioAcknowledgment"; 
	private static final String APP_RADIOMESSAGECOUNTER         = "app.radioMessageCounter";
	private static final String APP_LATITUDE                    = "app.latitude";
	private static final String APP_LONGITUDE                   = "app.longitude";
	private static final String APP_ALTITUDE                    = "app.altitude";
    
	private static final String COMMENT_BALISE                  = "comment";
	private static final String PROPERTIES_COMMENT              = "LWGW property file, last modified on ";

	private final static Logger logger = Logger.getLogger(LwgwProperties.class);
	private static Document document = null;
	private static LwgwProperties lp = null;
	private Properties prop = null;
	private String databaseFileName = null;
	private String ipNetworkInterface = null;
	private String defaultIpServerAddress = null;
	private int defaultIpServerPort = 0; 
	private int monitoringDelay = 0; 
	private int pullMessageDelay = 0;
	private int statisticsDelay = 0;
	private int udpSocketTimeOut = 0;
	private int maxRetryNumber = 0;
	private double loraRadioFrequency = 0.0;
	private int loraRadioPower = 0; // min 5, max 23
	private int loraRadioCodeRedundancy = 0; // 4/5
	private int loraRadioSpreadingFactor = 0;
	private int loraRadioBandwidth = 0;
	private boolean radioAcknowledgment = false;
	private int applicationRadioMessageCounter = 0;
	private String latitude = null; 
	private String longitude = null; 
	private String altitude = null;
	
	private boolean modified = false;

	private LwgwProperties(){
		reset();
		//Get Document Builder
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
		 
			//Build Document
			document = builder.parse(new File(CONFIG_FILE_NAME));
			 
			//Normalize the XML Structure
			document.getDocumentElement().normalize();
			 
			//Get the root node
			Element root = document.getDocumentElement();
			System.out.println(root.getNodeName());
			
    	    String defaultDefaultIpServerPort = (new Integer(DEFAULT_IP_SERVER_PORT)).toString();
    	    String defaultMonitoringDelay = (new Integer(DEFAULT_MONITORING_DELAY)).toString();
    	    String defaultPullMessageDelay = (new Integer(DEFAULT_PULL_MESSAGE_DELAY)).toString();
    	    String defaultStatisticsDelay = (new Integer(DEFAULT_STATISTICS_DELAY)).toString();
    	    String defaultUdpSocketTimeOut = (new Integer(DEFAULT_UDP_SOCKET_TIMEOUT)).toString();
    	    String defaultMaxRetryNumber = (new Integer(DEFAULT_MAX_RETRY_NUMBER)).toString();
    	    String defaultLoraRadioFrequency = (new Double(DEFAULT_LORA_RADIO_FREQUENCY)).toString();
    	    String defaultLoraRadioPower = (new Integer(DEFAULT_LORA_RADIO_POWER)).toString();
    	    String defaultLoraRadioCodeRedundancy = (new Integer(DEFAULT_LORA_RADIO_CODE_REDUNDANCY)).toString();
    	    String defaultLoraRadioSpreadingFactor = (new Integer(DEFAULT_LORA_RADIO_SPREADING_FACTOR)).toString();
    	    String defaultLoraRadioBandwidth = (new Integer(DEFAULT_LORA_RADIO_BANDWIDTH)).toString();
    	    String defaultradioAcknowledgment = new Boolean(DEFAULT_RADIO_ACKNOWLEDGMENT).toString();
    	    
	        // Get the keys values :
	    databaseFileName =                            getValue(APP_DATABASEFILENAME,           DATABASE_FILE_NAME);
	    ipNetworkInterface =                          getValue(APP_IPNETWORKINTERFACE,         IP_NETWORK_INTERFACE);
	    defaultIpServerAddress =                      getValue(APP_DEFAULTIPSERVERADDRESS,     DEFAULT_IP_SERVER_ADDRESS);
	    defaultIpServerPort =      Integer.parseInt(  getValue(APP_DEFAULTIPSERVERPORT,        defaultDefaultIpServerPort));
	    monitoringDelay =          Integer.parseInt(  getValue(APP_MONITORINGDELAY,            defaultMonitoringDelay));
	    pullMessageDelay =         Integer.parseInt(  getValue(APP_PULLMESSAGEDELAY,           defaultPullMessageDelay));
	    statisticsDelay =          Integer.parseInt(  getValue(APP_STATISTICSDELAY,            defaultStatisticsDelay));
	    udpSocketTimeOut =         Integer.parseInt(  getValue(APP_UDPSOCKETTIMEOUT,           defaultUdpSocketTimeOut));
	    maxRetryNumber =           Integer.parseInt(  getValue(APP_MAXRETRYNUMBER,             defaultMaxRetryNumber));
	    loraRadioFrequency =       Double.parseDouble(getValue(APP_LORARADIO_FREQUENCY,        defaultLoraRadioFrequency));;
	    loraRadioPower =           Integer.parseInt(  getValue(APP_LORARADIO_POWER,            defaultLoraRadioPower));
	    loraRadioCodeRedundancy =  Integer.parseInt(  getValue(APP_LORARADIO_CODEREDUNDANCY,   defaultLoraRadioCodeRedundancy));
	    loraRadioSpreadingFactor = Integer.parseInt(  getValue(APP_LORARADIO_SPREADINGFACTOR,  defaultLoraRadioSpreadingFactor));
	    loraRadioBandwidth =       Integer.parseInt(  getValue(APP_LORARADIO_BANDWIDTH,        defaultLoraRadioBandwidth));
	    latitude =                                    getValue(APP_LATITUDE,                   DEFAULT_LATITUDE);
	    longitude =                                   getValue(APP_LONGITUDE,                  DEFAULT_LONGITUDE);
	    altitude =                                    getValue(APP_ALTITUDE,                   DEFAULT_ALTITUDE);
	        
	        if(getValue("app.radioAcknowledgment", defaultradioAcknowledgment).compareTo(defaultradioAcknowledgment) == 0) {
		        radioAcknowledgment = DEFAULT_RADIO_ACKNOWLEDGMENT;
	        } else {
	        	radioAcknowledgment = !DEFAULT_RADIO_ACKNOWLEDGMENT;
	        }
	        applicationRadioMessageCounter = Integer.parseInt(getValue("app.radioMessageCounter", "0"));
	        
	        log();

		 
		} catch (ParserConfigurationException e) {
			logger.error(e.getMessage());
		} catch (SAXException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

	public static LwgwProperties getInstance() {
		if (lp == null)
			lp = new LwgwProperties();
		return lp;
	}

	public String getDatabaseFileName() {
		return databaseFileName;
	}

	public void setDatabaseFileName(String databaseFileName) {
		modified = true;
		this.databaseFileName = databaseFileName;
	}
	
	public String getIpNetworkInterface() {
		return ipNetworkInterface;
	}

	public void setIpNetworkInterface(String ipNetworkInterface) {
		modified = true;
		this.ipNetworkInterface = ipNetworkInterface;
	}

	public String getDefaultIpServerAddress() {
		return defaultIpServerAddress;
	}

	public void setDefaultIpServerAddress(String defaultIpServerAddress) {
		modified = true;
		this.defaultIpServerAddress = defaultIpServerAddress;
	}

	public int getDefaultIpServerPort() {
		return defaultIpServerPort;
	}

	public void setDefaultIpServerPort(int defaultIpServerPort) {
		modified = true;
		this.defaultIpServerPort = defaultIpServerPort;
	}

	public int getMonitoringDelay() {
		return monitoringDelay;
	}

	public void setMonitoringDelay(int monitoringDelay) {
		modified = true;
		this.monitoringDelay = monitoringDelay;
	}

	public int getPullMessageDelay() {
		return pullMessageDelay;
	}

	public void setPullMessageDelay(int pullMessageDelay) {
		modified = true;
		this.pullMessageDelay = pullMessageDelay;
	}

	public int getStatisticsDelay() {
		return statisticsDelay;
	}

	public void setStatisticsDelay(int statisticsDelay) {
		modified = true;
		this.statisticsDelay = statisticsDelay;
	}

	public int getUdpSocketTimeOut() {
		return udpSocketTimeOut;
	}

	public void setUdpSocketTimeOut(int udpSocketTimeOut) {
		modified = true;
		this.udpSocketTimeOut = udpSocketTimeOut;
	}

	public int getMaxRetryNumber() {
		return maxRetryNumber;
	}

	public void setMaxRetryNumber(int maxRetryNumber) {
		modified = true;
		this.maxRetryNumber = maxRetryNumber;
	}

	public double getLoraRadioFrequency() {
		return loraRadioFrequency;
	}

	public void setLoraRadioFrequency(double loraRadioFrequency) {
		modified = true;
		this.loraRadioFrequency = loraRadioFrequency;
	}

	public int getLoraRadioPower() {
		return loraRadioPower;
	}

	public void setLoraRadioPower(int loraRadioPower) {
		modified = true;
		this.loraRadioPower = loraRadioPower;
	}

	public int getLoraRadioCodeRedundancy() {
		return loraRadioCodeRedundancy;
	}

	public void setLoraRadioCodeRedundancy(int loraRadioCodeRedundancy) {
		modified = true;
		this.loraRadioCodeRedundancy = loraRadioCodeRedundancy;
	}

	public int getLoraRadioSpreadingFactor() {
		return loraRadioSpreadingFactor;
	}

	public void setLoraRadioSpreadingFactor(int loraRadioSpreadingFactor) {
		modified = true;
		this.loraRadioSpreadingFactor = loraRadioSpreadingFactor;
	}

	public int getLoraRadioBandwidth() {
		return loraRadioBandwidth;
	}

	public void setLoraRadioBandwidth(int loraRadioBandwidth) {
		modified = true;
		this.loraRadioBandwidth = loraRadioBandwidth;
	}
	
	public boolean isRadioAcknowledgment() {
		return radioAcknowledgment;
	}

	public void setRadioAcknowledgment(boolean radioAcknowledgment) {
		modified = true;
		this.radioAcknowledgment = radioAcknowledgment;
	}

	public int getApplicationRadioMessageCounter() {
		return applicationRadioMessageCounter;
	}

	public void setApplicationRadioMessageCounter(int applicationRadioMessageCounter) {
		this.applicationRadioMessageCounter = applicationRadioMessageCounter;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatidute(String latitude) {
		modified = true;
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		modified = true;
		this.longitude = longitude;
	}

	public String getAltitude() {
		return altitude;
	}

	public void setAltitude(String altitude) {
		modified = true;
		this.altitude = altitude;
	}

	public void reset() {
        databaseFileName = new String(DATABASE_FILE_NAME);
        ipNetworkInterface = new String(IP_NETWORK_INTERFACE);
        defaultIpServerAddress = new String(DEFAULT_IP_SERVER_ADDRESS);
        defaultIpServerPort = DEFAULT_IP_SERVER_PORT;
        monitoringDelay = DEFAULT_MONITORING_DELAY;
        pullMessageDelay = DEFAULT_PULL_MESSAGE_DELAY;
        statisticsDelay = DEFAULT_STATISTICS_DELAY;
        udpSocketTimeOut = DEFAULT_UDP_SOCKET_TIMEOUT;
        maxRetryNumber = DEFAULT_MAX_RETRY_NUMBER;
        loraRadioFrequency = DEFAULT_LORA_RADIO_FREQUENCY;
        loraRadioPower = DEFAULT_LORA_RADIO_POWER;
        loraRadioCodeRedundancy = DEFAULT_LORA_RADIO_CODE_REDUNDANCY;
        loraRadioSpreadingFactor = DEFAULT_LORA_RADIO_SPREADING_FACTOR;
        loraRadioBandwidth = DEFAULT_LORA_RADIO_BANDWIDTH;
        radioAcknowledgment = DEFAULT_RADIO_ACKNOWLEDGMENT;
        applicationRadioMessageCounter = 0;
        modified = false;
	}

	public void save(String time) {
	    try {	    	
	    	if(document == null) {
				logger.error("No document to save properties");
	    		return;
	    	}
	    	
	    	if(!modified) {
				logger.info("Properties left unchanged, no need to save them");
		        setValue(APP_RADIOMESSAGECOUNTER,       String.valueOf(applicationRadioMessageCounter));
	    		return;	    		
	    	} else {
				modified = false;
				logger.info("Saving properties");
		        setValue(COMMENT_BALISE,                PROPERTIES_COMMENT + time);
		        setValue(APP_DATABASEFILENAME,          databaseFileName);
		        setValue(APP_IPNETWORKINTERFACE,        ipNetworkInterface);
		        setValue(APP_DEFAULTIPSERVERADDRESS,    defaultIpServerAddress);
		        setValue(APP_DEFAULTIPSERVERPORT,       String.valueOf(defaultIpServerPort));
		        setValue(APP_MONITORINGDELAY,           String.valueOf(monitoringDelay));
		        setValue(APP_PULLMESSAGEDELAY,          String.valueOf(pullMessageDelay));
		        setValue(APP_STATISTICSDELAY,           String.valueOf(statisticsDelay));
		        setValue(APP_UDPSOCKETTIMEOUT,          String.valueOf(udpSocketTimeOut));
		        setValue(APP_MAXRETRYNUMBER,            String.valueOf(maxRetryNumber));
		        setValue(APP_LORARADIO_FREQUENCY,       String.valueOf(loraRadioFrequency));
		        setValue(APP_LORARADIO_POWER,           String.valueOf(loraRadioPower));
		        setValue(APP_LORARADIO_CODEREDUNDANCY,  String.valueOf(loraRadioCodeRedundancy));
		        setValue(APP_LORARADIO_SPREADINGFACTOR, String.valueOf(loraRadioSpreadingFactor));
		        setValue(APP_LORARADIO_BANDWIDTH,       String.valueOf(loraRadioBandwidth));
		        setValue(APP_RADIOACKNOWLEDGMENT,       String.valueOf(radioAcknowledgment));
		        setValue(APP_RADIOMESSAGECOUNTER,       String.valueOf(applicationRadioMessageCounter));
		        setValue(APP_LATITUDE,                  latitude);
		        setValue(APP_LONGITUDE,	                longitude);
		        setValue(APP_ALTITUDE, 	                altitude);
	    	}

	        //Save the XML data in the stream
	        FileOutputStream xmlFileStream = new FileOutputStream(CONFIG_FILE_NAME);
	        StreamResult result = new StreamResult(xmlFileStream);
	        TransformerFactory tFactory = TransformerFactory.newInstance();
	        Transformer transformer = tFactory.newTransformer();
	        transformer.transform(new DOMSource(document), result);

	        log();
	    }
	    catch (Exception e ) {
	        e.printStackTrace();
	    }
	}
	
	private void log() {
        logger.debug(APP_DATABASEFILENAME          + " : " + databaseFileName);
        logger.debug(APP_IPNETWORKINTERFACE        + " : " + ipNetworkInterface);
        logger.debug(APP_DEFAULTIPSERVERADDRESS    + " : " + defaultIpServerAddress);
        logger.debug(APP_DEFAULTIPSERVERPORT       + " : " + String.valueOf(defaultIpServerPort));
        logger.debug(APP_MONITORINGDELAY           + " : " + String.valueOf(monitoringDelay));
        logger.debug(APP_PULLMESSAGEDELAY          + " : " + String.valueOf(pullMessageDelay));
        logger.debug(APP_STATISTICSDELAY           + " : " + String.valueOf(statisticsDelay));
        logger.debug(APP_UDPSOCKETTIMEOUT          + " : " + String.valueOf(udpSocketTimeOut));
        logger.debug(APP_MAXRETRYNUMBER            + " : " + String.valueOf(maxRetryNumber));
        logger.debug(APP_LORARADIO_FREQUENCY       + " : " + String.valueOf(loraRadioFrequency));
        logger.debug(APP_LORARADIO_POWER           + " : " + String.valueOf(loraRadioPower));
        logger.debug(APP_LORARADIO_CODEREDUNDANCY  + " : " + String.valueOf(loraRadioCodeRedundancy));
        logger.debug(APP_LORARADIO_SPREADINGFACTOR + " : " + String.valueOf(loraRadioSpreadingFactor));
        logger.debug(APP_LORARADIO_BANDWIDTH       + " : " + String.valueOf(loraRadioBandwidth));
        logger.debug(APP_RADIOACKNOWLEDGMENT       + " : " + String.valueOf(radioAcknowledgment));
        logger.debug(APP_RADIOMESSAGECOUNTER       + " : " + String.valueOf(applicationRadioMessageCounter));
        logger.debug(APP_LATITUDE                  + " : " + latitude);
        logger.debug(APP_LONGITUDE                 + " : " + longitude);
        logger.debug(APP_ALTITUDE                  + " : " + altitude);
	}
	
	private String getValue(String tagName, String defaultValue) {
		if(document == null) return null;
		String str = null;
		NodeList nList = document.getElementsByTagName(tagName);
		if(nList.getLength() == 1) {
			Node node = nList.item(0);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				str = element.getTextContent();
			}
		} else str = defaultValue;
		return str;
	}
	
	private String setValue(String tagName, String value) {
		if(document == null) return null;
		String str = null;
		NodeList nList = document.getElementsByTagName(tagName);
		if(nList.getLength() == 1) {
			Node node = nList.item(0);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				element.setTextContent(value);
			}
		} else {
			Element root = null;
			nList = document.getElementsByTagName(ROOT_NODE);
			if(nList.getLength() == 1) {
				Node node = nList.item(0);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					root = (Element) node;
				}
			} else {
				root = document.createElement(ROOT_NODE);   
				document.appendChild(root);
			}
			Element element = document.createElement(tagName);
			element.appendChild(document.createTextNode(value));
			root.appendChild(element);
		}
		return str;
	}
}
