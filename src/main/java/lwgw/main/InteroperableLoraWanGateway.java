package lwgw.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

import lwgw.hardwareinterface.Chisterapi;
import lwgw.jmx.JmxManager;
import lwgw.customizedformat.JsonSchemaConverter;
import lwgw.dao.DatabaseManager;
import lwgw.databaseoperations.DatabaseOperation;
import lwgw.settings.LoraWanServer;
import lwgw.settings.LwgwSettings;
import lwgw.upstreamdata.GatewayRecord;
import lwgw.treatment.TreatingMessageThread;
import lwgw.treatment.MonitoringThread;
import lwgw.treatment.RejectedMessageHandler;
import lwgw.treatment.context.GatewayExecutionContext;
import lwgw.treatment.ServerCommandManagerThread;
import lwgw.treatment.DatabaseThread;

import java.lang.reflect.Method;
import java.net.NetworkInterface;

/**
* This is the main object of the application containing the <b>entry point</b>.
* It performs all <b>initializations</b> before launching the <b>radio listening loop</b>.<br>
* Script proposed for starting the gateway (address and port refers to the gateway's host) :<br>
* #!/bin/sh<br>
* sudo java -cp src -Djava.library.path=. -Djava.rmi.server.hostname=192.168.0.135 -Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=7097 -jar lwgw-0.0.1-jar-with-dependencies.jar<br>
* @author Laurent Monribot
* @version 1.0.0
*/
public class InteroperableLoraWanGateway {
	private final static Logger logger = Logger.getLogger(InteroperableLoraWanGateway.class);
	private final static int WAITING_DURATION_WHEN_INACTIVE = 10; // ms
	private final static int BROADCASTING_INTERVAL_DURATION = 2000; // ms
	// Application Singleton Object
	private static InteroperableLoraWanGateway ilwg;
	
	// File system saved values
	private LwgwProperties lwgwProperties;
	
	// JMX management
	private LwgwSettings lwgwSettings;

	// Thread pool management
	private ThreadPoolExecutor executor;
	
	// Queue for sequentializing write database operations
	private Queue<DatabaseOperation> dbFifo;
	// Thread owning all write operations in database
	private DatabaseThread databaseThread;

	MonitoringThread monitor;
	
	// LoRa hardware interface
	private static Chisterapi lora;

	// Number of intercepted Lora radio messages
	private int radioMessageCounter;
	
	// Byte array for message content storing
	private byte[] res;

	// IP net interface
	private byte[] macAddress;

	private DatabaseManager dbManager;

	private GatewayExecutionContext context;

	private ServerCommandManagerThread serverCommandManagerThread;

	// Memory to prevent some libraries giving us
	// the last treated radio message :
	private byte[] previousMessage;
	
	// Queue for sequentializing radio orders to emit :
	private Queue<byte[]> radioFifo;
	
	private int broadcastingCounter;
	
	private InteroperableLoraWanGateway() {
		// File system saved values
		lwgwProperties = LwgwProperties.getInstance();
		
		// JMX management
		lwgwSettings = null;
	
		// thread pool management
		executor = null;
		dbFifo = null;
		// consider this thread as the heart of the application :
		databaseThread = null;
	
		monitor = null;
		
		// LoRa hardware interface
		lora = null;
	
		// number of intercepted Lora radio messages since application installation
		radioMessageCounter = 0;
		
		// byte array for message content storing
		res = null;
	
		// IP net interface
		macAddress = null;
	
		dbManager = null;
		
		context = new GatewayExecutionContext();
		
		serverCommandManagerThread = null;
		
		previousMessage = null;
		
		radioFifo = null;
		
		broadcastingCounter = 0;
	}
	/** The InteroperableLoraWanGateway object is a singleton
	 * that manage all the application with all its thread.
	 * @return The InteroperableLoraWanGateway object.
	 */
	public static InteroperableLoraWanGateway getInstance() {
		if (ilwg == null)
			ilwg = new InteroperableLoraWanGateway();
		return ilwg;
	}

	/** This method initializes internal settings and the associated interface
	 * in the JMX JConsole. LwgwSettings is for internal settings and JmxManager
	 * is for JMX JConsole interface. LwgwSettings uses the dbManager object
	 * at start before the launch of the DatabaseThread and at stop when the
	 * DatabaseThread is finished.
	 * @return True if ok.
	 */
	private boolean initJmx() {
		boolean returnValue = true;
		// Load of internal settings form database, if any :
		lwgwSettings = new LwgwSettings(dbManager, lwgwProperties, dbFifo);
		// Expose settings to the JMX JConsole :
		JmxManager.load(lwgwSettings);
		return returnValue;
	}

	/** This method initializes the thread that deals with the database.
	 * The DatabaseThread thread object reads the dbFifo. All others threads
	 * need to do their writing operations by an output in the dbFifo.
	 */
	private void initDbManager() {
		// Activate a separate thread
		// for sequentialize all write operations in database
		// (it reads the queue)
		databaseThread = new DatabaseThread(dbFifo, lwgwSettings, dbManager, context);
		Thread dbThread = new Thread(databaseThread);
		dbThread.start();
	}

	/** This method creates threads for treating received LoRa radio messages,
	 * and another one for the LWGW gateway monitoring.
	 */
	private void initThreadPool() {
		// RejectedExecutionHandler implementation
		RejectedMessageHandler rejectionHandler = new RejectedMessageHandler(TreatingMessageThread.class.getName());
		// Get the ThreadFactory implementation to use
		ThreadFactory threadFactory = Executors.defaultThreadFactory();
		// creating the ThreadPoolExecutor (10 threads, 10 in work at starting,
		// max length life : 1 sec )
		executor = new ThreadPoolExecutor(10, 10, 1000, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(5),
				threadFactory, rejectionHandler);

		// start the monitoring thread
		monitor = new MonitoringThread(executor, lwgwSettings, databaseThread, context, macAddress);
		Thread monitorThread = new Thread(monitor);
		monitorThread.start();
	}

	/** This method initializes the Snootlab chisteraPi card,
	 * connected to the raspberry through the GPIO interface.
	 * @return True if the status of the hardware interface is ok.
	 */
	private boolean initLoraHardwareInterface() {
		lora = new Chisterapi();
		lwgwSettings.setLoraHardwareInterface(lora);
		return lwgwSettings.getStatus();
	}

	/**
	 * This methods is used to get the Mac address. This address
	 * is needed by the hardware LoRa radio interface.
	 * @param interfaceName typically is "eth0".
	 * @return The address on 6 bytes.
	 */
	public static byte[] getMacAddress(String interfaceName) {
		try {
			List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface intf : interfaces) {
				if (interfaceName != null) {
					if (!intf.getName().equalsIgnoreCase(interfaceName))
						continue;
				}
				byte[] mac = intf.getHardwareAddress();
				return mac;
			}
		} catch (Exception ex) {
		} // for now eat exceptions
		return null;
	}

	public static void logMacAddress(byte[] macAddress) {
		StringBuilder buf = new StringBuilder();
		for (int idx = 0; idx < macAddress.length; idx++)
			buf.append(String.format("%02X:", macAddress[idx]));
		if (buf.length() > 0)
			buf.deleteCharAt(buf.length() - 1);
		logger.info("Mac Address : " + buf.toString());
	}

	/** This method is called for initializing all parts of the LWGW gateway.
	 * @param interfaceName tipically is "eth0".
	 * @param dbPath is the database path on disc.
	 * @return True if all is ok.
	 */
	private boolean init(String interfaceName, String dbPath) {
        JsonSchemaConverter.cleanHardDisk();

		// Create a safe-thread queue where threads
		// that need to write in database do all their output :
		dbFifo = new ConcurrentLinkedQueue<DatabaseOperation>();

		// Number of intercepted Lora radio messages since application installation
		radioMessageCounter = lwgwProperties.getApplicationRadioMessageCounter();

		// net access is checked here, at start :
		macAddress = getMacAddress(interfaceName);
		if (macAddress == null) {
			logger.error("interface " + interfaceName + " not found");
			return false;
		}
		logMacAddress(macAddress);

		// database is checked here, at start :
		dbManager = DatabaseManager.getInstance();
		if (!dbManager.openDatabase(dbPath)) {
			logger.error("Database not found");
			return false;
		}
		logger.info("Database opened");

		// Enabling the external control of behavior of the gateway.
		// This loads all the internal settings from database :
		if (!initJmx()) {
			logger.error("Bean creation for JMX remote access failed");
			return false;
		}
		logger.info("JMX opened");

		// Thread for sequentialize all write operations in database
		initDbManager();
		// Thread pool creation for parallelism treatment of radio LoRa Messages
		initThreadPool();
		// Init hardware interface
		if (!initLoraHardwareInterface()) {
			logger.error("LoRa interface access failed");
			return false;
		}
		logger.info("LoRa opened");
		
		radioFifo = new ConcurrentLinkedQueue<byte[]>();
		serverCommandManagerThread = new ServerCommandManagerThread(lwgwSettings, macAddress, context,
				(ConcurrentLinkedQueue<DatabaseOperation>) dbFifo,
				(ConcurrentLinkedQueue<byte[]>) radioFifo);
		Thread commandsThread = new Thread(serverCommandManagerThread);
		commandsThread.start();
		
		return true;
	}

	/** This method is the loop that receives Lora radio messages
	 * during all the application's life.
	 */
	private void mainLoop() {
		while (lwgwSettings.getState() != JmxManager.LWGW_STATE_STOP) {
			lwgwSettings.setState(JmxManager.LWGW_STATE_RUN);
			ArrayList<LoraWanServer> serversList = lwgwSettings.getServersList();
			
			for (LoraWanServer s : serversList) {
				if (s.isActivated()) {
					logger.info(
							"Activated server : " + s.getAddress()
							+ ":" + s.getPort());
				}
			}
			
			monitor.setServersList(serversList);
			serverCommandManagerThread.setServersList(serversList);
			
			hookOnStop();
			
			logger.info("Loop started, listening for Lora radio messages");

			while (lwgwSettings.getState() == JmxManager.LWGW_STATE_RUN) {
				res = lora.receive(lora.getMaxMsgLength());
				if (res != null && res.length != 0 && differentOfPreviousMessage(res)) {
					previousMessage = res;
					radioMessageCounter++;
					lwgwSettings.setApplicationRadioMessageCounter(radioMessageCounter);
					lwgwSettings.incrementsReceivedMessagesOfLength(res.length);
					// Received Signal Strength Indication :
					int lastRssi = lora.getLastRssi();
					// Signal to Noise Ratio :
					int lastSnr = lora.getLastSnr();

					// Do we know if we have to reply the connected object ?
					if (lwgwSettings.getRadioAck()) {
						// For development only :
						// this is not a well formatted Lora radio message
						// and you could make an echo as well if you want to do a repeater
						lora.send(("LoRa response " + radioMessageCounter).getBytes());
					}

					// Create a record of the radio message :
					GatewayRecord record = new GatewayRecord(res, radioMessageCounter, lastRssi, lastSnr, macAddress);
					// Now, set hardware listening settings values :
					record.setLoraConfig(lora);
					// Now, set the server list :
					record.setServersList(serversList);
					
					// Submit the record to the thread pool - and writes into dbFifo :
					Runnable worker = new TreatingMessageThread(record, dbFifo, lwgwSettings, context);
					executor.execute(worker);
				} else {
					radioBroadcastingManagement();
					try {
						// air time is 56.6 msec, for a message
						// with SF7BW125 lora radio parameters
						// with 20 char. of physical payload
						Thread.sleep(WAITING_DURATION_WHEN_INACTIVE);
					} catch (InterruptedException e) {
						logger.error(e.getMessage());
					}
				}
			}
		}
		logger.info("LWGW STATE : " + lwgwSettings.getState());
	}

	/** This method is called on normal ending of the LWGW gateway.
	 */
	private void quit() {
		executor.shutdown();
		while (!executor.isTerminated()) {
		}
		logger.info("Finished all threads for Lora radio messages conditioning and forwarding");
		
    	// Finish to broadcast messages that could be remaining in the radioFifo :
    	while(!radioFifo.isEmpty()){
    		radioBroadcastingManagement();
    		try{
				Thread.sleep(WAITING_DURATION_WHEN_INACTIVE);
			} catch (InterruptedException e) {
				logger.error(e.getMessage());
			}
    	}
        JsonSchemaConverter.cleanHardDisk();

	}

	
	/** This method is called for initiating the LWGW gateway.
	 * This is the only method of the InteroperableLoraWanGateway object
	 * to call from the main entry point.
	 * 
	 */	
	public void launch() {
		boolean resultInit = false;
		resultInit = init(lwgwProperties.getIpNetworkInterface(),
			lwgwProperties.getDatabaseFileName() + LwgwProperties.DATABASE_FILE_EXT);				
		if (resultInit) {
			mainLoop();
			quit();
		}
	}
	
	public static void main(String[] args) {
		InteroperableLoraWanGateway threadsManager = getInstance();
		threadsManager.launch();
	}
	
	/** Method especially created for a hardware lib
	* that continuously gives us its last received message.
	* This method goes along the member variable previousMessage.
	* @return True if ok.
	*/
	private boolean differentOfPreviousMessage(byte[] m) {
		if(previousMessage == null) return true;
		if(m.length != previousMessage.length) return true;
		for(int a=0; a<m.length; a++) {
			if(m[a] != previousMessage[a]) return true;
		}
		return false;
	}
	
	/** This method tests if it is possible to boadcast at this time.
	 * @return true if it is ok.
	 */
	private boolean isBroadcastingIntervalOk() {
		if(broadcastingCounter * WAITING_DURATION_WHEN_INACTIVE > BROADCASTING_INTERVAL_DURATION) {
			return true;
		} else {
			return false;
		}
	}
	
	/** This method is owning the broadcasting of messages received
	 *  by the way of servers' commands.
	 */
	private void radioBroadcastingManagement() {
		// Check if last broadcast is old enough
		// to be sure it is terminated :
		if(isBroadcastingIntervalOk()) {
			// Check if a radio message to emit is present :
			if (!radioFifo.isEmpty()) {
				broadcastingCounter=0;
				// A received command from a server was containing a message :
				byte[] radioMessage = radioFifo.poll();
				lora.send(radioMessage);
				lora.available(); // Switch back in the listening mode
				logger.info("Radio message sent : " + new String(radioMessage));
			}
		} else broadcastingCounter++;
	}

	
	/** (try to) Terminate properly if execution is interrupted :
	* i.e: when someone does a ctrl^C from the command line interface, for example...
	* The good way to stop LWGW gateway is to use the JMX JConsole.
	*/
	private void hookOnStop() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
        	// Other threads are already terminated when we are here :
	        public void run() {
	        	System.out.println("Signal caught - Shutting down ...");
	        	
	        	// Finish to write in the database what is remaining in the dbFifo :
	        	DatabaseThread dt = new DatabaseThread(dbFifo, lwgwSettings, dbManager, context);
	        	dt.flushTheQueue();
	        	
	        	quit();
	        }
	    });
	}
}
