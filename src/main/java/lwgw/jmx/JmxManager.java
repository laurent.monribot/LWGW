package lwgw.jmx;

import java.lang.management.ManagementFactory;
import java.rmi.registry.LocateRegistry;
import java.util.ArrayList;
import java.util.HashMap;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.log4j.Logger;

import com.udojava.jmx.wrapper.JMXBeanWrapper;

import lwgw.settings.LoraWanServer;
import lwgw.settings.LwgwSettings;

/** Class for Loading beans in the JMX JConsole.
* @author Laurent Monribot
* @version 1.0.0
*/
public class JmxManager {
	public static final int LWGW_STATE_STOP = 0;
	public static final int LWGW_STATE_RUN = 1;
	/**
	 * This value is used to go out the main Lora radio messages reception loop
	 * for reloading the entire LoraWan servers' list and then go back into the
	 * loop with a new list.
	 */
	public static final int LWGW_STATE_RESTART = 2;
	
	private final static Logger logger = Logger.getLogger(JmxManager.class);
	
	private static JMXConnectorServer cs = null;
	private static ObjectName name1 = null;
	private static ObjectName name2 = null;
	private static ArrayList<ObjectName> objectNamesServersList = null;
	private static HashMap<String, String[]> environment = null;

	/** Loads LWGW beans in the JMX JConsole.
	 * It is designed to be called several times during the life of application.
	 * The only dynamic part of LWGW beans in the JMX JConsole is the servers' list.
	 * Each server present in the list appears under the form of a distinct
	 * occurrence. This solution is a choice done in order to allow the user adding
	 * and removing each server separately, with the {@link JmxLoraWanServerAdapter} object.
	 * @param lwgwSettings The internal object of the application gathering all settings.
	 * @return True if ok.
	 */
	public static boolean load(LwgwSettings lwgwSettings) {
		boolean returnValue = true;

		try {
			MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

			// At first time, cs is null... This is the connector to the JMX JConsole :
			if (cs == null) {
				// this operation has to be done only one time :
				LocateRegistry.createRegistry(9999);

				// Provide credentials required by server for user authentication
				// You must start app without option
				// -Dcom.sun.management.jmxremote.authenticate=false
				// This has no effect else
				environment = new HashMap<String, String[]>();
				String[] credentials = new String[] { "pi", "raspberry" };
				environment.put(JMXConnector.CREDENTIALS, credentials);
				
				JMXBeanWrapper wrappedBean1 = new JMXBeanWrapper(lwgwSettings);
				JMXBeanWrapper wrappedBean2 = new JMXBeanWrapper(lwgwSettings.getLoraRadioSettings());
				
				name1 = new ObjectName("lacl.lwgw:type=Lwgw,name=Behavior Settings");
				mbs.registerMBean(wrappedBean1, name1);
				name2 = new ObjectName("lacl.lwgw:type=Lwgw,name=Radio Settings");
				mbs.registerMBean(wrappedBean2, name2);
			} else  {
				// We're not at the first time and we have to unregister previous beans :
				if (objectNamesServersList != null) {
					for (ObjectName n : objectNamesServersList) {
						mbs.unregisterMBean(n);
					}
				}
			}

			// Beans of servers list have to be reloaded each time
			// because they can be removed or added :
			ArrayList<LoraWanServer> lwsList = lwgwSettings.getServersList();
			ArrayList<JMXBeanWrapper> wrappedBeanList = new ArrayList<JMXBeanWrapper>();
			for (LoraWanServer s : lwsList) {
				JmxLoraWanServerAdapter sAdapter = new JmxLoraWanServerAdapter(s);
				// These are beans for JMX Jconsole :
				wrappedBeanList.add(new JMXBeanWrapper(sAdapter));				
			}

			int i = 1;
			objectNamesServersList = new ArrayList<ObjectName>();
			for (JMXBeanWrapper wrappedBean : wrappedBeanList) {
				// And this is where beans are registered in the JMX Jconsole : 
				ObjectName name = new ObjectName("lacl.lwgw:type=Servers,name=" + i++);
				objectNamesServersList.add(name);
				mbs.registerMBean(wrappedBean, name);
			}

			
			// Operation ending, only at the first time :
			if (cs == null) {
				JMXServiceURL url = new JMXServiceURL("service:jmx:rmi://localhost/jndi/rmi://localhost:9999/server");
				// Create an RMI connector and start it :
				cs = JMXConnectorServerFactory.newJMXConnectorServer(url, environment, mbs);
				cs.start();
				logger.info("JMX service started on " + url);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			returnValue = false;
		}
		return returnValue;
	}
}