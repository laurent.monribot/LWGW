package lwgw.jmx;



import lwgw.settings.LoraWanServer;
import lwgw.settings.LwgwSettings;

import com.udojava.jmx.wrapper.JMXBean;
import com.udojava.jmx.wrapper.JMXBeanAttribute;

/**
 * This is the JMX adapter object containing data relative to a server.
 * Thus, all specifics values needed for communicate with a server
 * are set in this object.
 * This object is used as a bean in the JMX JConsole and acts as an adapter.
 * It works with an associated {@link LoraWanServer} object and receives its
 * modifications in order to show them in the JMX JConsole, as a listener.
 * All modifications done in the JMX JConsole by a user are took appart until
 * the user decides to validate them with the
 * {@link LwgwSettings}.LoadServersList() operation.
 * @author Laurent Monribot
 * @version 1.0.0
 */
@JMXBean(description = "LoraWan Server Settings")
public class JmxLoraWanServerAdapter extends LoraWanServer{
	
	// List management in jmx
	private boolean markForRemove;
	private boolean modifiedAndNotValidated;

	public JmxLoraWanServerAdapter(LoraWanServer s) {
		//we do a copy of the server :
		super(s);
		//reset specifics values of this object, excluded inherited ones :
		resetInterface();
		//set us as an adapter of the LoraWanServer object
		s.setAdapter(this);
	}
	
	@Override
	@JMXBeanAttribute(name = "Address", description = "IP V4")
	public void setAddress(String address) {
		String addressSave = this.address;
		super.setAddress(address);
		if(addressSave != this.address) modifiedAndNotValidated = true;
	}
	
	@Override
	@JMXBeanAttribute(name = "Port", description = "IP V4")
	public void setPort(int port) {
		int portSave = this.port;
		super.setPort(port);
		if(portSave != this.port) modifiedAndNotValidated = true;
	}

	@Override
	@JMXBeanAttribute(name = "Activated", description = "Used or not currently by the Gateway")
	public void setActivated(boolean activated) {
		boolean activatedSave = this.activated;
		super.setActivated(activated);
		if(activatedSave != this.activated) modifiedAndNotValidated = true;
	}

	@Override
	@JMXBeanAttribute(name = "Protocol version", description = "LoraWan protocol version (1 or 2)")
	public void setProtocolVersion(byte protocolVersion) {
		byte protocolVersionSave = this.protocolVersion;
		super.setProtocolVersion(protocolVersion);
		if(protocolVersionSave != this.protocolVersion) modifiedAndNotValidated = true;
	}

	@JMXBeanAttribute(name = "Remove", description = "Mark for indicate that this position has to be removed")
	public boolean getMarkForRemove() {
		return this.markForRemove;
	}
	
	/**
	 * If this mark is set when using the LwgwSettings.LoadServersList(), the server is removed of the list.
	 * @param b The boolean value to set or reset the mark.
	 */
	@JMXBeanAttribute(name = "Remove", description = "Mark for indicate that this position has to be removed")
	public void setMarkForRemove(boolean b) {
		boolean markForRemoveSave = this.markForRemove;
		this.markForRemove = b;
		if(markForRemoveSave != this.markForRemove) modifiedAndNotValidated = true;
	}

	/**
	 * This method serves the user to see if data displayed are in use,
	 * or if they are modified and not validated.
	 * In this last case, displayed values are not values currently
	 * in use in the LWGW gateway.
	 * To validate them in the LWGW gateway, the user has to use the
	 * {@link LwgwSettings}.LoadServersList() method.
	 * @return The boolean that say if the reload is needed.
	 */
	@JMXBeanAttribute(name = "Servers list reload needed", description = "Modified and not validated object, statistics display continues to be updated")
	public boolean isModifiedAndNotValidated() {
		return modifiedAndNotValidated;
	}

	@JMXBeanAttribute(name = "Radio JSON Schema", description = "LoraWan personalized content for radio encapsulation")
	public void setRadioJsonSchema(String jsonSchema) {
		String jsonSchemaSave = this.radioJsonSchema;
		super.setRadioJsonSchema(jsonSchema);
		if(jsonSchemaSave != this.radioJsonSchema) modifiedAndNotValidated = true;
	}

	@JMXBeanAttribute(name = "Stat JSON Schema", description = "LoraWan personalized content for statistics")
	public void setStatJsonSchema(String jsonSchema) {
		String jsonSchemaSave = this.statJsonSchema;
		super.setStatJsonSchema(jsonSchema);
		if(jsonSchemaSave != this.statJsonSchema) modifiedAndNotValidated = true;
	}

	public void resetInterface() {
		this.markForRemove = false;
		this.modifiedAndNotValidated = false;
	}
	
	@Override
	public void update(LoraWanServer s) {
	// Address and port are excluded of the update
		if(!modifiedAndNotValidated) {
			this.activated = s.isActivated(); 
			this.protocolVersion = s.getProtocolVersion();
			this.setCreationDate(s.getCreationDate());
			this.setId(s.getId());
			this.radioJsonSchema = s.getRadioJsonSchema();
			this.statJsonSchema = s.getStatJsonSchema();
			this.radioGeneratedClasses = s.getRadioGeneratedClasses();
			this.statGeneratedClasses = s.getStatGeneratedClasses();
		}
	}

	@Override
	public void setAdapter(JmxLoraWanServerAdapter adapter) {
		//Inhibited
	}

}
