package lwgw.upstreamdata;

import java.util.ArrayList;

import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;

import lwgw.databaseoperations.ForwardedMessageToServer;
import lwgw.hardwareinterface.Chisterapi;
import lwgw.settings.LoraWanServer;

/**
* This object represents the <b>up stream</b> going through the gateway.
* It is created with the <b>Lora radio message</b> and is enriched with several <b>formatted Json messages</b>.
* Each Json message is (LoraWan) server specific and is an encapsulation of the radio message.
* @author Laurent Monribot
* @version 1.0.0
*/
public class GatewayRecord {
	private byte[] radioMsg;
	private int number;
	private short token;
	private int rssi;
	private double freq;
	private int chan;
	private int rfch;
	private int stat;
	private String codr;
	private String modu;
	//private String datr; // for FSK modulation, not used in this program
	private int lsnr;

	private byte[] macAddress;

	private int sf = 0;
	private int bw = 0;
	private int pw = 0;

	// The given list of servers that have to receive the Lora radio message :
	private ArrayList<LoraWanServer> serversList;
	// Prepared Lora radio messages in a Json encapsulation, specific to each server :
	private ArrayList<LoraWanMessage> messagesList;
	// Effectives forwards done :
	private ArrayList<ForwardedMessageToServer> forwardsList;
	// Servers that were unreachable when forwarding the prepared message :
	private ArrayList<LoraWanServer> unreachableServersList;
	
	// Database index :
	private int id;
	private int loraMessageSettingsId;
	
	private boolean forwarded;
	private boolean acknowledged;
	
	private String creationDate;
	
	public GatewayRecord(byte[] m, int n, int r, int s, byte[] a) {
		setCreationDate();

		radioMsg = m;
		number = n;
		token = 0;
		rssi = r;
		freq = 0;
		chan = 0;
		rfch = 0;
		stat = 0;
		codr = "";
		modu = "LORA";
		lsnr = s;

		macAddress = a;
		serversList = null;
		messagesList = null;
		forwardsList = null;
		unreachableServersList = null;
		
		id = 0;
		loraMessageSettingsId = 0;

		forwarded = false;
		acknowledged = false;
	}

	public byte[] getRadioMsg() {
		return radioMsg;
	}

	public void setRadioMsg(byte[] message) {
		this.radioMsg = message;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public short getToken() {
		return token;
	}

	public void setToken(short token) {
		this.token = token;
	}

	public byte[] getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(byte[] macAddres) {
		this.macAddress = macAddres;
	}

	public int getRssi() {
		return rssi;
	}

	public void setRssi(int rssi) {
		this.rssi = rssi;
	}

	public double getFreq() {
		return freq;
	}

	public void setFreq(double freq) {
		this.freq = freq;
	}

	public int getChan() {
		return chan;
	}

	public void setChan(int chan) {
		this.chan = chan;
	}

	public int getRfch() {
		return rfch;
	}

	public void setRfch(int rfch) {
		this.rfch = rfch;
	}

	public int getStat() {
		return stat;
	}

	public void setStat(int stat) {
		this.stat = stat;
	}

	public String getCodr() {
		return codr;
	}

	public void setCodr(String codr) {
		this.codr = codr;
	}

	public String getModu() {
		return modu;
	}

	public void setModu(String modu) {
		this.modu = modu;
	}

	public String getDatr() {
		return "SF" + sf + "BW" + bw;
	}

	public int getLsnr() {
		return lsnr;
	}

	public void setLsnr(int lsnr) {
		this.lsnr = lsnr;
	}

	public int getSf() {
		return sf;
	}

	public void setSf(int sf) {
		this.sf = sf;
	}

	public int getBw() {
		return bw;
	}

	public void setBw(int bw) {
		this.bw = bw;
	}

	public int getPw() {
		return pw;
	}

	public void setPw(int pw) {
		this.pw = pw;
	}

	public void setLoraConfig(Chisterapi lora) {
		setFreq(lora.getFrequency());
		setPw(lora.getPower());
		switch (lora.getCodeRedundancy()) {
		case 45:
			setCodr("4/5");
			break;
		case 46:
			setCodr("4/6");
			break;
		case 47:
			setCodr("4/7");
			break;
		case 48:
			setCodr("4/8");
			break;
		default:
			setCodr("0");
		}
		setSf(lora.getSpreadingFactor());
		setBw(lora.getBandWidth());
		// Needed if you want to see your record displayed
		// in the TheThingsNetwork.org graphical interface :
		setStat(1);
	}

	public String toString() {
		String str = toStringHexDumpRadioMsg() + " |";
		for (LoraWanMessage m : messagesList) {
			if (m.getLoraWanServer().isActivated()) {
				str = str + "| " + m.getLoraWanServer().toString()
						+ " | " + m.toString() + " |";
			}
		}
		return str;
	}

	public String toStringHexDumpRadioMsg() {
		String str = toStringHexDumpRadioMsgShort();
		str = "Radio message number = " + number + ", length = " + radioMsg.length + ", content = " + str;
		return str;
	}

	public String toStringHexDumpRadioMsgShort() {
		String str = new String();
		for (int i = 0; i < radioMsg.length; i++) {
			str = str + String.format(" %02X", radioMsg[i]);
		}
		return str;
	}

	// This is for making specific messages according each server :
	public void setServersList(ArrayList<LoraWanServer> l) {
		// This is a set by reference
		// from now, the main thread must not change this list
		this.serversList = l;
	}

	public ArrayList<LoraWanServer> getServersList() {
		return serversList;
	}

	public void setMessagesList(ArrayList<LoraWanMessage> l) {
		this.messagesList = l;
	}

	public ArrayList<LoraWanMessage> getMessagesList() {
		return messagesList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getLoraMessageSettingsId() {
		return loraMessageSettingsId;
	}

	public void setLoraMessageSettingsId(int id) {
		this.loraMessageSettingsId = id;
	}

	public boolean isForwarded() {
		return forwarded;
	}

	public void setForwarded(boolean forwarded) {
		this.forwarded = forwarded;
	}

	/**
	 * This method is used by the DatabaseThread object to know
	 * if the table ForwardedMessages needs to be updated.
	 * @return True if at least one Lora wan server received
	 * the Lora radio encapsulated message.
	 */
	public boolean isAcknowledged() {
		return acknowledged;
	}

	public void setAcknowledged(boolean acknowledged) {
		this.acknowledged = acknowledged;
	}

	public ArrayList<ForwardedMessageToServer> getForwardsList() {
		return forwardsList;
	}

	public void setForwardsList(ArrayList<ForwardedMessageToServer> forwardsList) {
		this.forwardsList = forwardsList;
	}

	public ArrayList<LoraWanServer> getUnreachableServersList() {
		return unreachableServersList;
	}

	public void setUnreachableServersList(ArrayList<LoraWanServer> unreachableServersList) {
		this.unreachableServersList = unreachableServersList;
	}

	/** This method gets the reception date of the Lora radio message.
	 * @return The date in <b>iso 8601</b> format, like this : "2017-04-22T06:10:08Z456".
	 */
	public String getCreationDate() {
		return creationDate;
	}
	
	/** This method is called in the constructor
	 */
	private void setCreationDate() {
		ISOChronology chrono = ISOChronology.getInstance();
		DateTime dt = DateTime.now(chrono);
		creationDate = dt.toString();
	}
}
