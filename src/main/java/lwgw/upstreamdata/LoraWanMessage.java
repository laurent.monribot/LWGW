package lwgw.upstreamdata;

import lwgw.settings.LoraWanServer;

/**
* This is the object containing a <b>Lora radio message Json encapsulation</b> relative to a server.
* This object is in a list managed by GatewayRecord that owns the original Lora radio message. The
* list contains as many messages as servers are. It has two methods, one for getting the server that
* has to receive the message and another one for getting the message itself.
* @author Laurent Monribot
* @version 1.0.0
*/
public class LoraWanMessage {
	// This is the "ready to send" datagram :
	private byte[] serverMsg;
	// The datagram is made in compliance with :
	private LoraWanServer loraWanServer;
	
	public LoraWanMessage(byte[] serverMsg, LoraWanServer loraWanServer) {
		this.serverMsg = serverMsg;
		this.loraWanServer = loraWanServer;		
	}
	
	public byte[] getServerMsg() {
		return serverMsg;
	}

	public LoraWanServer getLoraWanServer() {
		return loraWanServer;
	}

	public String toString() {
		return toStringHexDumpHeaderUdp() + " | " +
			toStringJsonUdp();
	}

	public String toStringHexDumpHeaderUdp() {
		String str = new String();
       		if(serverMsg != null && serverMsg.length>=12) {
			for (int i = 0; i < 12; i++) {
				str = str + String.format(" %02X", serverMsg[i]);
			}
			str = "Udp message header , content = " + str;
		}
		return str;
	}
	
	public String toStringJsonUdp() {
		String str = new String();
		if(serverMsg != null && serverMsg.length>12) {
			byte[] jsonPart = new byte[serverMsg.length - 12];
			
			for (int i = 12; i < serverMsg.length; i++) {
				jsonPart[i - 12] = serverMsg[i];
			}
			str = "Json message part, length = " + (serverMsg.length - 12) + ", content = " + new String(jsonPart);
		}
		return str;
	}

}
