package lwgw.upstreamdata.defaultmessagesformats;

import java.time.Instant;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonProperty;

import lwgw.upstreamdata.GatewayRecord;

/**
* This is <b>"rxpk"</b>, one of possibles <b>Lora radio message Json encapsulations</b>.
* Described in the <b>Semtech</b> documentation : <b>ANNWS.01.2.1.W.SYS</b> "Gateway to server interface".
* @author Laurent Monribot
* @version 1.0.0
*/
public class JsonEncapsulatedRadioMessage {
	private final static Logger logger = Logger.getLogger(JsonEncapsulatedRadioMessage.class);
	
	@JsonProperty("time")
	String time;
	@JsonProperty("tmst")
	long tmst;
	@JsonProperty("chan")
	int chan;
	@JsonProperty("rfch")
	int rfch;
	@JsonProperty("freq")
	double freq;
	@JsonProperty("stat")
	int stat;
	@JsonProperty("modu")
	String modu;
	@JsonProperty("datr")
	String datr;
	@JsonProperty("codr")
	String codr;
	@JsonProperty("lsnr")
	int lsnr;
	@JsonProperty("rssi")
	int rssi;
	@JsonProperty("size")
	int size;
	@JsonProperty("data")
	byte[] data;

	public JsonEncapsulatedRadioMessage(GatewayRecord record) {
//	Instant instant = Instant.now();
//	tmst = instant.getEpochSecond();
	tmst = (System.nanoTime()/1000)%2^32;
	
	// comments on the right are typical values
	time = record.getCreationDate();
	freq = record.getFreq();//868.1;
	chan = record.getChan();//0;
	stat = record.getStat();//1;
	codr = record.getCodr();//"4/5";
	modu = record.getModu();//"LORA";
	datr = record.getDatr();//"SF7BW125";
	lsnr = record.getLsnr();//9;
	rssi = record.getRssi(); // -20;
	logger.info("FREQ : " + freq +
			" CHAN : " + chan +
			" RFCH : " + rfch +
			" STAT : " + stat +
			" CODR : " + codr +
			" MODU : " + modu +
			" DATR : " + datr +
			" LSNR : " + lsnr +
			" RSSI : " + rssi
			);
	size = record.getRadioMsg().length;
	data = record.getRadioMsg(); //byte array is automatically base 64 encoded
	}
}
