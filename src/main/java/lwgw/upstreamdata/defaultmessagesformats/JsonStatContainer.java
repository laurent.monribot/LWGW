package lwgw.upstreamdata.defaultmessagesformats;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
* This is the <b>"stat" object</b>, one of possibles <b>Lora radio message Json encapsulations</b>.
* Described in the <b>Semtech</b> documentation : <b>ANNWS.01.2.1.W.SYS</b> "Gateway to server interface".
* This is not a table in comparison with rxpk.
* @author Laurent Monribot
* @version 1.0.0
*/
public class JsonStatContainer {

	@JsonProperty("stat")
	JsonStatisticsMessage stat;
	
	public JsonStatContainer(int rxnb, int rxok, int rwfw, int ackr, int dwnb, int txnb) {
		stat = new JsonStatisticsMessage(rxnb, rxok, rwfw, ackr, dwnb, txnb);
	}
}
