package lwgw.upstreamdata.defaultmessagesformats;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * This is <b>"stat"</b>, one of possibles Json messages for sending
 * <b>statistic values required for gateways monitoring</b>. Described in the
 * <b>Semtech</b> documentation : <b>ANNWS.01.2.1.W.SYS</b> "Gateway to server
 * interface".
 * 
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class JsonStatisticsMessage {
	private final static Logger logger = Logger.getLogger(JsonStatisticsMessage.class);

	@JsonProperty("time")
	String time;
	@JsonProperty("lati")
	double lati;
	@JsonProperty("long")
	double lgtd;
	@JsonProperty("alti")
	int alti;
	@JsonProperty("rxnb")
	int rxnb;
	@JsonProperty("rxok")
	int rxok;
	@JsonProperty("rwfw")
	int rwfw;
	@JsonProperty("ackr")
	int ackr;
	@JsonProperty("dwnb")
	int dwnb;
	@JsonProperty("txnb")
	int txnb;

	public JsonStatisticsMessage(int rxnb, int rxok, int rwfw, int ackr, int dwnb, int txnb) {
		// never forget GMT at the end in order to be acknowledged by TTN
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss' GMT'");
		TimeZone tz = TimeZone.getTimeZone("UTC");
		df.setTimeZone(tz);

		time = df.format(new Date());
		// Créteil with 5 decimals
		lati = 48.78846;// 48.788463;
		lgtd = 2.44282;// 2.442822;
		alti = 50;

		this.rxnb = rxnb;
		this.rxok = rxok;
		this.rwfw = rwfw;
		this.ackr = ackr;
		this.dwnb = dwnb;
		this.txnb = txnb;
		logger.info("TIME : " + time + " LATI : " + lati + " LONG : " + lgtd + " ALTI : " + alti + " RXNB : "
				+ this.rxnb + " RXOK : " + this.rxok + " RWFW : " + this.rwfw + " ACKR : " + this.ackr + " DWNB : "
				+ this.dwnb + " TXNB : " + this.txnb);
	}
}
