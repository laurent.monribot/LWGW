package lwgw.upstreamdata.defaultmessagesformats;

import com.fasterxml.jackson.annotation.JsonProperty;

import lwgw.upstreamdata.GatewayRecord;

/**
* This is the <b>"rxpk" table</b>, one of possibles <b>Lora radio message Json encapsulations</b>.
* Described in the <b>Semtech</b> documentation : <b>ANNWS.01.2.1.W.SYS</b> "Gateway to server interface".
* Several Lora radio messages can be forwarded in only one UDP datagram to a server.
* @author Laurent Monribot
* @version 1.0.0
*/
public class JsonRxpkContainer {
	@JsonProperty("rxpk")
	JsonEncapsulatedRadioMessage[] rxpk;
	
	public JsonRxpkContainer(GatewayRecord record) {
		rxpk = new JsonEncapsulatedRadioMessage[1];
		rxpk[0] = new JsonEncapsulatedRadioMessage(record);
	}
}
