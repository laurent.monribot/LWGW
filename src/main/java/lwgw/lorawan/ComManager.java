package lwgw.lorawan;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Queue;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lwgw.databaseoperations.DatabaseOperation;
import lwgw.databaseoperations.ServerCommand;
import lwgw.downstreamdata.JsonServerCommand;
import lwgw.settings.LoraWanServer;
import lwgw.settings.LwgwSettings;
import lwgw.treatment.ListeningServerThread;
import lwgw.treatment.MonitoringThread;
import lwgw.treatment.TreatingMessageThread;
import lwgw.treatment.context.GatewayExecutionContext;
import lwgw.upstreamdata.GatewayRecord;

/** Object for UDP connection managing with servers.
* @author Laurent Monribot
* @version 1.0.0
*/
public class ComManager {
	private static final int BUFFER_RECEPTION_SIZE = 1024;
	private final static Logger logger = Logger.getLogger(ComManager.class);
	private DatagramSocket socket;
	private LwgwSettings lwgwSettings;
	private GatewayExecutionContext context;

	private Queue<DatabaseOperation> dbFifo;
	
	private Queue<byte[]> radioFifo;
	
	/** Constructor used by {@link TreatingMessageThread} and by {@link MonitoringThread}.
	 * These threads send "push" messages to the server.
	 * @param lwgwSettings Settings of the application.
	 * @param context Statistics values regarding the current execution.
	 */
	public ComManager(LwgwSettings lwgwSettings, GatewayExecutionContext context) {
		socket = null;
		this.lwgwSettings = lwgwSettings;
		this.context = context;
		dbFifo = null;
		radioFifo = null;
	}
	
	/** Constructor used by {@link ListeningServerThread}.
	 * The ListeningServerThread object is the only one
	 * that may have the need of broadcasting a message.
	 * This thread sends "pull" messages to the server.
	 * @param lwgwSettings Settings of the application.
	 * @param context Statistics values regarding the current execution.
	 * @param q The queue for adding {@link DatabaseOperation} objects.
	 * @param radioQ The queue for messages to broadcast
	 */
	public ComManager(LwgwSettings lwgwSettings, GatewayExecutionContext context,
			Queue<DatabaseOperation> q, Queue<byte[]> radioQ) {
		socket = null;
		this.lwgwSettings = lwgwSettings;
		this.context = context;
		dbFifo = q;
		radioFifo = radioQ;
	}
	
	private void sendudp(byte[] datagram, DatagramSocket socket, InetAddress adr, int port) {
		try {
			DatagramPacket packet;
			packet = new DatagramPacket(datagram, datagram.length, adr, port);

			socket.send(packet);

		} catch (SocketException e) { // for the new Datagram...()
			logger.error(e.getMessage());
		} catch (IOException e) { // for the send()
			logger.error(e.getMessage() + " Address : " + adr.getHostAddress() + " Port : " + port);
		}
	}

	private DatagramPacket sendWithAck(byte[] datagram, InetAddress adr, int port) {
		boolean somethingReceived = false;
		byte[] buffer = new byte[BUFFER_RECEPTION_SIZE];
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
		sendudp(datagram, socket, adr, port);
		try {
			socket.receive(packet);
			if(packet.getLength()>0) somethingReceived = true;

		} catch (SocketTimeoutException e) {
			logger.warn(e.getMessage());
		} catch (IOException e) {
			logger.warn(e.getMessage());
		}
		if (!somethingReceived)
			return null;
		return packet;
	}

	private boolean testAck(byte[] sentdata, DatagramPacket packet, InetAddress adr, int port, GatewayRecord record) {
		boolean ackOk = false;
		String strLog, strSubLog;
		if (record != null) {
			strSubLog = "for following message : " + record.toString();
		} else {
			strSubLog = "for message of type : " + sentdata[3];
		}

		if (packet == null || (packet != null && packet.getData().length == 0)) {
			strLog = "Ack NOT received " + strSubLog + " | From server " + adr + " : " + port;
			logger.warn(strLog);
		} else {
			byte[] receivedData = packet.getData();

			// Response message analysis :
			// receivedData[0] protocol version (2 possibilities)
			// receivedData[1-2] PUSH_DATA message number
			// receivedData[3] PUSH_ACK identifier 0x01 or receivedData[3]
			// PULL_ACK identifier 0x04
			// receivedData[3] PULL_RESP identifier 0x03

			if ((receivedData[0] == 0x01 || receivedData[0] == 0x02)
					&& (receivedData[3] == lwgw.lorawan.Constants.MES_ID_PUSH_ACK
							|| receivedData[3] == lwgw.lorawan.Constants.MES_ID_PULL_ACK
							|| receivedData[3] == lwgw.lorawan.Constants.MES_ID_PULL_RESP)) {

				// PULL_RESP has tow way interpreting token value (set to 0 in
				// V1 and
				// set to token PULL in V2)
				if (receivedData[3] == lwgw.lorawan.Constants.MES_ID_PULL_RESP) {
					if ((receivedData[0] == 0x01 && receivedData[1] == 0 && receivedData[2] == 0)
							|| (receivedData[0] == 0x02 && receivedData[1] == sentdata[1]
									&& receivedData[2] == sentdata[2])) {
						logger.info("Pull response received");
					} else {
						logger.info("Pull response received but not correct");
					}
				} else if ((receivedData[1] == sentdata[1] && receivedData[2] == sentdata[2])) {
					strLog = "Ack received " + strSubLog + " | Generated token High value : " + (byte) (receivedData[1])
							+ ", Low value : " + (byte) (receivedData[2]) + " | From server " + adr + " : " + port;
					logger.info(strLog);
					ackOk = true;

				} else {
					strLog = "Ack received but not " + strSubLog + " | From server " + adr + " : " + port;
					logger.warn(strLog);
				}
			} else {
				String strDump = new String();
				byte[] bytearray = packet.getData();
				for (int i = 0; i < packet.getLength(); i++) {
					strDump = strDump + String.format(" %02X", bytearray[i]);
				}
				strLog = "Other response than Ack got " + strSubLog + " | Packet received : " + strDump
						+ " | From server " + adr + " : " + port;
				logger.warn(strLog);
			}
		}
		return ackOk;
	}
	/**
	 * This method is used for sending messages to servers. It can send
	 * the message several times if the server does not acknowledge it.
	 * This is specified in {@link LwgwSettings} by the method getMaxNumberOfRetry().
	 * The timeout socket is specified in {@link LwgwSettings} by the
	 * method getUdpSocketTimeout().
	 * If the message has got a response from the server, the response
	 * is treated by using a {@link JsonServerCommand} object.
	 * @param datagram The message to send.
	 * @param s The LoraWanServer to contact.
	 * @param record The GatewayRecord object is passed to the function just
	 * for some logging operations. It can be null if no Lora radio message
	 * is concerned.
	 * @return True is the server acknowledged the message.
	 */
	public boolean sendWithRetry(byte[] datagram, LoraWanServer s, GatewayRecord record) {
		DatagramPacket packet = null;
		int repeatNumber = 0;
		boolean ackIsOk = false;
		
		try {
			socket = new DatagramSocket();
			socket.setSoTimeout(lwgwSettings.getUdpSocketTimeout());

			InetAddress adr = InetAddress.getByName(s.getAddress());
			int port = s.getPort();

			packet = sendWithAck(datagram, adr, port);
			s.incNbMesSent();
			ackIsOk = testAck(datagram, packet, adr, port, record);
			if (s.getNbLastConsecutiveMesNotAck() < 3) {
				// a repeat only in case the server is known for acknowledging
				// our messages :
				while (!ackIsOk && repeatNumber < lwgwSettings.getMaxNumberOfRetry()) {
					repeatNumber++;
					logger.warn("REPEAT " + repeatNumber);
					packet = sendWithAck(datagram, adr, port);
					ackIsOk = testAck(datagram, packet, adr, port, record);
				}
			}
			if (!ackIsOk) {
				s.incNbLastConsecutiveMesNotAck();
				logger.error("FAILED");
			} else {
				s.incNbMesAcknowledged();
				byte[] receivedData = packet.getData();
				packet = null;
				if (receivedData[3] == lwgw.lorawan.Constants.MES_ID_PUSH_ACK) {
				} else if (receivedData[3] == lwgw.lorawan.Constants.MES_ID_PULL_ACK) {
					listeningForCommand(receivedData, s, adr, port);
				} else {
					logger.warn("UNKNOWN MESSAGE TYPE received : " + receivedData[3]);
				}
			}
		} catch (UnknownHostException e) { // for the getByName()
			logger.error(e.getMessage());
		} catch (SocketException e) { // for the new DatagramSocket()
			logger.error(e.getMessage());
		}

		socket.close();
		socket = null;
		return ackIsOk;
	}
	
	private void treatServerCommand(DatagramPacket packet, LoraWanServer s, InetAddress adr, int port) {
		byte[] receivedData = new byte[packet.getLength()];
		receivedData = packet.getData();
		String json = getMessageJsonPart(receivedData);
		logger.debug("JSON Received Command : " + json);
		ServerCommand serverCommand = new ServerCommand(s, json);
		ObjectMapper mapper = new ObjectMapper();

		JsonServerCommand command;
		try {
			command = (JsonServerCommand) mapper
								.readerFor(JsonServerCommand.class)
								.readValue(json);
			String result = command.perform(s, context, lwgwSettings, radioFifo);
			// if needed, send a TX_ACK
			if(result != null) {
				serverCommand.setErrorExplanation(result);
				byte[] tx = makeTxServerMsg(receivedData, result);
				sendudp(tx, socket, adr, port);
			} else {
				serverCommand.setPerformed(true);
			}
			if(receivedData != null) {
				dbFifo.add(new DatabaseOperation(serverCommand));
			} else {
				logger.warn("No queue has been set for writing received server's command in database");
			}
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}
	
	public String getMessageJsonPart(byte[] datagramReceived) {
		// Be carrefull : datagramReceived.length  does not return the value we want
		int datagramSize=0;
		for(int i = 0; i<datagramReceived.length; i++) {
			if(datagramReceived[i]!=0 && datagramReceived[i]!=32) datagramSize=i+1;
		}
				
		byte[] jsonBuf = new byte[datagramSize - 4];
		for(int offset=0; offset+4 < datagramSize; offset++) {
			jsonBuf[offset] = datagramReceived[offset + 4];
		}
		return new String(jsonBuf);
	}
	
	private byte[] makeTxServerMsg(byte[] receivedData, String strErrorType) {
		byte[] header = new byte[4];

		header[0] = 0; // ProtocolVersion

		header[1] = receivedData[1];
		header[2] = receivedData[2];

		header[3] = lwgw.lorawan.Constants.MES_ID_TX_ACK;

		if (strErrorType == null)
			return header; // positive acknowledgment

		byte[] content = strErrorType.getBytes();
		// create a destination array that is the size of the two arrays
		byte[] destination = new byte[header.length + content.length];
		System.arraycopy(header, 0, destination, 0, header.length);
		System.arraycopy(content, 0, destination, header.length, content.length);

		return destination;
	}
	
	private void listeningForCommand(byte[] receivedData, LoraWanServer s, InetAddress adr, int port) {
		boolean somethingReceived = true;
		int timeToWait = lwgwSettings.getPullMessageDelayInterval()*1000;
		// Stop the listening 500ms just before a new thread is about to be launched :
		try {	
			socket.setSoTimeout(timeToWait -500);
			while(somethingReceived) {
				byte[] buffer = new byte[BUFFER_RECEPTION_SIZE];
				DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
				somethingReceived = false;
				socket.receive(packet);
				if(packet.getLength()>0) {
					somethingReceived = true;
					logger.debug("We were waiting for a command from server "
					+ adr.toString() + ":" + port
					+ " and we got it");
				}
				if (somethingReceived) {
					logger.info("Command to process received");
					treatServerCommand(packet, s, adr, port);
				}
				// After a first command received in this thread,
				// the next command (if any) is expected quickly
				// else, the server has to wait another pull
				socket.setSoTimeout(3000);
			}
			// Quit, leaving the socket with the standard time out
			socket.setSoTimeout(lwgwSettings.getUdpSocketTimeout());
		} catch (SocketTimeoutException e) {
			logger.warn(e.getMessage());
		} catch (SocketException e) { // For setSoTimeout 
			logger.warn(e.getMessage());
		} catch (IOException e) {
			logger.warn(e.getMessage());
		}
	}
}
