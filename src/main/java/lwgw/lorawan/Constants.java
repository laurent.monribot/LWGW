package lwgw.lorawan;

/**
* This is the object containing constants for LoraWan messages headers.
* These constants are used for distinguish types of messages exchanged between a gateway and a server.
* Described in the <b>Semtech</b> documentation : <b>ANNWS.01.2.1.W.SYS</b> "Gateway to server interface".
* @author Laurent Monribot
* @version 1.0.0
*/
public class Constants {
	/**
	 * Upstream : It serves the LWGW gateway to send Lora radio
	 * messages to the server or statistics messages.
	 */
	public final static byte MES_ID_PUSH_DATA  = 0x00; //PUSH_DATA
	/**
	 * Downstream : It acknowledge MES_ID_PUSH_DATA.
	 */
	public final static byte MES_ID_PUSH_ACK  = 0x01; // PUSH_ACK
	/**
	 * Upstream : It serves to establish a connection to
	 * the server in order to allow it passing orders.
	 */
	public final static byte MES_ID_PULL_DATA = 0x02; // PULL_DATA
	/**
	 * Downstream : This is a command from the server,
	 * after the server sent a MES_ID_PULL_ACK.
	 */
	public final static byte MES_ID_PULL_RESP = 0x03; // PULL_RESP
	/**
	 * Downstream : It acknowledge MES_ID_PULL_DATA.
	 */
	public final static byte MES_ID_PULL_ACK  = 0x04; // PULL_ACK
	/**
	 * Upstream : If needed, sent in response of a MES_ID_PULL_RESP.
	 */
	public final static byte MES_ID_TX_ACK    = 0x05; // TX_ACK
}
