package lwgw.downstreamdata;


import java.util.Queue;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lwgw.lorawan.ComManager;
import lwgw.lorawan.Constants;
import lwgw.settings.LoraWanServer;
import lwgw.settings.LwgwSettings;
import lwgw.treatment.ServerCommandManagerThread;
import lwgw.treatment.context.GatewayExecutionContext;
/** This object treats a server's command received in the format
 * of the Semtech's documentation. It contains also the list of TX_ACK
 * error messages that can be transmitted to the server in response of
 * a PULL RESP command. Described in the <b>Semtech</b>
 * documentation : <b>ANNWS.01.2.1.W.SYS</b> "Gateway to server interface".
 * Warning: here, FSK modulation not supported. Coverage of Semtech's
 * documentation is not complete. The JsonServerCommand is used by the
 * {@link ComManager} object.
 * This part of the application should be updated with the Json schema
 * mecanism in put place for Statistics and Lora radio messages encapsulation
 * for customizing commands format.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class JsonServerCommand {
	private final static Logger logger = Logger.getLogger(JsonServerCommand.class);
	public final static String TOO_LATE = "\"error\":\"TOO_LATE\"";
	public final static String TOO_EARLY = "\"error\":\"TOO_EARLY\"";
	public final static String COLLISION_PACKET = "\"error\":\"COLLISION_PACKET\"";
	public final static String COLLISION_BEACON = "\"error\":\"COLLISION_BEACON\"";
	public final static String TX_FREQ = "\"error\":\"TX_FREQ\"";
	public final static String TX_POWER = "\"error\":\"TX_POWER\"";
	public final static String GPS_UNLOCKED = "\"error\":\"GPS_UNLOCKED\"";
	/**
	 * For now : the only value used in the application.
	 */
	public final static String NOT_IMPLEMENTED = "\"error\":\"NOT_IMPLEMENTED\"";

	
	@JsonProperty("imme")
	boolean imme;
	@JsonProperty("tmst")
	long tmst;
	@JsonProperty("tmms")
	long tmms;
	@JsonProperty("freq")
	double freq;
	@JsonProperty("rfch")
	int rfch;
	@JsonProperty("powe")
	int powe;
	@JsonProperty("modu")
	String modu;
	@JsonProperty("datr")
	String datr;
	// @JsonProperty("datr")
	// int datr;
	@JsonProperty("codr")
	String codr;

	// @JsonProperty("fdev")
	// int fdev;

	@JsonProperty("ipol")
	boolean ipol;

	// @JsonProperty("prea")
	// int prea;

	@JsonProperty("size")
	int size;
	@JsonProperty("data")
	String data;

	@JsonProperty("ncrc")
	boolean ncrc;

    @JsonCreator
    public JsonServerCommand(	@JsonProperty("imme") boolean imme,
    							@JsonProperty("tmst") long tmst,
    							@JsonProperty("tmms") long tmms,
    							@JsonProperty("freq") double freq,
    							@JsonProperty("rfch") int rfch,
    							@JsonProperty("powe") int powe,
    							@JsonProperty("modu") String modu,
    							@JsonProperty("datr") String datr,
    							@JsonProperty("codr") String codr,
    							@JsonProperty("ipol") boolean ipol,
    							@JsonProperty("size") int size,
    							@JsonProperty("data") String data,
    							@JsonProperty("ncrc") boolean ncrc) {
		reset();
		this.imme = imme;
		this.tmst = tmst;
		this.tmms = tmms;
		this.freq = freq;
		this.rfch = rfch;
		this.powe = powe;
		this.modu = modu;
		this.datr = datr;
		this.codr = codr;
		this.ipol = ipol;
		this.size = size;
		this.data = data;
		this.ncrc = ncrc;
	}
    
    public JsonServerCommand() {
		reset();
	}
	/**
	 * This method is used to execute the received command. The command can change
	 * Lora radio settings, ask the LWGW gateway to broadcast a given message or
	 * other things, like packing Lora radio messages together for sending them at
	 * a precise time. This method uses the
	 * {@link LwgwSettings}.LoadLoraHardwareInterfaceSettingsModifFromServer() method.
	 * @param s The server is needed to trace the change of Lora radio settings in
	 * the database.
	 * @param context The context is needed to update statistics of the LWGW gateway.
	 * @param lwgwSettings This is where Lora radio settings are stocked and modified.
	 * @param radioFifo This queue is read by the main thread loop to fetch messages
	 * to broadcast, if any.
	 * @return The error message content to send to the server in a
	 * {@link Constants}.MES_ID_TX_ACK response.
	 */
	public String perform(LoraWanServer s, GatewayExecutionContext context,
			LwgwSettings lwgwSettings, Queue<byte[]> radioFifo) {
		String returnValue = null;
		logger.info("Imme, received value : " + imme);
		if(imme == false) {
			logger.warn("Imme, received value not yet managed");
			logger.info("Tmst, received value : " + tmst + "for send packet on a certain timestamp value");
			// tmms field for GPS time as a monotonic number of milliseconds
			// ellapsed since January 6th, 1980 (GPS Epoch). No leap second.
			logger.info("Tmms, received value : " + tmms + "for send packet at a certain GPS time");			
		}
		
		if(freq != 0.0) {
			lwgwSettings.getLoraRadioSettings().setFrequency(freq);
			logger.info("Frequency, received value : " + freq);
		}
		if(rfch != 0) {
			logger.info("Rfch (not used), received value : " + rfch);
			returnValue = NOT_IMPLEMENTED;
		}
		if(powe != 0) {
			lwgwSettings.getLoraRadioSettings().setPower(powe);
			logger.info("Power, received value : " + powe);
		}
		if(modu != null) {
			logger.info("Application only accepts Lora modulation, received value : " + modu);
		}
		if(datr != null) {
			if(datr.contains("SF7")) lwgwSettings.getLoraRadioSettings().setSpreadingFactor(7);
			else if(datr.contains("SF8")) lwgwSettings.getLoraRadioSettings().setSpreadingFactor(8);
			else if(datr.contains("SF9")) lwgwSettings.getLoraRadioSettings().setSpreadingFactor(9);
			else if(datr.contains("SF10")) lwgwSettings.getLoraRadioSettings().setSpreadingFactor(10);
			else if(datr.contains("SF11")) lwgwSettings.getLoraRadioSettings().setSpreadingFactor(11);
			else if(datr.contains("SF12")) lwgwSettings.getLoraRadioSettings().setSpreadingFactor(12);
			if(datr.contains("BW125")) lwgwSettings.getLoraRadioSettings().setBandwidth(125);
			else if(datr.contains("BW250")) lwgwSettings.getLoraRadioSettings().setBandwidth(250);
			else if(datr.contains("BW500")) lwgwSettings.getLoraRadioSettings().setBandwidth(500);
			logger.info("Datr, received value : " + datr);
		}
		// int datr : not yet in use in this app (this for SFK only)
		if(codr != null) {
			if(codr.compareTo("4/5") == 0) lwgwSettings.getLoraRadioSettings().setPower(45);
			if(codr.compareTo("4/6") == 0) lwgwSettings.getLoraRadioSettings().setPower(46);
			if(codr.compareTo("4/7") == 0) lwgwSettings.getLoraRadioSettings().setPower(47);
			if(codr.compareTo("4/8") == 0) lwgwSettings.getLoraRadioSettings().setPower(48);
			logger.info("Codr, received value : " + codr);
		}
		if(freq != 0.0 || rfch != 0 || powe != 0 || modu != null || datr != null || codr != null) {
			lwgwSettings.LoadLoraHardwareInterfaceSettingsModifFromServer(s);
		}

		// int fdev : not yet in use in this app (this for SFK only)
		
		context.incNumberRadioMessagesGotFromServer();
		if(size !=0 && data != null) {
			// Send a radio message with data as content
			if(radioFifo != null) {
				radioFifo.add(data.getBytes());
				context.incNumberRadioMessagesEmitted();
				logger.info("Radio message to send placed in the output fifo " + data);
			} else {
				logger.error("No output fifo available to send radio message " + data);
			}
		}
		logger.info("Ipol (not used), received value : " + ipol);
		logger.info("Ncrc (not used), received value : " + ncrc);
		return returnValue;
	}

	public void reset() {
		this.imme = true;
		this.tmst = 0;
		this.tmms = 0;
		this.freq = 0.0;
		this.rfch = 0;
		this.powe = 0;
		this.modu = null;
		this.datr = null;
		this.codr = null;
		this.ipol = false;
		this.size = 0;
		this.data = null;
		this.ncrc = false;
	}

	@Override
	public String toString() {
		return "JsonServerCommand [imme=" + imme + ", tmst=" + tmst + ", tmms=" + tmms + ", freq=" + freq
				+ ", rfch=" + rfch + ", powe=" + powe + ", modu=" + modu + ", datr=" + datr + ", codr=" + codr
				+ ", ipol=" + ipol + ", size=" + size + ", data=" + data + ", ncrc=" + ncrc + "]";
	}

}
