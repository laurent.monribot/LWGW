package lwgw.customizedformat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.annotation.Nullable;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import org.jsonschema2pojo.DefaultGenerationConfig;
import org.jsonschema2pojo.GenerationConfig;
import org.jsonschema2pojo.Jackson2Annotator;
import org.jsonschema2pojo.SchemaGenerator;
import org.jsonschema2pojo.SchemaMapper;
import org.jsonschema2pojo.SchemaStore;
import org.jsonschema2pojo.rules.RuleFactory;

import com.sun.codemodel.JCodeModel; 

/** This Class generates and compile the class corresponding
 * to the Json Schema used by the server. It has only a constructor
 * that makes all and a getter on the generated class. This object is
 * used when <b>a user changes the schema in the JMX JConsole</b> and when
 * <b>application reads the server object in database</b>.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class JsonSchemaConverter {
	private final static Logger logger = Logger.getLogger(JsonSchemaConverter.class);
	private static final String JAVA = ".java";
	private static final String JSON_SCHEMA = ".jsonSchema";
	private static final String PACKAGE_NAME = JsonSchemaConverter.class.getPackage().getName();
	private List<GeneratedClass> generatedClasses;
	
	public JsonSchemaConverter(String className, String jsonSchema) {
		generatedClasses = null;
		if(className == null || jsonSchema == null) return;        
		
		JCodeModel codeModel = new JCodeModel();
		
        File f = new File(className + JSON_SCHEMA);
        OutputStream out = null;
		try {
			out = new FileOutputStream( f );
			out.write(jsonSchema.getBytes(), 0, jsonSchema.getBytes().length);
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
        
		URL source = null;
		try {
			source = f.toURI().toURL();
		} catch (MalformedURLException e) {
			logger.error("MalformedURLException : " + e.getMessage());
		}

		GenerationConfig config = new DefaultGenerationConfig() {
		    /**
		     * @return <code>true</code>
		     */
			@Override
			public boolean isGenerateBuilders() {
				// set config option by overriding method
				return true;
			}
			
		    /**
		     * @return <code>false</code>
		     */
		    @Override
		    public boolean isIncludeAdditionalProperties() {
		        return false;
		    }

		    /**
		     * @return <code>false</code>
		     */
		    @Override
		    public boolean isIncludeHashcodeAndEquals() {
		        return false;
		    }
		    
		    /**
		     * @return <code>true</code>
		     */
		    @Override
		    public boolean isIncludeToString() {
		        return true;
		    }
		};

		SchemaMapper mapper = new SchemaMapper(
				new RuleFactory(config, new Jackson2Annotator(config), new SchemaStore()), new SchemaGenerator());

		try {
			mapper.generate(codeModel, className, PACKAGE_NAME, source);
		} catch (IOException e) {
			logger.error("IOException : " + e.getMessage());
		}

		try {
			codeModel.build(new File("."));
			
			
			File dir = new File((new String(PACKAGE_NAME)).replaceAll("\\.", File.separator));
			File[] filesList = dir.listFiles();
	        Path path = Paths.get((new String(PACKAGE_NAME)).replaceAll("\\.", File.separator));
	        compileJava(dir, path.toString());
	        
	        generatedClasses = new ArrayList<GeneratedClass>();
			for (File file : filesList) {
			    if (file.isFile() && file.toString().endsWith(JAVA)) {
			    	String strName = file.getName();
			        String strNameWithoutExt = strName.substring(0, strName.length() - JAVA.length());
			        
			        Class<?> generatedClass = null;
			        generatedClass = load(path, strNameWithoutExt);
			        GeneratedClass gClass = new GeneratedClass();
			        gClass.setName(strNameWithoutExt);
			        gClass.setGeneratedClass(generatedClass);
			        if(className.compareTo(strNameWithoutExt) == 0) {
			        	gClass.setMainClass(true);
			        	gClass.setSchema(jsonSchema);
			        }
			        StringBuilder strB = new StringBuilder();
		        	InputStream stream = new FileInputStream(file); 
		        	InputStreamReader reader = new InputStreamReader(stream);
		        	BufferedReader bufferedReader = new BufferedReader(reader);
		        	String line;
		        	while ((line=bufferedReader.readLine())!=null){
		        		strB.append(line);
		        	}
		        	gClass.setSource(strB.toString());
		        	bufferedReader.close(); 
		        	reader.close();
		        	stream.close();
		        	generatedClasses.add(gClass);

			    }
			}
		} catch (SecurityException e) {
			logger.error("SecurityException : " + e.getMessage());
		} catch (ClassNotFoundException e) {
			logger.error("ClassNotFoundException : " + e.getMessage());
		} catch (IOException e) {
			logger.error("IOException : " + e.getMessage());
		}
		if(generatedClasses.isEmpty()) generatedClasses = null;
	}
	

	private static void cleanPackage(String pckge) {
        String directoryToRemove = pckge;
        int dotIndex = 0;
        if((dotIndex = directoryToRemove.indexOf(".")) > 0) {
        	directoryToRemove = directoryToRemove.substring(0,dotIndex);
        }
        FileUtils.deleteQuietly(new File(directoryToRemove));
	}
	
	private static void cleanSchemas(String extension) {
		File path = new File(".");
	    if( path.exists() ) {
	    	FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith(extension) ? true : false;
				}};
	    	File[] files = path.listFiles(filter);
	    	for(int i=0; i<files.length; i++) {
	    		if(!files[i].isDirectory()) {
	    			files[i].delete();
	    		}
	    	}
	    }
	}
	
	public static void cleanHardDisk() {
		cleanPackage(PACKAGE_NAME);
		cleanSchemas(JSON_SCHEMA);
	}

	
	private static Class<?> load(Path basePath, String className) throws MalformedURLException, ClassNotFoundException {
		URL[] urls = new URL[] { basePath.toUri().toURL() };
		@SuppressWarnings("resource")
		ClassLoader classLoader = new URLClassLoader(urls);
	    Class<?> thisClass = classLoader.loadClass(PACKAGE_NAME + "." + className);
	    return thisClass;
	}

	public List<GeneratedClass> getGeneratedClasses() {
		return generatedClasses;
	}
	
	
	private File[] getAllFiles(String directory, String extension) {
		File[] files = null;
		File path = new File(directory);
	    if( path.exists() ) {
	    	FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith(extension) ? true : false;
				}};
	    	files = path.listFiles(filter);
	    	for(File f : files){
	    	}
	    }
    	return files;
	}
	
	/** Compile java src into java .class files */
	private void compileJava(File tempDir, String classpath) {
	  JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
	  if (compiler == null) {
	    logger.error("JDK required (running inside of JRE)");
	    return;
	  }

	  DiagnosticCollector<JavaFileObject> diagnostics =
	      new DiagnosticCollector<>();
	  StandardJavaFileManager fileManager =
	      compiler.getStandardFileManager(diagnostics, null, null);
	  try {
	    Iterable<? extends JavaFileObject> compilationUnits =
	    		fileManager.getJavaFileObjectsFromFiles(Arrays.asList(getAllFiles(tempDir.toString(), ".java")));
	    ArrayList<String> options = new ArrayList<>();
	    if (classpath != null) {
	      options.add("-classpath");
	      //options.add(classpath);
	      String allJarsClassPathInMavenLocalRepo = getAllJarsClassPathInMavenLocalRepo();
	      options.add(allJarsClassPathInMavenLocalRepo);
	    }
	    options.add("-d");
	    options.add(tempDir.getPath());
	    JavaCompiler.CompilationTask task = compiler.getTask(
	        null,
	        fileManager,
	        diagnostics,
	        options,
	        null,
	        compilationUnits);
	    if (!task.call()) {
	      Locale myLocale = Locale.getDefault();
	      StringBuilder msg = new StringBuilder();
	      msg.append("Cannot compile to Java bytecode:");
	      for (Diagnostic<? extends JavaFileObject> err : diagnostics.getDiagnostics()) {
	        msg.append('\n');
	        msg.append(err.getKind());
	        msg.append(": ");
	        if (err.getSource() != null) {
	          msg.append(err.getSource().getName());
	        }
	        msg.append(':');
	        msg.append(err.getLineNumber());
	        msg.append(": ");
	        msg.append(err.getMessage(myLocale));
	      }
	      logger.error(msg.toString());
	    }
	  } catch(Exception e) {
		  logger.error(e.getMessage());
	  } finally {
		  try{
			  fileManager.close();
		  } catch(IOException e) {
		  logger.error(e.getMessage());
		  }
	  }
	}
	
	private String getAllJarsClassPathInMavenLocalRepo() { 	 
		  Class<?> cls; 
		  String retVal; 
		  try { 
		   cls = Class.forName("lwgw.main.InteroperableLoraWanGateway"); 
		  } catch (ClassNotFoundException e) { 
		   retVal = System.getProperty("java.class.path"); 
		   return retVal;// NOPMD 
		  }
		 
		  // returns the ClassLoader object associated with this Class 
		  ClassLoader cLoader = cls.getClassLoader(); 
		 
		  URL[] paths = ((URLClassLoader) cLoader).getURLs();
		  
		  String pathSep= ":"; 
		  retVal = "target" + File.separator + "classes" 
		    + pathSep + "target" + File.separator + "*.jar"
		    + pathSep + "dist" + File.separator + "InlineCompiler.jar"; 
		  for(URL path : paths) { 
			   // try { 
			   if(null == path) { 
				 logger.error("classloader url wrong");
				 return null;
			   }
			   URI fileURI = fixFileURL(path);
			   retVal = retVal + pathSep + Paths.get(fileURI).toString();
		  }
		  return retVal;
	}
		  
    private static @Nullable URI fixFileURL(URL url) { 
	  if (!"file".equals(url.getProtocol()))
	   throw new IllegalArgumentException(); 
	  File tmp ; 
	  try { 
	   tmp = new File(url.toURI());
	  } catch (URISyntaxException e) { 
	   tmp = new File(url.getFile()); 
	  } 
	  return tmp.toURI(); 
    }
    
}
