package lwgw.customizedformat;

/**
 * This object is to keep a reference on generated class
 * made from the json schema. This is usefull when the main
 * generated class contain a list of another generated classes
 * @author Laurent Monribot
 * @version 1.0.0
 */

public class GeneratedClass {
	private boolean isMainClass;
	private Class<?> generatedClass;
	private String name;
	private String source;
	private String Schema;
	
	public boolean isMainClass() {
		return isMainClass;
	}
	public void setMainClass(boolean isMainClass) {
		this.isMainClass = isMainClass;
	}
	public Class<?> getGeneratedClass() {
		return generatedClass;
	}
	public void setGeneratedClass(Class<?> theGeneratedClass) {
		this.generatedClass = theGeneratedClass;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getSchema() {
		return Schema;
	}
	public void setSchema(String schema) {
		Schema = schema;
	}
}
