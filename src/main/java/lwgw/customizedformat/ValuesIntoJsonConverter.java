package lwgw.customizedformat;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.NoClass;

import japa.parser.ast.expr.ArrayAccessExpr;

/**
 * Object providing one method for converting an object
 * of the type of a generated annoted class.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class ValuesIntoJsonConverter {
	private final static Logger logger = Logger.getLogger(ValuesIntoJsonConverter.class);
	private final static String PREFIX_FOR_ARRAY = "_array_";
	
	/** This method use a generated class of data to convert a list of values into a json message.
	 * @param lgc The list of generated annoted classes (with a main object in it).
	 * @param fvts The list of values to put in the json message.
	 * @return The json message.
	 */
	public static String makeJson(List<GeneratedClass> lgc, FieldsValuesToSet fvts) {
		String jsonReturnValue = "";
		Class<?> thisClass = getMainClass(lgc);
		if(thisClass != null) {
			try {
				Object instance = thisClass.newInstance();
				visitGeneratedClass(thisClass, fvts, instance, lgc);
				jsonReturnValue = generatedObjectSerialization(instance);
			} catch (InstantiationException e) {
				logger.error("InstantiationException : " + e.getMessage());
			} catch (IllegalAccessException e) {
				logger.error("IllegalAccessException : " + e.getMessage());
			}
		}
		return jsonReturnValue;
	}
	
	private static void visitGeneratedClass(Class<?> thisClass, FieldsValuesToSet fvts,
												Object instance, List<GeneratedClass> lgc) {
	    try {
			for(Field m :thisClass.getDeclaredFields()) {
			    fieldAnnotationParsing(m, thisClass, fvts, instance, lgc);
			}
		} catch (IllegalArgumentException e) {
			logger.error("IllegalArgumentException : " + e.getMessage());
		} catch (SecurityException e) {
			logger.error("SecurityException : " + e.getMessage());
		}	    
	}
	
	private static void fieldAnnotationParsing(Field m, Class<?> thisClass, FieldsValuesToSet fvts,
													Object instance, List<GeneratedClass> lgc) {
	    try {
			PropertyDescriptor pd = new PropertyDescriptor(m.getName(), thisClass);
			
	    	if(m.isAnnotationPresent(JsonPropertyDescription.class)) {
				JsonPropertyDescription jdp = m.getAnnotation(JsonPropertyDescription.class);
		    	// Treat members that are a type of generated class in an array :
		    	// Since there is not any method to retrive the type of elements there is a trick
		        // We need an annotation begining with a prefix
		        // The end of the annotation contains the type name
		        if(jdp.value().startsWith(PREFIX_FOR_ARRAY)) {
		        	String className = jdp.value().substring(PREFIX_FOR_ARRAY.length());
			        Class<?> nestedClass = getClassOfName(lgc, className);
		    		try{
    					Object nestedInstance = nestedClass.newInstance();
    					
			        	visitGeneratedClass(nestedClass, fvts, nestedInstance, lgc);
 			        	ArrayList<Object> nestedList = new ArrayList<Object>();
			        	nestedList.add(nestedInstance);
			        	pd.getWriteMethod().invoke(instance, nestedList);
					} catch (InstantiationException e) {
						logger.error("InstantiationException : " + e.getMessage());
					} catch (IllegalAccessException e) {
						logger.error("IllegalAccessException : " + e.getMessage());
					}		        	
		        } else {	        	
			        Object[] o = (Object[])fvts.getMap().get(jdp.value());
			        if(o != null) {	        	
						pd.getWriteMethod().invoke(instance, o);
			        }
		        }
	        }
	    	// Treat members that are a type of generated class :
	    	// They are not annoted, but can contain annoted members
	    	else if(m.getType().getName().compareTo(Integer.class.getName()) != 0
	    			&& m.getType().getName().compareTo(String.class.getName()) != 0
	    			&& m.getType().getName().compareTo(Double.class.getName()) != 0) {
	    		try{
    					Object nestedInstance = m.getType().newInstance();
			        	visitGeneratedClass(m.getType(), fvts, nestedInstance, lgc);
			        	pd.getWriteMethod().invoke(instance, nestedInstance);
				} catch (InstantiationException e) {
					logger.error("InstantiationException : " + e.getMessage());
				} catch (IllegalAccessException e) {
					logger.error("IllegalAccessException : " + e.getMessage());
				}
	        }
		} catch (IllegalAccessException e) {
			logger.error("IllegalAccessException : " + e.getMessage());
		} catch (InvocationTargetException e) {
			logger.error("InvocationTargetException : " + e.getMessage());
		} catch (IntrospectionException e) {
			logger.error("IntrospectionException : " + e.getMessage());
		} catch (IllegalArgumentException e) {
			logger.error("IllegalArgumentException : " + e.getMessage());
		}
	}
	
	private static String generatedObjectSerialization(Object instance) {
		String jsonReturnValue = "";
	    try {
			ObjectMapper mapper = new ObjectMapper();
			jsonReturnValue = mapper.writeValueAsString(instance);
		} catch (JsonGenerationException e) {
			logger.error("JsonGenerationException : " + e.getMessage());
		} catch (JsonMappingException e) {
			logger.error("JsonMappingException : " + e.getMessage());
		} catch (IOException e) {
			logger.error("IOException : " + e.getMessage());
		}
		return jsonReturnValue;
	}
	
	private static Class<?> getMainClass(List<GeneratedClass> lgc) {
		Class<?> mainClass = null;
		for(GeneratedClass gc : lgc) {
			if(gc.isMainClass()) mainClass = gc.getGeneratedClass();
		}
		return mainClass;
	}
	
	private static Class<?> getClassOfName(List<GeneratedClass> lgc, String className) {
		Class<?> classToGet = null;
		for(GeneratedClass gc : lgc) {
			if(gc.getName().compareTo(className) == 0) classToGet = gc.getGeneratedClass();
		}
		return classToGet;
	}
}
