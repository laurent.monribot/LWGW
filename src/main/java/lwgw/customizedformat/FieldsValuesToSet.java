package lwgw.customizedformat;

import java.util.TreeMap;

import lwgw.treatment.MonitoringThread;
import lwgw.treatment.TreatingMessageThread;

/** Object that needs to be defined according to
 * the fields of the json message to send.
 * It is used by {@link MonitoringThread} and
 * {@link TreatingMessageThread}.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public abstract class FieldsValuesToSet {
	protected TreeMap<String,Object[]> map;
	
	public TreeMap<String, Object[]> getMap() {
		return map;
	}	 
}
