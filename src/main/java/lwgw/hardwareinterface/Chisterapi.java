package lwgw.hardwareinterface;

/**
 * This is the plug for the native library to the Snootlab ChisteraPi card.
 * @author Laurent Monribot
 * @version 1.0.0
 */
public class Chisterapi {
	private double frequency;
	private int codeRedundancy;
	private int spreadingFactor;
	private int bandWidth;
	private int power;
	
	static {
		System.loadLibrary("chisterapi");
	}

	/** This method starts the library with all settings previously done.
	 * If not, these values are used : frequency = 868.0, bandwidth = 125,
	 * checkredundancy = 48, spreadingfactor = 12 and power = 23.
	 * It can be called as many time as Lora radio settings are changed.
	 * @return True if ok.
	 */
	public native boolean setup();
	
	/** This method is not used. 
	 * @return True if ok ?
	 */
	public native boolean sleep();

	/** This method switches the library in the listening mode.
	 * @return True if the switch was done instantaneously.
	 */
	public native boolean available();

	/** This method is used for broadcasting Lora radio messages.
	 * @param msg The message to broadcast.
	 * @return True if ok.
	 */
	public native boolean send(byte[] msg);
	
	/** This method is used for reading presence of the last received
	 * Lora radio message, since the available() method was called.
	 * @param len The maximum message length that can be received.
	 * @return The message received.
	 */
	public native byte[] receive(int len);

	/** This method returns the maximum Lora radio message
	 * that can be received.
	 * @return The length in bytes.
	 */
	public native int getMaxMsgLength();
	/** This method returns the Received Signal Strength Indication
	 * of the last received message.
	 * @return The RSSI.
	 */
	public native int getLastRssi();
	
	/** This method returns the Signal to Noise Ratio
	 * of the last received message.
	 * @return The SNR.
	 */
	public native int getLastSnr();
	
	/** This is the blocking method for Lora radio messages reception.
	 * @param len The maximum message length that can be received.
	 * @param timeout If 0, this means no timeout.
	 * @return The received message.
	 */
	public native byte[] receiveWaitAvailable(int len, int timeout);
	private native boolean setFreq(Double freq);
	private native boolean setCr(int cr);
	private native boolean setSf(int sf);
	private native boolean setBw(int bw);
	private native boolean setPw(int pw);

	/** This method sets the Lora radio frequency 
	 * 868.10 Mhz (used by Gateway to listen ),
	 * 868.30 MHz (used by Gateway to listen ),
	 * 868.50 MHz (used by Gateway to listen ),
	 * 864.10 MHz (used by End device to transmit Join Request),
	 * 864.30 MHz (used by End device to transmit Join Request),
	 * 864.50 MHz (used by End device to transmit Join Request),
	 * 868.10 MHz (used by End device to transmit Join Request),
	 * 868.30 MHz (used by End device to transmit Join Request) and
	 * 868.50 MHz (used by End device to transmit Join Request).
	 * @param freq The frequency.
	 * @return True if ok.
	 */
	public boolean setFrequency(Double freq) {
		boolean resultIsOk = setFreq(freq);
		if(resultIsOk) frequency = freq;
		return resultIsOk;
	}
	
	/** This method changes the code redundancy. 
	 * Acceptable values :
	 * 45 = codeRedundancy 4/5,
	 * 46 = codeRedundancy 4/6,
	 * 47 = codeRedundancy 4/7 and
	 * 48 = codeRedundancy 4/8.
	 * @param cr The code redundancy.
	 * @return True if ok.
	 */
	public boolean setCodeRedundancy(int cr) {
		boolean resultIsOk = setCr(cr);
		if(resultIsOk) codeRedundancy = cr;		
		return resultIsOk;
	}
	
	/** This method changes the spreading factor.
	 * Acceptable values :
	 * 7 = 128 chips/symbols,
	 * 8 = 256 chips/symbols,
	 * 9 = 512 chips/symbols,
	 * 10 = 1024 chips/symbols,
	 * 11 = 2048 chips/symbols and
	 * 12 = 4096 chips/symbols.
	 * @param sf The spreading factor.
	 * @return true if ok.
	 */
	public boolean setSpreadingFactor(int sf) {
		boolean resultIsOk = setSf(sf);
		if(resultIsOk) spreadingFactor = sf;
		return resultIsOk;
	}
	
	/** This method changes the band width. 
	 * Acceptable values :
	 * 7.8 kHz,
	 * 10.4 kHz,
	 * 15.6 kHz,
	 * 20.8 kHz,
	 * 31.2 kHz,
	 * 41.7 kHz,
	 * 62.5 kHz,
	 * 125 kHz,
	 * 250 kHz and
	 * 500 kHz.
	 * @param bw The bandwidth.
	 * @return True if ok.
	 */
	public boolean setBandWidth(int bw) {
		boolean resultIsOk = setBw(bw);
		if(resultIsOk) bandWidth = bw;
		return resultIsOk;
	}
	
	/** This method changes the power of broadcasting. 
	 * Acceptable values are from 5 to 23.
	 * @param pw The power.
	 * @return True if ok.
	 */
	public boolean setPower(int pw) {
		boolean resultIsOk = setPw(pw);
		if(resultIsOk) power = pw;		
		return resultIsOk;
	}
	
	public double getFrequency() {
		return frequency;
	}
	public int getCodeRedundancy() {
		return codeRedundancy;
	}
	public int getSpreadingFactor() {
		return spreadingFactor;
	}
	public int getBandWidth() {
		return bandWidth;
	}
	public int getPower() {
		return power;
	}

	public static void main(String ... args) {

		Chisterapi lora = new Chisterapi();
		System.out.println("Lora setup");
		lora.setup();
		boolean loraAvailability = lora.available();
		System.out.println("Asking for lora availability : " + loraAvailability);
		//if(!loraAvailability) return;
		
		System.out.println("Sending 2 bytes : ");
		lora.send(new byte[] { (byte)0x01, (byte)0x02 });
		System.out.println("Now sleep");
		lora.sleep();
	}
}
