

bool chisterapi_send(unsigned char *, int);
int chisterapi_receive(unsigned char *);
bool chisterapi_setup();
bool chisterapi_sleep();
bool chisterapi_available();
int chisterapi_getmaxmsglength();
int chisterapi_getlastrssi();
int chisterapi_getlastsnr();
int chisterapi_receivewaitavailable(unsigned char *, int);
bool chisterapi_setfreq(double);
bool chisterapi_setcr(int);
bool chisterapi_setsf(int);
bool chisterapi_setbw(int);
bool chisterapi_setpw(int);
