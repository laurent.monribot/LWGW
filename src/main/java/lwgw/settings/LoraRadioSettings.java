package lwgw.settings;

import lwgw.databaseoperations.DatabaseOperation;
import lwgw.databaseoperations.LoraSettingsChange;
import lwgw.main.LwgwProperties;

import java.util.Queue;

import com.udojava.jmx.wrapper.JMXBean;
import com.udojava.jmx.wrapper.JMXBeanAttribute;


/**
* Specific settings for Lora radio listening of the gateway.
* This object is used as a bean in the JMX JConsole.
* @author Laurent Monribot
* @version 0.0.0
*/
@JMXBean(description = "Lora Radio Listening Settings")
public class LoraRadioSettings {
	private LwgwProperties lwgwProperties;
	private double frequency;
	private int power;
	private int codeRedundancy;
	private int spreadingFactor;
	private int bandwidth;
	private boolean modifiedAndNotValidated;

	public LoraRadioSettings(LwgwProperties lwgwProperties) {
		this.lwgwProperties = lwgwProperties;
		frequency = lwgwProperties.getLoraRadioFrequency();
		power = lwgwProperties.getLoraRadioPower();
		codeRedundancy = lwgwProperties.getLoraRadioCodeRedundancy();
		spreadingFactor = lwgwProperties.getLoraRadioSpreadingFactor();
		bandwidth = lwgwProperties.getLoraRadioBandwidth();
		modifiedAndNotValidated = false;
	}
	
    @JMXBeanAttribute(name="Frequency", description="Between 863 and 870 Mhz")
	public double getFrequency() {
		return frequency;
	}

    @JMXBeanAttribute(name="Frequency", description="Between 863 and 870 Mhz")
	public void setFrequency(double frequency) {
    	double frequencySave = this.frequency;
		this.frequency = frequency;
		if(frequencySave != this.frequency) modifiedAndNotValidated = true;

	}

    @JMXBeanAttribute(name="Power", description="Power of emission, between 5 and 23")
	public int getPower() {
		return power;
	}

    @JMXBeanAttribute(name="Power", description="Power of emission, between 5 and 23")
	public void setPower(int power) {
    	int powerSave = this.power;
		this.power = power;
		if(powerSave != this.power) modifiedAndNotValidated = true;
	}

    @JMXBeanAttribute(name="Code Redundancy", description="45, 46, 47 and 48 are accepted")
	public int getCodeRedundancy() {
		return codeRedundancy;
	}

    @JMXBeanAttribute(name="Code Redundancy", description="45, 46, 47 and 48 are accepted")
	public void setCodeRedundancy(int codeRedundancy) {
    	int codeRedundancySave = this.codeRedundancy;
		this.codeRedundancy = codeRedundancy;
		if(codeRedundancySave != this.codeRedundancy) modifiedAndNotValidated = true;
	}

    @JMXBeanAttribute(name="Spreading Factor", description="7, 8, 9, 10, 11 and 12 are accepted")
	public int getSpreadingFactor() {
		return spreadingFactor;
	}

    @JMXBeanAttribute(name="Spreading Factor", description="7, 8, 9, 10, 11 and 12 are accepted")
	public void setSpreadingFactor(int spreadingFactor) {
    	int spreadingFactorSave = this.spreadingFactor;
		this.spreadingFactor = spreadingFactor;
		if(spreadingFactorSave != this.spreadingFactor) modifiedAndNotValidated = true;
	}

    @JMXBeanAttribute(name="Bandwidth", description="125, 250 and 500 are accepted")
	public int getBandwidth() {
		return bandwidth;
	}

    @JMXBeanAttribute(name="Bandwidth", description="125, 250 and 500 are accepted")
	public void setBandwidth(int bandwidth) {
    	int bandwidthSave = this.bandwidth;
		this.bandwidth = bandwidth;
		if(bandwidthSave != this.bandwidth) modifiedAndNotValidated = true;
	}

	@JMXBeanAttribute(name = "Radio settings reload needed", description = "Modified and not validated object, statistics display continues to be updated")
	public boolean isModifiedAndNotValidated() {
		return modifiedAndNotValidated;
	}
	
	public void resetInterface() {
		modifiedAndNotValidated = false;
	}
	
	@Override
	public String toString() {
		return "LoraRadioSettings [frequency=" + frequency + ", power=" + power + ", codeRedundancy=" + codeRedundancy
				+ ", spreadingFactor=" + spreadingFactor + ", bandwidth=" + bandwidth + "]";
	}
	
	public void save(LoraWanServer s, Queue<DatabaseOperation> dbFifo) {
		lwgwProperties.setLoraRadioFrequency(frequency);
		lwgwProperties.setLoraRadioPower(power);
		lwgwProperties.setLoraRadioCodeRedundancy(codeRedundancy);
		lwgwProperties.setLoraRadioSpreadingFactor(spreadingFactor);
		lwgwProperties.setLoraRadioBandwidth(bandwidth);
		
		LoraSettingsChange loraSettingsChange = new LoraSettingsChange();
		loraSettingsChange.setLoraRadioSettings(this);
		loraSettingsChange.setServer(s);
		dbFifo.add(new DatabaseOperation(loraSettingsChange));
		
		resetInterface();
	}
}
