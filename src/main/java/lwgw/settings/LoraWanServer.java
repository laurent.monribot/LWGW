package lwgw.settings;


import lwgw.customizedformat.GeneratedClass;
import lwgw.customizedformat.JsonSchemaConverter;
import lwgw.jmx.JmxLoraWanServerAdapter;

import lwgw.treatment.context.LoraWanServerStatistics;

import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;

import com.udojava.jmx.wrapper.JMXBean;
import com.udojava.jmx.wrapper.JMXBeanAttribute;


/**
* This is the object containing data relative to a server.
* Thus, all specifics values needed for communicate with a server
* are set in this object.
* This object is used as a bean in the JMX JConsole with a
* {@link JmxLoraWanServerAdapter} adapter.
* @author Laurent Monribot
* @version 0.0.0
*/
@JMXBean(description = "LoraWan Server Settings")
public class LoraWanServer {
	private final static Logger logger = Logger.getLogger(LoraWanServer.class);
	protected String address;
	protected int port;
	protected boolean activated;
	// statistic values
	protected LoraWanServerStatistics statistics;

	// LoraWan protocol used by this server
	protected byte protocolVersion;
	protected String radioJsonSchema;
	protected String statJsonSchema;
	protected List<GeneratedClass> radioGeneratedClasses;
	protected List<GeneratedClass> statGeneratedClasses;
	
	// database index
	private int id;
	
	protected String creationDate;

	// List management in jmx
	private JmxLoraWanServerAdapter adapter;
	
	
	// url schema
	// hashmap<url serveur, url schema>

	public LoraWanServer(String address, int port) {
		this.address = address;
		this.port = port;
		activated = false;
		protocolVersion = 0x01;
		id = 0;
		radioJsonSchema = "";
		statJsonSchema = "";
		statistics = new LoraWanServerStatistics();
		adapter = null;
		radioGeneratedClasses = null;
		statGeneratedClasses = null;
	}

	public LoraWanServer(LoraWanServer s) {
		address = s.address;
		port = s.port;
		activated = s.activated;
		protocolVersion = s.protocolVersion;
		id = s.id;
		radioJsonSchema = s.radioJsonSchema;
		statJsonSchema = s.statJsonSchema;
		// Always get statistics currently used to avoid
		// a loss of data :
		// JMX annotations forces us to keep
		// the statistics interface in LoraWanServer
		statistics = s.statistics;
		// The adapter will be affected by JMX manager :
		adapter = null;
		
		radioGeneratedClasses = s.radioGeneratedClasses;
		statGeneratedClasses = s.statGeneratedClasses;
	}
	
	@JMXBeanAttribute(name = "Address", description = "IP V4")
	public String getAddress() {
		return address;
	}

	@JMXBeanAttribute(name = "Address", description = "IP V4")
	public void setAddress(String address) {
		String addressSave = this.address; 
		this.activated = (address.length() > 0 && port > 0) ? activated : false;
		this.address = address;
		if(addressSave != this.address) {
			setNewsSatistics(new LoraWanServerStatistics());
		}
		
		if(this.adapter != null) adapter.setAddress(address); 
	}
	
	@JMXBeanAttribute(name = "Port", description = "IP V4")
	public int getPort() {
		return port;
	}

	@JMXBeanAttribute(name = "Port", description = "IP V4")
	public void setPort(int port) {
		int portSave = this.port; 
		this.activated = (address.length() > 0 && port > 0) ? activated : false;
		this.port = port;
		if(portSave != this.port) {
			setNewsSatistics(new LoraWanServerStatistics());
		}
		
		if(this.adapter != null) adapter.setPort(port); 
	}

	@JMXBeanAttribute(name = "Activated", description = "Used or not currently by the Gateway")
	public boolean isActivated() {
		return activated;
	}

	@JMXBeanAttribute(name = "Activated", description = "Used or not currently by the Gateway")
	public void setActivated(boolean activated) {
		this.activated = (address.length() > 0 && port > 0) ? activated : false;
		updateAdapter(); 
	}

	@JMXBeanAttribute(name = "Number Messages sent", description = "Number of Messages forwarded to the server")
	public int getNbMesSent() {
		return statistics.getNbMesSent();
	}

	public void incNbMesSent() {
		statistics.incNbMesSent();
	}

	@JMXBeanAttribute(name = "Number Messages Aknowledged", description = "Number of Messages Acknowledged")
	public int getNbMesAcknowledged() {
		return statistics.getNbMesAcknowledged();
	}

	public void incNbMesAcknowledged() {
		statistics.incNbMesAcknowledged();;
	}

	@JMXBeanAttribute(name = "Nb Last Consec Mes Not Ack", description = "Number of Last Consecutives Messages Not Acknowledged")
	public int getNbLastConsecutiveMesNotAck() {
		return statistics.getNbLastConsecutiveMesNotAck();
	}

	public void incNbLastConsecutiveMesNotAck() {
		statistics.incNbLastConsecutiveMesNotAck();
	}

	public void setNewsSatistics(LoraWanServerStatistics newsStatistics) {
		statistics = newsStatistics;
		if(this.adapter != null) adapter.setNewsSatistics(newsStatistics);
	}

	@Override
	public String toString() {
		return "LoraWanServer [address=" + address + ", port=" + port + ", activated=" + activated + ", nbMesSent="
				+ statistics + ", protocolVersion=" + protocolVersion + ", id=" + id + ", adapter="
				+ adapter + "]";
	}

	@JMXBeanAttribute(name = "Protocol version", description = "LoraWan protocol version (1 or 2)")
	public byte getProtocolVersion() {
		return protocolVersion;
	}

	@JMXBeanAttribute(name = "Protocol version", description = "LoraWan protocol version (1 or 2)")
	public void setProtocolVersion(byte protocolVersion) {
		this.protocolVersion = protocolVersion;
		updateAdapter();
	}
	
	@JMXBeanAttribute(name = "Radio JSON Schema", description = "LoraWan personalized content for radio encapsulation")
	public String getRadioJsonSchema() {
		return radioJsonSchema;
	}

	@JMXBeanAttribute(name = "Radio JSON Schema", description = "LoraWan personalized content for radio encapsulation")
	public void setRadioJsonSchema(String jsonSchema) {
		radioGeneratedClasses = null;			
		if(jsonSchema!= null && jsonSchema.length()!=0) {
			String className = makeGeneratedClassName();
			className += "radio";
			this.radioJsonSchema = jsonSchema;
			
			JsonSchemaConverter converter = new JsonSchemaConverter(className, jsonSchema);
			radioGeneratedClasses = converter.getGeneratedClasses();
		}
		updateAdapter();
	}

	@JMXBeanAttribute(name = "Stat JSON Schema", description = "LoraWan personalized content for statistics")
	public String getStatJsonSchema() {
		return statJsonSchema;
	}

	@JMXBeanAttribute(name = "Stat JSON Schema", description = "LoraWan personalized content for statistics")
	public void setStatJsonSchema(String jsonSchema) {
		statGeneratedClasses = null;
		if(jsonSchema!= null && jsonSchema.length()!=0) {
			String className = makeGeneratedClassName();
			className += "stat";
			this.statJsonSchema = jsonSchema;
			
			JsonSchemaConverter converter = new JsonSchemaConverter(className, jsonSchema);
			statGeneratedClasses = converter.getGeneratedClasses();
		}
		updateAdapter();
	}

	@JMXBeanAttribute(name = "Creation date", description = "First time this server was used")
	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
		updateAdapter();
	}

	/** Method used only with the Dao.
	 * @param nb Number of messages sent to this server
	 */
	public void setNbMesSent(int nb) {
		statistics.setNbMesSent(nb);
		updateAdapter();
	}
	
	/** Method used only with the Dao.
	 * @param nb Number of messages acknowledged by this server
	 */
	public void setNbMesAcknowledged(int nb) {
		statistics.setNbMesAcknowledged(nb);
	}

	public int getId() {
		return id;
	}

	/** Method used only with the Dao.
	 * @param id Id of this server in database.
	 */
	public void setId(int id) {
		this.id = id;
		if(this.adapter != null) adapter.setId(id);
	}

	/** This method serves to set an adaptator for JMX JConsole.
	 * @param adapter The Adatper that will display the server on the JMX JConsole. 
	 */
	public void setAdapter(JmxLoraWanServerAdapter adapter) {
		this.adapter = adapter;
	}

	// For LwgwSettings :
	/**
	 * This method serves the {@link LwgwSettings} object
	 * to get modifications done by a user in the JMX JConsole. 
	 * @return The JMX JConsole adapter.
	 */
	public JmxLoraWanServerAdapter getAdapter() {
		return this.adapter;
	}
	
	private void updateAdapter() {
		if(this.adapter != null) {
			// adapter will update itself with our values
			adapter.update(this);
		}
	}
	
	/** This method is overridden by adapter child class.
	 * @param s The server the current {@link JmxLoraWanServerAdapter} is representing
	 */
	public void update(LoraWanServer s) {
	}

	/** This method is needed in case of a JSon schema presence in the JMX JConsole
	 * and is used for Lora radio message encapsulation.
	 * @return The generated class serving to produce a Json output
	 */
	public List<GeneratedClass> getRadioGeneratedClasses() {
		return radioGeneratedClasses;
	}

	/** This method is needed in case of a JSon schema presence in the JMX JConsole
	 * and is used for sending statistics to the server.
	 * @return The generated class serving to produce a Json output
	 */
	public List<GeneratedClass> getStatGeneratedClasses() {
		return statGeneratedClasses;
	}
	
	private String makeGeneratedClassName() {
		ISOChronology chrono = ISOChronology.getInstance();
		DateTime dt = DateTime.now(chrono);
		String creationDate = dt.toString();
		
		String className = "S" + address + "P" + port + "D" + creationDate;
		className = className.replace('.', 'x');
		className = className.replace(' ', 'x');
		className = className.replace(':', 'x');
		className = className.replace('-', 'x');
		
		return className;
	}
}
