package lwgw.settings;

import java.util.ArrayList;
import java.util.Queue;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;

import com.udojava.jmx.wrapper.JMXBean;
import com.udojava.jmx.wrapper.JMXBeanAttribute;
import com.udojava.jmx.wrapper.JMXBeanOperation;
import com.udojava.jmx.wrapper.JMXBeanOperation.IMPACT_TYPES;

import lwgw.dao.DatabaseManager;
import lwgw.databaseoperations.DatabaseOperation;
import lwgw.databaseoperations.ServerDatabaseOperation;
import lwgw.databaseoperations.ServersListUpdate;
import lwgw.downstreamdata.JsonServerCommand;
import lwgw.hardwareinterface.Chisterapi;
import lwgw.settings.LoraRadioSettings;
import lwgw.settings.LoraWanServer;
import lwgw.treatment.context.LoraWanServerStatistics;
import lwgw.jmx.JmxManager;
import lwgw.main.LwgwProperties;

/**
 * This is the object containing data relative to the gateway itself. This
 * object is used as a bean in the Jconsole.
 * 
 * @author Laurent Monribot
 * @version 0.0.0
 */
@JMXBean(description = "Interoperable LoraWan Gateway")
public class LwgwSettings {
	private final static Logger logger = Logger.getLogger(LwgwSettings.class);

	// Change this state for taking in account new settings
	private int state;

	private Chisterapi loraHardwareInterface;
	private boolean loraHardwareInterfaceOk;

	// If you want acknowledge connected objects messages
	private boolean radioAck;

	// LoRa listening settings
	private LoraRadioSettings loraRadioSettings;

	// Gateway periodic checking :
	private int monitoringDelayInterval;
	// Gateway periodic pull server message :
	private int pullMessageDelayInterval;
	// Gteway periodic statistic message :
	private int statMessageDelayInterval;
	// Communication failure management :
	private int udpSocketTimeout;
	private int maxNumberOfRetry;

	// Database file name (without extension)
	// in case the JMX JConsole user wants to change it :
	private String databaseFileName;
	
	private ArrayList<LoraWanServer> serversList;
	private DatabaseManager dbManager = null;
	private LwgwProperties lwgwProperties = null;
	
	private int appRadioMessageCounter;
	
	private String latitude;
	private String longitude;
	private String altitude;
	
	private int maximumLoraMessageLength = 0;
	private int[] statOnMessagesLengths = null;
	
	private Queue<DatabaseOperation> dbFifo;

	public LwgwSettings(DatabaseManager dbManager, LwgwProperties lwgwProperties, Queue<DatabaseOperation> q) {
		ISOChronology chrono = ISOChronology.getInstance();
		DateTime dt = DateTime.now(chrono);
		String time = dt.toString();

		this.dbManager = dbManager;
		this.lwgwProperties = lwgwProperties;

		loraRadioSettings = new LoraRadioSettings(lwgwProperties);
		
		appRadioMessageCounter = lwgwProperties.getApplicationRadioMessageCounter();
		
		monitoringDelayInterval = lwgwProperties.getMonitoringDelay();
		pullMessageDelayInterval = lwgwProperties.getPullMessageDelay();
		statMessageDelayInterval = lwgwProperties.getStatisticsDelay();
		udpSocketTimeout = lwgwProperties.getUdpSocketTimeOut();
		maxNumberOfRetry = lwgwProperties.getMaxRetryNumber();
		databaseFileName = lwgwProperties.getDatabaseFileName();
		radioAck = lwgwProperties.isRadioAcknowledgment();
		latitude = lwgwProperties.getLatitude();
		longitude = lwgwProperties.getLongitude(); 
		altitude = lwgwProperties.getAltitude();

		serversList = dbManager.getServerDao().readActivesServers();
		if (serversList.isEmpty()) {
			LoraWanServer s = null;
			serversList = new ArrayList<LoraWanServer>();
			s = new LoraWanServer(lwgwProperties.getDefaultIpServerAddress(),
					lwgwProperties.getDefaultIpServerPort());
			s.setCreationDate(time);
			serversList.add(0, s);
			dbManager.getServerDao().insert(s);
		}
		for(LoraWanServer s : serversList) {
			s.setActivated(true);
			dbManager.getServerActivationDao().insert(s, time);
		}
		
		// Blanck position in case a JMX console user wants to add a server
		// in the list :
		serversList.add(serversList.size(), new LoraWanServer("", 0));
		
		loraHardwareInterface = null;
		loraHardwareInterfaceOk = false;

		state = JmxManager.LWGW_STATE_RUN;
		
		this.dbFifo = q;
	}

	public ArrayList<LoraWanServer> getServersList() {
		return serversList;
	}

	public int getState() {
		return state;
	}

	public void setState(int s) {
		state = s;
	}

	@JMXBeanAttribute(name = "Radio Acknowledgment", description = "Reply or not to radio messages")
	public boolean getRadioAck() {
		return radioAck;
	}

	@JMXBeanAttribute(name = "Radio Acknowledgment", description = "Reply or not to radio messages")
	public void setRadioAck(boolean radioAck) {
		lwgwProperties.setRadioAcknowledgment(radioAck);
		this.radioAck = radioAck;
	}

	public LoraRadioSettings getLoraRadioSettings() {
		return loraRadioSettings;
	}

	public void setLoraRadioSettings(LoraRadioSettings loraRadioSettings) {
		this.loraRadioSettings = loraRadioSettings;
	}

	@JMXBeanAttribute(name = "Monitoring Delay Interval", description = "Periodic time to do some checks")
	public int getMonitoringDelayInterval() {
		return monitoringDelayInterval;
	}

	@JMXBeanAttribute(name = "Monitoring Delay Interval", description = "Periodic time to do some checks")
	public void setMonitoringDelayInterval(int monitoringDelayInterval) {
		lwgwProperties.setMonitoringDelay(monitoringDelayInterval);
		this.monitoringDelayInterval = monitoringDelayInterval;
	}

	@JMXBeanAttribute(name = "Pull Message Delay Interval", description = "Periodic time to send a pull message in order to keep connection opened for receiving commands")
	public int getPullMessageDelayInterval() {
		return pullMessageDelayInterval;
	}

	@JMXBeanAttribute(name = "Pull Message Delay Interval", description = "Periodic time to send a pull message in order to keep connection opened for receiving commands")
	public void setPullMessageDelayInterval(int pullMessageDelayInterval) {
		lwgwProperties.setPullMessageDelay(pullMessageDelayInterval);
		this.pullMessageDelayInterval = pullMessageDelayInterval;
	}

	@JMXBeanAttribute(name = "Stat Message Delay Interval", description = "Periodic time to send statistics")
	public int getStatMessageDelayInterval() {
		return statMessageDelayInterval;
	}

	@JMXBeanAttribute(name = "Stat Message Delay Interval", description = "Periodic time to send statistics")
	public void setStatMessageDelayInterval(int statMessageDelayInterval) {
		lwgwProperties.setStatisticsDelay(statMessageDelayInterval);
		this.statMessageDelayInterval = statMessageDelayInterval;
	}

	@JMXBeanAttribute(name = "UDP Socket Timeout", description = "Time to wait a server UDP response in milliseconds")
	public int getUdpSocketTimeout() {
		return udpSocketTimeout;
	}

	@JMXBeanAttribute(name = "UDP Socket Timeout", description = "Time to wait a server UDP response in milliseconds")
	public void setUdpSocketTimeout(int udpSocketTimeout) {
		lwgwProperties.setUdpSocketTimeOut(udpSocketTimeout);
		this.udpSocketTimeout = udpSocketTimeout;
	}

	@JMXBeanAttribute(name = "Max Number Of Retries", description = "Number of times a message not acknowledged has to be repeated")
	public int getMaxNumberOfRetry() {
		return maxNumberOfRetry;
	}

	@JMXBeanAttribute(name = "Max Number Of Retries", description = "Number of times a message not acknowledged has to be repeated")
	public void setMaxNumberOfRetry(int maxNumberOfRetry) {
		lwgwProperties.setMaxRetryNumber(maxNumberOfRetry);
		this.maxNumberOfRetry = maxNumberOfRetry;
	}

	@JMXBeanAttribute(name = "Database File Name", description = "This is the file name to open when application starts, without extension")
	public String getDatabaseFileName() {
		return databaseFileName;
	}

	@JMXBeanAttribute(name = "Database File Name", description = "This is the file name to open when application starts, without extension")
	public void setDatabaseFileName(String databaseFileName) {
		lwgwProperties.setDatabaseFileName(databaseFileName);
		this.databaseFileName = databaseFileName;
	}

	@JMXBeanAttribute(name = "Radio Message Number", description = "This is number of Lora radio message intercepted since the application installation")
	public String getApplicationRadioMessageCounter() {
		return String.valueOf(appRadioMessageCounter);
	}

	public void setApplicationRadioMessageCounter(int appRadioMessageCounter) {
		lwgwProperties.setApplicationRadioMessageCounter(appRadioMessageCounter);
		this.appRadioMessageCounter = appRadioMessageCounter;
	}

	@JMXBeanAttribute(name = "Gateway Latitude", description = "Geographical position : Latitude")
	public String getLatitude() {
		return latitude;
	}

	@JMXBeanAttribute(name = "Gateway Latitude", description = "Geographical position : Latitude")
	public void setLatidute(String latitude) {
		lwgwProperties.setLatidute(latitude);
		this.latitude = latitude;
	}

	@JMXBeanAttribute(name = "Gateway Longitude", description = "Geographical position : Longitude")
	public String getLongitude() {
		return longitude;
	}

	@JMXBeanAttribute(name = "Gateway Longitude", description = "Geographical position : Longitude")
	public void setLongitude(String longitude) {
		lwgwProperties.setLongitude(longitude);
		this.longitude = longitude;
	}

	@JMXBeanAttribute(name = "Gateway Altitude", description = "Geographical position : Altitude")
	public String getAltitude() {
		return altitude;
	}

	@JMXBeanAttribute(name = "Gateway Altitude", description = "Geographical position : Altitude")
	public void setAltitude(String altitude) {
		lwgwProperties.setAltitude(altitude);
		this.altitude = altitude;
	}
	
	@JMXBeanAttribute(name = "Max Lora Message Length", description = "Maximum Lora Radio Message Length")	
	public int getMaximumLoraMessageLength() {
		return maximumLoraMessageLength;
	}


	/** JMX JConsole operation allowing the user to reload the list of Lora Wan Servers after a change.
	 * @return A message telling the user if the operation worked or not.
	 */
	@JMXBeanOperation(name = "Reload Servers List", description = "To take in account changes of servers list",
			impactType = IMPACT_TYPES.ACTION_INFO)
	public String LoadServersList() {
		// For queuing database write operations :
		ServersListUpdate serversListUpdate = new ServersListUpdate();
		ArrayList<ServerDatabaseOperation> operationsList = new ArrayList<ServerDatabaseOperation>();
		ServerDatabaseOperation sdo = null;
		// Other local variables :
		String lastAdrInList = "";
		int indice = 0;
		
		// Previous list leaved unchanged.
		ArrayList<LoraWanServer> previousServersList = serversList; 
		// It continues to be used for a while
		// by GatewayRecords objects that references it.
		// Previous list serves just to make a new updated list :
		serversList = new ArrayList<LoraWanServer>();
		
		for (LoraWanServer s : previousServersList) {
			//Update server statistic values in DB before get JConsole modifications
//			sdo = new ServerDatabaseOperation(s);
//			sdo.setUpdateForStatistics(true);
//			operationsList.add(sdo);
			
			if (s.getAdapter() == null || !s.getAdapter().isModifiedAndNotValidated()) {
				// Take the server as it is :
				serversList.add(indice++, s);
				logger.info("Server updated " + s);
				lastAdrInList = s.getAddress();

			} else if (s.getAdapter().getMarkForRemove()) {
				//This test here after is a protection from a deletion of a new server
				//that has not had enough time to register itself in database
				if(s.getId() !=0) {
					// You can drop it...
					//Close database line (add an end date)
					sdo = new ServerDatabaseOperation(s);
					sdo.setUpdateForTermination(true);
					operationsList.add(sdo);
					logger.info("Server removed " + s);
				} else {
					// Add the unmodified server in new list :
					serversList.add(indice++, new LoraWanServer(s));
					lastAdrInList = s.getAddress();
					logger.warn("Server couldn't be removed since it wasn't in database " + s);
				}
			} else {
				// Particular case when the position refers to a new server
				if(s.getAddress().compareTo(s.getAdapter().getAddress())!=0
						|| s.getPort() != s.getAdapter().getPort()) {
					boolean newServerOperationOk = true;
					
					if(s.getAddress().length() > 0 && s.getPort() != 0) {						
						//This is a protection from a deletion of a new server
						//that has not had enough time to register itself in database
						if(s.getId() !=0) {
							//Close database line (add an end date) :
							sdo = new ServerDatabaseOperation(s);
							sdo.setUpdateForTermination(true);
							operationsList.add(sdo);
						} else {
							newServerOperationOk = false;
							logger.warn("Server couldn't be replaced by another one since it wasn't in database " + s);
						}
					}
					if(newServerOperationOk) {
						//Leave the former reference being used for a while
						//And create a new one :
						s = new LoraWanServer(s.getAdapter());
						s.setNewsSatistics(new LoraWanServerStatistics());
						
						//Open a new line in database for the new server :
						ISOChronology chrono = ISOChronology.getInstance();
						DateTime dt = DateTime.now(chrono);
						s.setCreationDate(dt.toString());
	
						logger.info("Server database insertion : " + s.getAddress() + ":" + s.getPort());
						sdo = new ServerDatabaseOperation(s);
						sdo.setInsert(true);						
						// s has not yet got its id... but you can refer to it
						// due to ServerDatabaseOperation implementation 
						if(s.activated) {
							sdo.setInsertServerActivation(true);
						}
						operationsList.add(sdo);
					}
				} else {
					// Get all new values that the JConsole user could have modified :
					boolean updateProtoc = false;
					boolean updateRadSch = false;
					boolean updateStaSch = false;
					boolean updateActive = false;
					
					// Update server protocol version in DB and other values :
					if(s.getProtocolVersion() != s.getAdapter().getProtocolVersion()) {
						updateProtoc = true;
					}
					
					String strAdapterRadioJsonSchema = s.getAdapter().getRadioJsonSchema();
					String strCurrentRadioJsonSchema = s.getRadioJsonSchema();

					if(strCurrentRadioJsonSchema == null && strAdapterRadioJsonSchema != null) {
						updateRadSch = true;
					} else if(strCurrentRadioJsonSchema != null && strAdapterRadioJsonSchema == null) {
						updateRadSch = true;
					} else if(strCurrentRadioJsonSchema != null && 
							s.getRadioJsonSchema().compareTo(s.getAdapter().getRadioJsonSchema()) !=0) {
						updateRadSch = true;
					}
					
					String strAdapterStatJsonSchema = s.getAdapter().getStatJsonSchema();
					String strCurrentStatJsonSchema = s.getStatJsonSchema();

					if(strCurrentStatJsonSchema == null && strAdapterStatJsonSchema != null) {
						updateStaSch = true;
					} else if(strCurrentStatJsonSchema != null && strAdapterStatJsonSchema == null) {
						updateStaSch = true;
					} else if(strCurrentStatJsonSchema != null && 
							s.getStatJsonSchema().compareTo(s.getAdapter().getStatJsonSchema()) !=0) {
						updateStaSch = true;
					}
					
					if(s.isActivated() != s.getAdapter().isActivated()) {
						updateActive = true;
					}
										
					//Leave the former reference being used for a while
					//And create a new one :
					s = new LoraWanServer(s.getAdapter());
					
					if(updateProtoc) {
						logger.debug("Server protocol version to update");
						sdo = new ServerDatabaseOperation(s);
						sdo.setUpdateForProtocolVersion(true);
						operationsList.add(sdo);
					}
					if(updateRadSch) {
						logger.debug("Server radio messages to update");
						sdo = new ServerDatabaseOperation(s);
						sdo.setUpdateForRadioJsonSchema(true);
						operationsList.add(sdo);
					}
					if(updateStaSch) {
						logger.debug("Server statistics messages to update");
						sdo = new ServerDatabaseOperation(s);
						sdo.setUpdateForStatJsonSchema(true);
						operationsList.add(sdo);
					}
					if(updateActive) {
						logger.debug("Server activation update");
						sdo = new ServerDatabaseOperation(s);
						sdo.setInsertServerActivation(true);
						operationsList.add(sdo);
					}
				}
				
				// Add the modified server in new list :
				serversList.add(indice++, new LoraWanServer(s));
				logger.debug("Server updated " + s);
				lastAdrInList = s.getAddress();
			}
		}
		if (lastAdrInList == null || lastAdrInList.compareTo("") != 0) {
			// Blanck position in case a JMX console user wants to add a server
			// in the list :
			serversList.add(indice, new LoraWanServer("", 0));
		}
		// Force the Jconsole update with new appropriated beans :
		// Adapters will be changed.
		JmxManager.load(this);
		// Do all write operations in database :
		serversListUpdate.setOperationsList(operationsList);
		dbFifo.add(new DatabaseOperation(serversListUpdate));
		// Notice the main thread (go out and back again in its loop with new settings) :
		state = JmxManager.LWGW_STATE_RESTART;
		return "Servers list reloaded";
	}
	
/** JMX JConsole operation allowing the user to reload Lora radio listening settings after a change.
 * @return A message telling the user if the operation worked or not.
 */
	@JMXBeanOperation(name = "Reload Radio settings", description = "To take in account changes of radio settings",
			impactType = IMPACT_TYPES.ACTION_INFO)
	public String LoadLoraHardwareInterfaceSettings() {
		return LoadLoraHardwareInterfaceSettingsModifFromServer(null);
	}
	
	/**
	 * This method is called by the {@link JsonServerCommand}.perform() method.
	 * @param s The server that sent the command to perform, information that
	 * needs to be traced in the database record.
	 * @return A message telling the user if the operation worked or not used
	 * in the JMX JConsole.
	 */
	public String LoadLoraHardwareInterfaceSettingsModifFromServer(LoraWanServer s) {
		boolean returnValue = true;
		String strError = null;

		if (!loraHardwareInterface.setFrequency(new Double(loraRadioSettings.getFrequency()))) {
			returnValue = false;
			strError = "Irrecevable Frequency";
		} else if (!loraHardwareInterface.setPower(loraRadioSettings.getPower())) {
			returnValue = false;
			strError = "Irrecevable Power";
		} else if (!loraHardwareInterface.setCodeRedundancy(loraRadioSettings.getCodeRedundancy())) {
			returnValue = false;
			strError = "Irrecevable Code Redundancy";
		} else if (!loraHardwareInterface.setSpreadingFactor(loraRadioSettings.getSpreadingFactor())) {
			returnValue = false;
			strError = "Irrecevable Spreading Factor";
		} else if (!loraHardwareInterface.setBandWidth(loraRadioSettings.getBandwidth())) {
			returnValue = false;
			strError = "Irrecevable Bandwidth";
		}

		if (returnValue) {
			// try to stop and restart radio listening with new settings :
			if (!loraHardwareInterface.setup()) {
				returnValue = false;
				strError = "Irrecevable settings";
			} else {
				loraRadioSettings.save(s, dbFifo);
				strError = "New  " + loraRadioSettings + " OK";
			}
		}

		if (returnValue == true) {
			logger.info(strError);
			loraHardwareInterface.available();// Command for switching in listening mode
			maximumLoraMessageLength = loraHardwareInterface.getMaxMsgLength();
			messagesLengthsStatInitialisation();
			logger.info("Lora max length message : " + maximumLoraMessageLength);
		} else {
			logger.error(strError);
		}
		loraHardwareInterfaceOk = returnValue;
		return strError;
	}

	/** JMX JConsole operation allowing the user to stop properly the LWGW gateway.
	 * @return A message telling the user if the operation worked or not.
	 */
	@JMXBeanOperation(name = "Stop", description = "To stop definitly the Gateway",
			impactType = IMPACT_TYPES.ACTION_INFO)
	public String StopGateway() {
		state = JmxManager.LWGW_STATE_STOP;
		return "Shutdown";
	}

	public String setLoraHardwareInterface(Chisterapi lora) {
		this.loraHardwareInterface = lora;
		return LoadLoraHardwareInterfaceSettings();
	}

	public boolean getStatus() {
		return loraHardwareInterfaceOk;
	}
	
	public void saveAll() {
		ISOChronology chrono = ISOChronology.getInstance();
		DateTime dt = DateTime.now(chrono);
		String time = dt.toString();

		lwgwProperties.save(time);
		
		// Write in database servers inactivations and their statistics :
		for(LoraWanServer s : serversList) {
			if(s.isActivated() && s.getId() != 0) {
				logger.debug("Save statistics in database");
				LoraWanServer sCopy = new  LoraWanServer(s);
				sCopy.setActivated(false);
				dbManager.getServerActivationDao().insert(sCopy, time);
				dbManager.getServerDao().updateForStatistics(s);
			}
		}
	}
	private void messagesLengthsStatInitialisation(){
		statOnMessagesLengths = new int[maximumLoraMessageLength];
		for(int i=0; i<maximumLoraMessageLength; i++) {
			statOnMessagesLengths[i] = 0;
		}
	}
	
	public void incrementsReceivedMessagesOfLength(int l) {
		if(l <=maximumLoraMessageLength) {
			statOnMessagesLengths[l-1]++;
		}
	}
	
	@JMXBeanAttribute(name = "Average Length Of Messages", description = "Average length of received radio messages")
	public int getAverageLengthOfMessages() {
		int average = 0;
		int sum = 0;
		int numberOfReceivedMessages = 0;
		for(int i=0; i<maximumLoraMessageLength; i++) {
			sum += statOnMessagesLengths[i]*(i+1);
			numberOfReceivedMessages += statOnMessagesLengths[i];
		}
		if(numberOfReceivedMessages>0) {
			average = sum / numberOfReceivedMessages;
		}
		return average;
	}

}
