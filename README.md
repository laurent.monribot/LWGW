

Install and start the LWGW
==========================

Install
-------

This is the 0.0.1 version of the Interoperable Lora Wan Gateway.

This application works on a Raspberry Pi 2 the ChisteraPi card from Snootlab.

You have to install :

 * the wiringPi library to connect the Chistera Pi card
 * the sqlite3 database 

Then, just copy the target repository of the project on the Raspberry Pi 2.


Start
-----

Start with this command line :

sudo java -cp src -Djava.library.path=. -Djava.rmi.server.hostname=<RaspberryAddress> -Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=7097 -jar lwgw-0.0.1-jar-with-dependencies.jar

Then you can open a JConsole on the Raspberry Pi 2 with the address and port specified in this command line.


Go on and enjoy !