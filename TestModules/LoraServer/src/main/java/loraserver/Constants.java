package loraserver;

/**
* This is the object containing constants values for LoraWan messages headers.
* These constants are used for distinguish types of messages exchanged between a gateway and a server.
* Described in the <b>Semtech</b> documentation : <b>ANNWS.01.2.1.W.SYS</b> "Gateway to server interface".
* @author Laurent Monribot
* @version 0.0.0
*/
public class Constants {
	public final static byte MES_ID_PUSH_DATA  = 0x00; //PUSH_DATA
	public final static byte MES_ID_PUSH_ACK  = 0x01; // PUSH_ACK
	public final static byte MES_ID_PULL_DATA = 0x02; // PULL_DATA
	public final static byte MES_ID_PULL_RESP = 0x03; // PULL_RESP
	public final static byte MES_ID_PULL_ACK  = 0x04; // PULL_ACK
	public final static byte MES_ID_TX_ACK    = 0x05; // TX_ACK
}
