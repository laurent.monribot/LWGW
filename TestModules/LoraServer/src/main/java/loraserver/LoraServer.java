package loraserver;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import org.apache.log4j.Logger;


public class LoraServer {
	private static final int BUFFER_RECEPTION_SIZE = 2048;
	private final static Logger logger = Logger.getLogger(LoraServer.class);
	private DatagramSocket socket;
	
	public LoraServer() {
		try {
			socket = new DatagramSocket();
			socket.setSoTimeout(10);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void sendudp(byte[] datagram, InetAddress adr, int port) {
		try {
			DatagramPacket packet;
			packet = new DatagramPacket(datagram, datagram.length, adr, port);

			socket.send(packet);

		} catch (SocketException e) { // for the new Datagram...()
			logger.error(e.getMessage());
		} catch (IOException e) { // for the send()
			logger.error(e.getMessage() + " Address : " + adr.getHostAddress() + " Port : " + port);
		}
	}

	private DatagramPacket listen() {
		boolean somethingReceived = false;
		byte[] buffer = new byte[BUFFER_RECEPTION_SIZE];
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

		try {
			socket.receive(packet);
			if(packet.getLength()>0) somethingReceived = true;
 		} catch (SocketTimeoutException e) {
			//logger.warn(e.getMessage());
		} catch (IOException e) {
			logger.warn(e.getMessage());
		}
		if (!somethingReceived)
			return null;
		return packet;
	}

	private void treatMessage(DatagramPacket packet) {
		byte[] content, destination = null;
		String document1 = "{" + "\"imme\":10," + "\"freq\":869.3," + "\"rfch\":0," + "\"powe\":12,"
		+ "\"modu\":\"LORA\"," + "\"datr\":\"SF11BW125\"," /*+ "\"fdev\":3000,"*/ + "\"size\":32,"
		+ "\"data\":\"H3P3N2i9qc4yt7rK7ldqoeCVJGBybzPY5h1Dd7P7p8v\"" + "}";
		String document2 = "[{\"txpk\":{" + "\"imme\":10," + "\"freq\":861.3," + "\"rfch\":0," + "\"powe\":12,"
		+ "\"modu\":\"LORA\"," + "\"datr\":50000," + "\"ipol\":true," + "\"size\":32,"
		+ "\"data\":\"H3P3N2i9qc4yt7rK7ldqoeCVJGBybzPY5h1Dd7P7p8v\"" + "}}]";
		byte[] receivedData = packet.getData();

		if (receivedData[0] == 0x01 || receivedData[0] == 0x02) {
			if(receivedData[3] == Constants.MES_ID_PULL_DATA){
				byte[] header = new byte[12];
				// PULL_RESP has tow way interpreting token value (set to 0 in
				// V1 and set to token PULL in V2)
				header[3] = Constants.MES_ID_PULL_ACK;
				header[0] = 0x01;
				header[1] = receivedData[1];
				header[2] = receivedData[2];
				header[4] = receivedData[4];
				header[5] = receivedData[5];
				header[6] = receivedData[6];
				header[7] = receivedData[7];
				header[8] = receivedData[8];
				header[9] = receivedData[9];
				header[10] = receivedData[10];
				header[11] = receivedData[11];
				
				sendudp(header, packet.getAddress(), packet.getPort());
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				byte[] header2 = new byte[4];
				header2[3] = Constants.MES_ID_PULL_RESP;
				header2[0] = 0x01;
				header2[1] = 0;
				header2[2] = 0;
				// V2 :
				//header2[0] = 0x02;
				//header2[1] = receivedData[1];
				//header2[2] = receivedData[2];
				
				content = document1.getBytes();	
				logger.debug("JSON PART : " + new String(content));
				// Create a destination array that is the size of the two arrays
				destination = new byte[header2.length + content.length];
				System.arraycopy(header2, 0, destination, 0, header2.length);
				System.arraycopy(content, 0, destination, header2.length, content.length);

				sendudp(destination, packet.getAddress(), packet.getPort());

				for(int i = 0; i<10; i++) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					sendudp(destination, packet.getAddress(), packet.getPort());
				}
			}
			if(receivedData[3] == Constants.MES_ID_PUSH_DATA){
				byte[] header = new byte[4];
				header[3] = Constants.MES_ID_PUSH_ACK;
				header[0] = 0x01;
				// V2 :
				//header[0] = 0x02;
				header[1] = receivedData[1];
				header[2] = receivedData[2];
				sendudp(header, packet.getAddress(), packet.getPort());
				logger.debug("MES_ID_PUSH_ACK sent");
				int jsonOffset = 12; // why the offset is 12 bytes ? This is unknown...
				byte[] jsonPart  = new byte[packet.getLength()-jsonOffset];
				for(int i=0; i<packet.getLength()-jsonOffset; i++){
					jsonPart[i] = receivedData[i+jsonOffset];
				}
				logger.debug(new String(jsonPart));
			}
		}
	}
	
	public void loop() {
		DatagramPacket packet = null;
		int port = 1700;
		try {
			socket = new DatagramSocket(port);
			socket.setSoTimeout(100);

			logger.debug(" En ecoute sur le port : " + port);
			while (true) {
				packet = listen();
				if (packet != null && packet.getData().length != 0) {
					logger.info("Message recu de la gateway " + packet.getAddress() + " : " + packet.getPort());
					treatMessage(packet);
				}
			}
		} catch (SocketException e) { // for the new DatagramSocket()
			logger.error(e.getMessage());
		}

		socket.close();
		socket = null;
	}
	
	public static void main(String[] argv) {
		LoraServer loraServer = new LoraServer();
		loraServer.loop();
	}
}