/*******************************************************************************
 *
 * Modified file of Thomas Telkamp for the Single Channel Packet Forwarder
 *
 * This is to make a lib that can be used with the LWGW Gateway
 *
 *******************************************************************************/
/*******************************************************************************
 *
 * Copyright (c) 2015 Thomas Telkamp
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/

#include <string>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <iostream>
#include <cstdlib>
#include <sys/time.h>
#include <cstring>

#include <sys/ioctl.h>
#include <net/if.h>

using namespace std;

#include "base64.h"

#include <wiringPi.h>
#include <wiringPiSPI.h>

typedef bool boolean;
typedef unsigned char byte;
enum sf_t { SF7=7, SF8, SF9, SF10, SF11, SF12 };

static const int CHANNEL = 0;

//static byte currentMode = 0x81;

static char message[256];
static char b64[256];

static bool sx1272 = true;

static byte receivedbytes;

static struct sockaddr_in si_other;
static int slen=sizeof(si_other);
static int udpSocket;
static struct ifreq ifr;

static uint32_t cp_nb_rx_rcv;
static uint32_t cp_nb_rx_ok;
//static uint32_t cp_nb_rx_bad;
//static uint32_t cp_nb_rx_nocrc;
static uint32_t cp_up_pkt_fwd;


/*******************************************************************************
 *
 * Configure these values!
 *
 *******************************************************************************/

// SX1272 - Raspberry connections
int ssPin = 6;
int dio0  = 7;
int RST   = 0;


static int codeR = 45;
static int bandW = 125;
static int pwr = 5;
static int rssiPacket = 0;
static int rssiValue = 0;
static long int snrValue = 0;
static int lenReceived = 0;

// Set spreading factor (SF7 - SF12)
static sf_t sf = SF7;

// Set center frequency
static uint32_t  freq = 868100000; // in Mhz! (868.1)

// Versailles
// static float lat=48.795280;
// static float lon=2.138730;
// static int   alt=50;

// Cr�teil
static float lat=48.788463;
static float lon=2.442822;
static int   alt=50;

/* Informal status fields */
static char platform[24]    = "LO Pi3 mono-channel";   /* platform definition */
static char email[40]       = "lorawan.lacl@gmail.com";  /* used for contact email */
static char description[64] = "LWGW";          /* used for free form description */

// define server
// TODO: use host names and dns
#define SERVER1 "40.114.249.243"
#define PORT 1700

// #############################################
// #############################################

#define REG_FIFO                    0x00
#define REG_FIFO_ADDR_PTR           0x0D
#define REG_FIFO_TX_BASE_AD         0x0E
#define REG_FIFO_RX_BASE_AD         0x0F
#define REG_RX_NB_BYTES             0x13
#define REG_OPMODE                  0x01
#define REG_FIFO_RX_CURRENT_ADDR    0x10
#define REG_IRQ_FLAGS               0x12
#define REG_DIO_MAPPING_1           0x40
#define REG_DIO_MAPPING_2           0x41
#define REG_MODEM_CONFIG            0x1D
#define REG_MODEM_CONFIG2           0x1E
#define REG_MODEM_CONFIG3           0x26
#define REG_SYMB_TIMEOUT_LSB        0x1F
#define REG_PKT_SNR_VALUE           0x19
#define REG_PAYLOAD_LENGTH          0x22
#define REG_IRQ_FLAGS_MASK          0x11
#define REG_MAX_PAYLOAD_LENGTH      0x23
#define REG_HOP_PERIOD              0x24
#define REG_SYNC_WORD               0x39
#define REG_VERSION                 0x42

#define SX72_MODE_RX_CONTINUOS      0x85
#define SX72_MODE_TX                0x83
#define SX72_MODE_SLEEP             0x80
#define SX72_MODE_STANDBY           0x81

#define PAYLOAD_LENGTH              0x40

// LOW NOISE AMPLIFIER
#define REG_LNA                     0x0C
#define LNA_MAX_GAIN                0x23
#define LNA_OFF_GAIN                0x00
#define LNA_LOW_GAIN                0x20

// CONF REG
#define REG1                        0x0A
#define REG2                        0x84

#define SX72_MC2_FSK                0x00
#define SX72_MC2_SF7                0x70
#define SX72_MC2_SF8                0x80
#define SX72_MC2_SF9                0x90
#define SX72_MC2_SF10               0xA0
#define SX72_MC2_SF11               0xB0
#define SX72_MC2_SF12               0xC0

#define SX72_MC1_LOW_DATA_RATE_OPTIMIZE  0x01 // mandated for SF11 and SF12

// FRF
#define        REG_FRF_MSB              0x06
#define        REG_FRF_MID              0x07
#define        REG_FRF_LSB              0x08

#define        FRF_MSB                  0xD9 // 868.1 Mhz
#define        FRF_MID                  0x06
#define        FRF_LSB                  0x66

#define BUFLEN 2048  //Max length of buffer

#define PROTOCOL_VERSION  1
#define PKT_PUSH_DATA 0
#define PKT_PUSH_ACK  1
#define PKT_PULL_DATA 2
#define PKT_PULL_RESP 3
#define PKT_PULL_ACK  4

#define TX_BUFF_SIZE  2048
#define STATUS_SIZE   1024

static void selectreceiver()
{
    digitalWrite(ssPin, LOW);
}

static void unselectreceiver()
{
    digitalWrite(ssPin, HIGH);
}

static byte readRegister(byte addr)
{
    unsigned char spibuf[2];

    selectreceiver();
    spibuf[0] = addr & 0x7F;
    spibuf[1] = 0x00;
    wiringPiSPIDataRW(CHANNEL, spibuf, 2);
    unselectreceiver();

    return spibuf[1];
}

static void writeRegister(byte addr, byte value)
{
    unsigned char spibuf[2];

    spibuf[0] = addr | 0x80;
    spibuf[1] = value;
    selectreceiver();
    wiringPiSPIDataRW(CHANNEL, spibuf, 2);

    unselectreceiver();
}

static boolean receivePkt(char *payload)
{

    // clear rxDone
    writeRegister(REG_IRQ_FLAGS, 0x40);

    int irqflags = readRegister(REG_IRQ_FLAGS);

    cp_nb_rx_rcv++;

    //  payload crc: 0x20
    if((irqflags & 0x20) == 0x20)
    {
        printf("CRC error\n");
        writeRegister(REG_IRQ_FLAGS, 0x20);
        return false;
    } else {

        cp_nb_rx_ok++;

        byte currentAddr = readRegister(REG_FIFO_RX_CURRENT_ADDR);
        byte receivedCount = readRegister(REG_RX_NB_BYTES);
        receivedbytes = receivedCount;

        writeRegister(REG_FIFO_ADDR_PTR, currentAddr);

        for(int i = 0; i < receivedCount; i++)
        {
	  //test added because first 4 bytes are always equal to "FF FF 00 00"
	  if(i<4) {
	    readRegister(REG_FIFO);
	  } else {
	    payload[i-4] = (char)readRegister(REG_FIFO);
	  }
        }
    }
    return true;
}

static void SetupLoRa()
{
    digitalWrite(RST, HIGH);
    delay(100);
    digitalWrite(RST, LOW);
    delay(100);

    byte version = readRegister(REG_VERSION);

    if (version == 0x22) {
        // sx1272
        printf("SX1272 detected, starting.\n");
        sx1272 = true;
    } else {
        // sx1276?
        digitalWrite(RST, LOW);
        delay(100);
        digitalWrite(RST, HIGH);
        delay(100);
        version = readRegister(REG_VERSION);
        if (version == 0x12) {
            // sx1276
            printf("SX1276 detected, starting.\n");
            sx1272 = false;
        } else {
            printf("Unrecognized transceiver.\n");
            //printf("Version: 0x%x\n",version);
            exit(1);
        }
    }

    writeRegister(REG_OPMODE, SX72_MODE_SLEEP);

    // set frequency
    uint64_t frf = ((uint64_t)freq << 19) / 32000000;
    writeRegister(REG_FRF_MSB, (uint8_t)(frf>>16) );
    writeRegister(REG_FRF_MID, (uint8_t)(frf>> 8) );
    writeRegister(REG_FRF_LSB, (uint8_t)(frf>> 0) );

    writeRegister(REG_SYNC_WORD, 0x34); // LoRaWAN public sync word

    if (sx1272) {
        if (sf == SF11 || sf == SF12) {
            writeRegister(REG_MODEM_CONFIG,0x0B);
        } else {
            writeRegister(REG_MODEM_CONFIG,0x0A);
        }
        writeRegister(REG_MODEM_CONFIG2,(sf<<4) | 0x04);
    } else {
        if (sf == SF11 || sf == SF12) {
            writeRegister(REG_MODEM_CONFIG3,0x0C);
        } else {
            writeRegister(REG_MODEM_CONFIG3,0x04);
        }
        writeRegister(REG_MODEM_CONFIG,0x72);
        writeRegister(REG_MODEM_CONFIG2,(sf<<4) | 0x04);
    }

    if (sf == SF10 || sf == SF11 || sf == SF12) {
        writeRegister(REG_SYMB_TIMEOUT_LSB,0x05);
    } else {
        writeRegister(REG_SYMB_TIMEOUT_LSB,0x08);
    }
    writeRegister(REG_MAX_PAYLOAD_LENGTH,0x80);
    writeRegister(REG_PAYLOAD_LENGTH,PAYLOAD_LENGTH);
    writeRegister(REG_HOP_PERIOD,0xFF);
    writeRegister(REG_FIFO_ADDR_PTR, readRegister(REG_FIFO_RX_BASE_AD));

    // Set Continous Receive Mode
    writeRegister(REG_LNA, LNA_MAX_GAIN);  // max lna gain
    writeRegister(REG_OPMODE, SX72_MODE_RX_CONTINUOS);
}

static void sendudp(char *msg, int length) {
//send the update
#ifdef SERVER1
    inet_aton(SERVER1 , &si_other.sin_addr);
    sendto(udpSocket, (char *)msg, length, 0 , (struct sockaddr *) &si_other, slen);
#endif
}

static void sendstat() {
    static char status_report[STATUS_SIZE]; /* status report as a JSON object */
    char stat_timestamp[24];
    time_t t;

    int stat_index=0;

    /* pre-fill the data buffer with fixed fields */
    status_report[0] = PROTOCOL_VERSION;
    status_report[3] = PKT_PUSH_DATA;

    status_report[4] = (unsigned char)ifr.ifr_hwaddr.sa_data[0];
    status_report[5] = (unsigned char)ifr.ifr_hwaddr.sa_data[1];
    status_report[6] = (unsigned char)ifr.ifr_hwaddr.sa_data[2];
    status_report[7] = 0xFF;
    status_report[8] = 0xFF;
    status_report[9] = (unsigned char)ifr.ifr_hwaddr.sa_data[3];
    status_report[10] = (unsigned char)ifr.ifr_hwaddr.sa_data[4];
    status_report[11] = (unsigned char)ifr.ifr_hwaddr.sa_data[5];

    /* start composing datagram with the header */
    uint8_t token_h = (uint8_t)rand(); /* random token */
    uint8_t token_l = (uint8_t)rand(); /* random token */
    status_report[1] = token_h;
    status_report[2] = token_l;
    stat_index = 12; /* 12-byte header */

    /* get timestamp for statistics */
    t = time(NULL);
    strftime(stat_timestamp, sizeof stat_timestamp, "%F %T %Z", gmtime(&t));

    int j = snprintf((char *)(status_report + stat_index), STATUS_SIZE-stat_index, "{\"stat\":{\"time\":\"%s\",\"lati\":%.5f,\"long\":%.5f,\"alti\":%i,\"rxnb\":%u,\"rxok\":%u,\"rxfw\":%u,\"ackr\":%.1f,\"dwnb\":%u,\"txnb\":%u,\"pfrm\":\"%s\",\"mail\":\"%s\",\"desc\":\"%s\"}}", stat_timestamp, lat, lon, (int)alt, cp_nb_rx_rcv, cp_nb_rx_ok, cp_up_pkt_fwd, (float)0, 0, 0,platform,email,description);
    stat_index += j;
    status_report[stat_index] = 0; /* add string terminator, for safety */

    printf("stat update: %s\n", (char *)(status_report+12)); /* DEBUG: display JSON stat */

    //send the update
    sendudp(status_report, stat_index);

}

static void receivepacket() {
    long int SNR;
    int rssicorr;
    if(digitalRead(dio0) == 1) {
        if(receivePkt(message)) {
	    rssiPacket = 0;
	    rssiValue = 0;
            snrValue = 0;
            lenReceived = 0;
	    
            byte value = readRegister(REG_PKT_SNR_VALUE);
	    
            if( value & 0x80 ) { // The SNR sign bit is 1
                 // Invert and divide by 4
                value = ( ( ~value + 1 ) & 0xFF ) >> 2;
                SNR = -value;
            } else {
                // Divide by 4
                SNR = ( value & 0xFF ) >> 2;
            }
          
            if (sx1272) {
                rssicorr = 139;
            } else {
                rssicorr = 157;
            }

            rssiPacket = readRegister(0x1A)-rssicorr;
            rssiValue = readRegister(0x1B)-rssicorr;
            snrValue = SNR;
            lenReceived = (int)receivedbytes - 4;
        } // received a message
    } // dio0=1
}

int main () {

    struct timeval nowtime;
    uint32_t lasttime;
    
    if ( (udpSocket=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
      return (1);
    }
    
    memset((char *) &si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(PORT);

    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, "eth0", IFNAMSIZ-1);  // can we rely on eth0?
    ioctl(udpSocket, SIOCGIFHWADDR, &ifr);

    /* display result */
    printf("Gateway ID: %.2x:%.2x:%.2x:ff:ff:%.2x:%.2x:%.2x\n",
           (unsigned char)ifr.ifr_hwaddr.sa_data[0],
           (unsigned char)ifr.ifr_hwaddr.sa_data[1],
           (unsigned char)ifr.ifr_hwaddr.sa_data[2],
           (unsigned char)ifr.ifr_hwaddr.sa_data[3],
           (unsigned char)ifr.ifr_hwaddr.sa_data[4],
           (unsigned char)ifr.ifr_hwaddr.sa_data[5]);

    wiringPiSetup() ;
    pinMode(ssPin, OUTPUT);
    pinMode(dio0, INPUT);
    pinMode(RST, OUTPUT);

    //int fd = 
    wiringPiSPISetup(CHANNEL, 500000);
    //cout << "Init result: " << fd << endl;	

    SetupLoRa();
    
    printf("Listening at SF%i on %.6lf Mhz.\n", sf,(double)freq/1000000);
    printf("------------------\n");


    while(1) {
        receivepacket();

        gettimeofday(&nowtime, NULL);
        uint32_t nowseconds = (uint32_t)(nowtime.tv_sec);
        if (nowseconds - lasttime >= 30) {
            lasttime = nowseconds;
            sendstat();
            cp_nb_rx_rcv = 0;
            cp_nb_rx_ok = 0;
            cp_up_pkt_fwd = 0;
        }
        delay(1);
    }
    return (0);
}


bool chisterapi_send(unsigned char * msg, int len) {
  // This lib cannot do a radio emission 
  return !true;
}

bool chisterapi_setup() {
  memset(message, 0, 256);
  if(bandW != 125 && codeR != 45) return false;
  
  wiringPiSetup () ;
  pinMode(ssPin, OUTPUT);
  pinMode(dio0, INPUT);
  pinMode(RST, OUTPUT);

  wiringPiSPISetup(CHANNEL, 500000);

  SetupLoRa();
  return true;
}

int chisterapi_receive(unsigned char * res) {
  receivepacket();
  memcpy(res, message, lenReceived);
  return lenReceived;
}

bool chisterapi_sleep() {
  return true;
}

bool chisterapi_available() {
  return true;
}

int chisterapi_getmaxmsglength() {
  return REG_MAX_PAYLOAD_LENGTH;
}

int chisterapi_getlastrssi() {
  return rssiValue;
}

int chisterapi_getlastsnr() {
  return snrValue;
}

int chisterapi_receivewaitavailable(unsigned char * res, int timeout) {
  return chisterapi_receive(res);
}

bool chisterapi_setfreq(double frequence) {
  bool ret=false;
  //868.10 Mhz (used by Gateway to listen ) 
  //868.30 MHz (used by Gateway to listen )
  //868.50 MHz (used by Gateway to listen ) 
  //864.10 MHz (used by End device to transmit Join Request)
  //864.30 MHz (used by End device to transmit Join Request)
  //864.50 MHz (used by End device to transmit Join Request)
  //868.10 MHz (used by End device to transmit Join Request)
  //868.30 MHz (used by End device to transmit Join Request)
  //868.50 MHz (used by End device to transmit Join Request)
  if(frequence >= 864 && frequence <= 870) {
    ret=true;
    freq = (uint32_t)(frequence*1000000);
  }
  return ret;
}

bool chisterapi_setcr(int codeRedundancy) {
  bool ret=false;
  // there is only one acceptable value with this lib
  if(codeRedundancy == 45) {
    ret=true;
    codeR = codeRedundancy;
  }
  return ret;
}

bool chisterapi_setsf(int spreadingFactor) {
  bool ret=false;
  //7 = 128 chips/symbols 8 = 256 chips/symbols 9 = 512 chips/symbols
  //10 = 1024 chips/symb. 11 = 2048 chips/symb. 12 = 4096 chips/symb.
  if(spreadingFactor >= 7 && spreadingFactor <= 12) {
    ret=true;
    switch(spreadingFactor) {
    case 7:
      sf = SF7;
      break;
    case 8:
      sf = SF8;
      break;
    case 9:
      sf = SF9;
      break;
    case 10:
      sf = SF10;
      break;
    case 11:
      sf = SF11;
      break;
    case 12:
      sf = SF12;
      break;
    }
  }
  return ret;
}

bool chisterapi_setbw(int bandwidth) {
  bool ret=false;
  // there is only one acceptable value with this lib
  if(bandwidth == 125) {
    ret=true;
    bandW = bandwidth;
  }
  return ret;
}

bool chisterapi_setpw(int power) {
   bool ret=false;
  // this value isn't used in this lib and has no effect
  if(power >= 5 && power <= 23) {
    ret=true;
    pwr = power;
  }
  return ret;
}

