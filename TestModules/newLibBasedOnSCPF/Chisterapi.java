package lwgw.hardwareinterface;

public class Chisterapi {
	private double frequence;
	private int codeRedundancy;
	private int spreadingFactor;
	private int bandWidth;
	private int power;
	
	static {
		System.loadLibrary("chisterapi");
	}

	public native boolean setup();
	public native boolean sleep();

	public native boolean available();

	public native boolean send(byte[] msg);
	public native byte[] receive(int len);

	public native int getMaxMsgLength();
	public native int getLastRssi();
	public native int getLastSnr();
	public native byte[] receiveWaitAvailable(int size, int timeout);
	private native boolean setFreq(Double freq);
	private native boolean setCr(int cr);
	private native boolean setSf(int sf);
	private native boolean setBw(int bw);
	private native boolean setPw(int pw);

	public boolean setFrequence(Double freq) {
		boolean resultIsOk = setFreq(freq);
		if(resultIsOk) frequence = freq;
		return resultIsOk;
	}
	public boolean setCodeRedundancy(int cr) {
		boolean resultIsOk = setCr(cr);
		if(resultIsOk) codeRedundancy = cr;		
		return resultIsOk;
	}
	public boolean setSpreadingFactor(int sf) {
		boolean resultIsOk = setSf(sf);
		if(resultIsOk) spreadingFactor = sf;
		return resultIsOk;
	}
	public boolean setBandWidth(int bw) {
		boolean resultIsOk = setBw(bw);
		if(resultIsOk) bandWidth = bw;
		return resultIsOk;
	}
	public boolean setPower(int pw) {
		boolean resultIsOk = setPw(pw);
		if(resultIsOk) power = pw;		
		return resultIsOk;
	}
	
	public double getFrequence() {
		return frequence;
	}
	public int getCodeRedundancy() {
		return codeRedundancy;
	}
	public int getSpreadingFactor() {
		return spreadingFactor;
	}
	public int getBandWidth() {
		return bandWidth;
	}
	public int getPower() {
		return power;
	}

	public static void main(String ... args) {

		Chisterapi lora = new Chisterapi();
		System.out.println("Lora setup");
		lora.setup();
		boolean loraAvailability = lora.available();
		System.out.println("Asking for lora availability : " + loraAvailability);
		//if(!loraAvailability) return;
		
		System.out.println("Sending 2 bytes : ");
		lora.send(new byte[] { (byte)0x01, (byte)0x02 });
		System.out.println("Now sleep");
		lora.sleep();
	}
}
