
#include "lwgw_hardwareinterface_Chisterapi.h"
#include "chisterapi-util.h"

JNIEXPORT jboolean JNICALL Java_lwgw_hardwareinterface_Chisterapi_setup(JNIEnv *env, jobject obj) {

	return chisterapi_setup();
}

JNIEXPORT jboolean JNICALL Java_lwgw_hardwareinterface_Chisterapi_sleep(JNIEnv *env, jobject obj) {

	return chisterapi_sleep();
}

JNIEXPORT jboolean JNICALL Java_lwgw_hardwareinterface_Chisterapi_available(JNIEnv *env, jobject obj) {

	return chisterapi_available();
}

JNIEXPORT jboolean JNICALL Java_lwgw_hardwareinterface_Chisterapi_send(JNIEnv *env, jobject obj, jbyteArray array) {

    	int n = (int)(env->GetArrayLength(array));
	jbyte *inCArray = env->GetByteArrayElements(array, NULL);
	return chisterapi_send((unsigned char*)inCArray, n);
}

JNIEXPORT jbyteArray JNICALL Java_lwgw_hardwareinterface_Chisterapi_receive(JNIEnv *env, jobject obj, jint size) {

	jbyte* tmp = new jbyte[(int)size];
	int len = chisterapi_receive((unsigned char*)tmp);
	jbyteArray res = env->NewByteArray(len);
	env->SetByteArrayRegion(res, 0, len, const_cast<const jbyte*>(tmp));
	delete [] tmp;
	return res;
}

JNIEXPORT jint JNICALL Java_lwgw_hardwareinterface_Chisterapi_getMaxMsgLength(JNIEnv *env, jobject obj) {

	return chisterapi_getmaxmsglength();
}

JNIEXPORT jint JNICALL Java_lwgw_hardwareinterface_Chisterapi_getLastRssi(JNIEnv *env, jobject obj) {

	return chisterapi_getlastrssi();
}

JNIEXPORT jint JNICALL Java_lwgw_hardwareinterface_Chisterapi_getLastSnr(JNIEnv *env, jobject obj) {

	return chisterapi_getlastsnr();
}

JNIEXPORT jbyteArray JNICALL Java_lwgw_hardwareinterface_Chisterapi_receiveWaitAvailable(JNIEnv *env, jobject obj, jint size, jint timeout) {

	jbyte* tmp = new jbyte[(int)size];
	int len = chisterapi_receivewaitavailable((unsigned char*)tmp, (int)timeout);
	jbyteArray res = env->NewByteArray(len);
	env->SetByteArrayRegion(res, 0, len, const_cast<const jbyte*>(tmp));
	delete [] tmp;
	return res;
}

JNIEXPORT jboolean JNICALL Java_lwgw_hardwareinterface_Chisterapi_setFreq(JNIEnv *env, jobject obj, jobject freq) {

	//jclass jcDouble = env->FindClass("java/lang/Double");
	//jmethodID jmidDoubleValue = env->GetMethodID(jcDouble, "doubleValue", "()D" );
	//double dFreq = env->CallDoubleMethod(freq, jmidDoubleValue);
  
	jclass activityClass = env->GetObjectClass(freq);
	jmethodID jmidDoubleValue = env->GetMethodID(activityClass, "doubleValue", "()D");
	if (jmidDoubleValue == 0) return false;
	double dFreq = env->CallDoubleMethod(freq, jmidDoubleValue);

	return chisterapi_setfreq((double) dFreq);
}

JNIEXPORT jboolean JNICALL Java_lwgw_hardwareinterface_Chisterapi_setCr(JNIEnv *env, jobject obj, jint cr) {

	return chisterapi_setcr((int) cr);
}

JNIEXPORT jboolean JNICALL Java_lwgw_hardwareinterface_Chisterapi_setSf(JNIEnv *env, jobject obj, jint sf) {

	return chisterapi_setsf((int) sf);
}

JNIEXPORT jboolean JNICALL Java_lwgw_hardwareinterface_Chisterapi_setBw(JNIEnv *env, jobject obj, jint bw) {

	return chisterapi_setbw((int) bw);
}

JNIEXPORT jboolean JNICALL Java_lwgw_hardwareinterface_Chisterapi_setPw(JNIEnv *env, jobject obj, jint pw) {

	return chisterapi_setpw((int) pw);
}


