

Modules for testing the LWGW
============================


lora_chisterapi_java
--------------------

This is the tiny Iot mockup

This application works on a Raspberry Pi 2 the ChisteraPi card from Snootlab.

You have to install :

 * the wiringPi library to connect the Chistera Pi card

The makefile is to make the library working with RADIOHEAD
Just go in src/ to make the program with javac.
And start the Iot mockup with send.sh 


LoraServer
----------

This is the tiny server mockup

Build it with eclipse and execute it from your PC.
It listens PULL mesages coming from the LWGW gateway on port 1700.
It acknowledges PULL messages and then sends command to the LWGW gateway.


newLibBasedOnSCPF
-----------------

Just launch the Makefile to make the lib and place it in the target directory of the LWGW project. This library can listen much more types of Iot than the Radiohead based lib (it certainly depends from preambule symbols of radio frames). This lib cannot execute broadcast radio messages that can be found in servers' commands.

Go on and enjoy !