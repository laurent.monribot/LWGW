
public class Receive {

    public static void main(String ... args) {
	int counter = 0;
	lwgw.hardwareinterface.Chisterapi lora = new lwgw.hardwareinterface.Chisterapi();
	lora.setCr(45);
	lora.setSf(7);
	lora.setPw(5);
	lora.setBw(125);
	lora.setFreq(new Double(868.0));
	lora.setup();
	System.out.println(lora.available());
	System.out.println(lora.getMaxMsgLength());
	byte[] res = null;
	while(true) {
	    try {
		Thread.sleep(100);
	    } catch (InterruptedException e) {
		System.out.println(e);
	    }
	    res = lora.receive(lora.getMaxMsgLength());
	    if (res.length != 0) {
		System.out.println(res.length);
		System.out.println(new String(res));
		
		lora.send(("LoRa response " + counter).getBytes());
		counter++;
	    }
	}
    }
}
