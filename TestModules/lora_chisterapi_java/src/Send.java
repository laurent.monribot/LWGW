import java.util.Base64;

public class Send {
    private static final int SLEEP_UNIT = 250; // millisecond
    private static final int NUMBER_OF_SLEEP_UNIT = 12;

    public static void main(String ... args) throws Exception {
	byte[] res = null;
	byte[] mes = null;
	int messageNumber=0;

	lwgw.hardwareinterface.Chisterapi lora = new lwgw.hardwareinterface.Chisterapi();
	lora.setCr(45);
	lora.setSf(7);
	lora.setPw(5);
	lora.setBw(125);
	lora.setFreq(new Double(868.0));
	lora.setup();
	
	mes = createMsg(messageNumber++);
	res = sendWithAck(mes, lora);		    
	while(res.length!=0) {
	    if (res.length != 0) {
		System.out.println("------------------");
		System.out.println("length response : " + res.length);
		System.out.println("content response : " + new String(res));
	    }
	    try {
		Thread.sleep(29950);
	    } catch (InterruptedException e) {
		System.out.println(e);
	    }
	    mes = createMsg(messageNumber++);
	    res = sendWithAck(mes, lora);
	    //lora.sleep();
	}
	//System.out.println("FAILED");
    }

    public static byte[] sendWithAck(byte[] msg, lwgw.hardwareinterface.Chisterapi lora) {
	byte[] res = null;
	
	res = send(msg, lora);
	if(res == null || (res!=null && res.length==0)) {
	    System.out.println("REPEAT");
	    res = send(msg, lora);
	    if(res == null || (res!=null && res.length==0)) {
		System.out.println("REPEAT 2");
		res = send(msg, lora);
		if(res == null || (res!=null && res.length==0)) {
		    System.out.println("REPEAT 3");
		    res = send(msg, lora);
		}
	    }
	}
	return res;
    }

    public static byte[] send(byte[] msg, lwgw.hardwareinterface.Chisterapi lora) {
	byte[] res = null;	
	int waitingCounter = 0;
	lora.send(msg);
	lora.available();

	while((res == null || (res!=null && res.length==0)) && waitingCounter++ < NUMBER_OF_SLEEP_UNIT) {
	    try {
		Thread.sleep(SLEEP_UNIT);
	    } catch (InterruptedException e) {
		System.out.println(e);
	    }
	    res = lora.receive(lora.getMaxMsgLength());
	}
	System.out.println("Waited " + waitingCounter + " times");
	return res;
    }

    public static byte[] createMsg(int n) {
	//return ("Hello LoRa " + new java.util.Date().toString()).getBytes();
	String str = new String("gPEA/wOAZQADD5zR7yZSMq04obo=");
	byte[] returnValue = Base64.getDecoder().decode(str.getBytes());
	returnValue[6] = (byte)(n & 0xFF);
	returnValue[7] = (byte)(n >> 8);
	System.out.println(new String(Base64.getEncoder().encode(returnValue)));
	return returnValue;
	//return Base64.b64_to_bin(msg, msg.length, destination, destination.length);
    }
}
