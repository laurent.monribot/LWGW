
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include <RH_RF95.h>

static RH_RF95 rf95;
static int RHRF95_lastRssi = 0;
static double RHRF95_freq = 868.0;
static int RHRF95_bw = 125;
static int RHRF95_cr = 48;
static int RHRF95_sf = 12;
static int RHRF95_pw = 23;

bool chisterapi_send(unsigned char * msg, int len) {

    	bool res = rf95.send((uint8_t*)msg, len);
    	rf95.waitPacketSent();
    	return res;
}

bool chisterapi_setup() {
    	int errorConfig = 0;
	
    	wiringPiSetupGpio();
    	if (!rf95.init()) {
        	return false;
    	}
    	rf95.setTxPower(RHRF95_pw);
	
	if(RHRF95_bw==31 ){ // special case where cr=4/8 & sf=9
	  rf95.setModemConfig(RH_RF95::Bw31_25Cr48Sf512);
	} else if(RHRF95_bw==125){
	  if(RHRF95_cr==45) {
	    if(RHRF95_sf==7) {
	      rf95.setModemConfig(RH_RF95::Bw125Cr45Sf128);
	    } else if(RHRF95_sf==9) {
	      rf95.setModemConfig(RH_RF95::sf512cr45bw125);
	    } else if(RHRF95_sf==11) {
	      rf95.setModemConfig(RH_RF95::sf2048cr45bw125);
	    } else errorConfig = 1;
	  } else if(RHRF95_cr==48) {
	    if(RHRF95_sf==7) {
	      rf95.setModemConfig(RH_RF95::sf128cr48bw125);// should be default
	    } else if(RHRF95_sf==9) {
	      rf95.setModemConfig(RH_RF95::sf512cr48bw125);
	    } else if(RHRF95_sf==11) {
	      rf95.setModemConfig(RH_RF95::sf2048cr48bw125);
	    } else if(RHRF95_sf==12) {
	      rf95.setModemConfig(RH_RF95::Bw125Cr48Sf4096);
	    } else errorConfig = 1;
	  } else errorConfig = 1;
	} else if(RHRF95_bw==250){
	  if(RHRF95_cr==45) {
	    if(RHRF95_sf==7) {
	      rf95.setModemConfig(RH_RF95::sf128cr45bw250);
	    } else if(RHRF95_sf==9) {
	      rf95.setModemConfig(RH_RF95::sf512cr45bw250);
	    } else if(RHRF95_sf==11) {
	      rf95.setModemConfig(RH_RF95::sf2048cr45bw250);
	    } else errorConfig = 1;
	  } else if(RHRF95_cr==48) {
	    if(RHRF95_sf==7) {
	      rf95.setModemConfig(RH_RF95::sf128cr48bw250);
	    } else if(RHRF95_sf==9) {
	      rf95.setModemConfig(RH_RF95::sf512cr48bw250);
	    } else if(RHRF95_sf==11) {
	      rf95.setModemConfig(RH_RF95::sf2048cr48bw250);
	    } else errorConfig = 1;
	  } else errorConfig = 1;    
	} else if(RHRF95_bw==500){
	  if(RHRF95_cr==45) {
	    if(RHRF95_sf==7) {
	      rf95.setModemConfig(RH_RF95::Bw500Cr45Sf128);
	    } else if(RHRF95_sf==9) {
	      rf95.setModemConfig(RH_RF95::sf512cr45bw500);
	    } else if(RHRF95_sf==11) {
	      rf95.setModemConfig(RH_RF95::sf2048cr45bw500);
	    } else errorConfig = 1;
	  } else if(RHRF95_cr==48) {
	    if(RHRF95_sf==7) {
	      rf95.setModemConfig(RH_RF95::sf128cr48bw500);
	    } else if(RHRF95_sf==9) {
	      rf95.setModemConfig(RH_RF95::sf512cr48bw500);
	    } else if(RHRF95_sf==11) {
	      rf95.setModemConfig(RH_RF95::sf2048cr48bw500);
	    } else errorConfig = 1;
	  } else errorConfig = 1;
	} else errorConfig = 1;
	if(errorConfig) { // long range
	  rf95.setModemConfig(RH_RF95::Bw125Cr48Sf4096);
	}
    	rf95.setFrequency(RHRF95_freq);
//	rf95.printRegisters();
	return true;
}


int chisterapi_receive(unsigned char * res) {

        uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
        uint8_t len = sizeof(buf);
        if (rf95.recv(buf, &len)) {
		memcpy(res, buf, len);
		RHRF95_lastRssi = rf95.lastRssi();
		return len;
        } else {
		return 0;
	}
}

bool chisterapi_sleep() {

	return rf95.sleep();
}

bool chisterapi_available() {

	return rf95.available();
}

int chisterapi_getmaxmsglength() {

	return rf95.maxMessageLength();
}

int chisterapi_getLastRssi() {

	return RHRF95_lastRssi;
}

int chisterapi_receiveWaitAvailable(unsigned char * res, int timeout) {

  int len =0;
  if(timeout>0) {
    if(rf95.waitAvailableTimeout((uint16_t) timeout)) {
      len = chisterapi_receive(res);
    }
  } else {
    rf95.waitAvailable();
    len = chisterapi_receive(res);
  }
  return len;
}

bool chisterapi_setFreq(double frequence) {
  bool ret=false;
  //868.10 Mhz (used by Gateway to listen ) 
  //868.30 MHz (used by Gateway to listen )
  //868.50 MHz (used by Gateway to listen ) 
  //864.10 MHz (used by End device to transmit Join Request)
  //864.30 MHz (used by End device to transmit Join Request)
  //864.50 MHz (used by End device to transmit Join Request)
  //868.10 MHz (used by End device to transmit Join Request)
  //868.30 MHz (used by End device to transmit Join Request)
  //868.50 MHz (used by End device to transmit Join Request)
  if(frequence >= 864 && frequence <= 870) {
    ret=true;
    RHRF95_freq = frequence;
  }
  return ret;
}

bool chisterapi_setCr(int codeRedundancy) {
  bool ret=false;
  if(codeRedundancy == 45 || codeRedundancy == 46 ||
     codeRedundancy == 47 || codeRedundancy == 48) {
    ret=true;
    RHRF95_cr = codeRedundancy;
  }
  return ret;
}

bool chisterapi_setSf(int spreadingFactor) {
  bool ret=false;
  //7 = 128 chips/symbols 8 = 256 chips/symbols 9 = 512 chips/symbols
  //10 = 1024 chips/symb. 11 = 2048 chips/symb. 12 = 4096 chips/symb.
  if(spreadingFactor == 7 || spreadingFactor == 8 ||
     spreadingFactor == 9 || spreadingFactor == 10 ||
     spreadingFactor == 11 || spreadingFactor == 12) {
    ret=true;
    RHRF95_sf = spreadingFactor;
  }
  return ret;
}

bool chisterapi_setBw(int bandwidth) {
  bool ret=false;
  //7.8 kHz; 10.4 kHz; 15.6 kHz; 20.8 kHz; 31.2 kHz;
  //41.7 kHz; 62.5 kHz; 125 kHz; 250 kHz; 500 kH
  if(bandwidth == 125 || bandwidth == 250 ||
     bandwidth == 500 || bandwidth == 31) {
    ret=true;
    RHRF95_bw = bandwidth;
  }
  return ret;
}

bool chisterapi_setPw(int power) {
  bool ret=false;
  if(power >= 5 && power <= 23) {
    ret=true;
    RHRF95_pw = power;
  }
  return ret;
}



/*
RH_RF95::RH_RF95(uint8_t slaveSelectPin, uint8_t interruptPin)
bool RH_RF95::init()
void RH_RF95::handleInterrupt()
void RH_RF95::isr0()
void RH_RF95::isr1()
void RH_RF95::isr2()
void RH_RF95::validateRxBuf()
void RH_RF95::lowPowerListening()
bool RH_RF95::available()
void RH_RF95::clearRxBuf()
bool RH_RF95::recv(uint8_t* buf, uint8_t* len)
bool RH_RF95::send(const uint8_t* data, uint8_t len)
bool RH_RF95::printRegisters()
uint8_t RH_RF95::maxMessageLength()
bool RH_RF95::setFrequency(float centre)
void RH_RF95::setModeCad()
void RH_RF95::setModeIdle()
bool RH_RF95::sleep()
void RH_RF95::setModeRx()
void RH_RF95::setModeTx()
void RH_RF95::setTxPower(int8_t power)
void RH_RF95::setModemRegisters(const ModemConfig* config)
bool RH_RF95::setModemConfig(ModemConfigChoice index)
void RH_RF95::setPreambleLength(uint16_t bytes)
*/
