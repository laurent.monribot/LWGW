

bool chisterapi_send(unsigned char *, int);
int chisterapi_receive(unsigned char *);
bool chisterapi_setup();
bool chisterapi_sleep();
bool chisterapi_available();
int chisterapi_getmaxmsglength();
int chisterapi_getLastRssi();
int chisterapi_receiveWaitAvailable(unsigned char * res, int timeout);
bool chisterapi_setFreq(double frequence);
bool chisterapi_setCr(int codeRedundancy);
bool chisterapi_setSf(int spreadingFactor);
bool chisterapi_setBw(int bandwidth);
bool chisterapi_setPw(int power);
