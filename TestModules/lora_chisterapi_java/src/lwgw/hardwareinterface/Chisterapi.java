package lwgw.hardwareinterface;

public class Chisterapi {

	static {
		System.loadLibrary("chisterapi");
	}

	public native boolean setup();
	public native boolean sleep();

	public native boolean available();

	public native boolean send(byte[] msg);
	public native byte[] receive(int len);

	public native int getMaxMsgLength();
	public native int getLastRssi();
	public native byte[] receiveWaitAvailable(int size, int timeout);
	public native boolean setFreq(Double freq);
	public native boolean setCr(int cr);
	public native boolean setSf(int sf);
	public native boolean setBw(int bw);
	public native boolean setPw(int pw);



	public static void main(String ... args) {

		Chisterapi lora = new Chisterapi();
		System.out.println("Lora setup");
		lora.setup();
		boolean loraAvailability = lora.available();
		System.out.println("Asking for lora availability : " + loraAvailability);
		//if(!loraAvailability) return;
		
		System.out.println("Sending 2 bytes : ");
		lora.send(new byte[] { (byte)0x01, (byte)0x02 });
		System.out.println("Now sleep");
		lora.sleep();
	}
}
